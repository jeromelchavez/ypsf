﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private btnBack As Button
	Private btnSet As Button
	Private btnTut As Button
	Private btnDev As Button
	Private lbl1 As Label
	Private img1 As ImageView
	Private lbl2 As Label
	Private img2 As ImageView
	Private lbl3 As Label
	Private img3 As ImageView
	Dim info1, info2, info3 As String
	info1 =	"name surname" & CRLF & _
			"12345678900"  & CRLF & _
			"email@gmail.com"
	info2 =	"name surname" & CRLF & _
			"12345678900"  & CRLF & _
			"email@gmail.com"
	info3 =	"name surname" & CRLF & _
			"12345678900"  & CRLF & _
			"email@gmail.com"
		
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Activity.LoadLayout("6")

End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub



Sub btnDev_Click
	Activity.RemoveAllViews
	Activity.LoadLayout("7")
	lbl1.Text = info1
	lbl2.Text = info2
	lbl3.Text = info3
	Activity.Invalidate
End Sub
Sub btnTut_Click
	
End Sub
Sub btnSet_Click
	
End Sub
Sub btnBack_Click
	
End Sub