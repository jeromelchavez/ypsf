﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim  db As SQL

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private btnBack As Button
	Private btnSet As Button
	Dim screen As PhoneWakeState
'	Private listScore As ListView
	Private label1, label2 As Label
	Private pnlVol As Panel
	Private pnlCur As Panel
	Private tglFull As ToggleButton
	Private btnReset As Button
	Private pnlSet As Panel
	Dim anim As ICOSSlideAnimation
	Dim setclick As Boolean
	Dim slideval = 700, durval = 1500 As Float
	Private pnlBg As Panel
	Private scroScore As ScrollView
	Private lblName, lblScore As Label
	Dim su As StringUtils
	Private btnClearHS As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Dim fontsize = (10%x/100)*40 As Int
	Dim finalheight As Float
	If File.Exists(File.DirInternal,"db") == False Then
		File.Copy(File.DirAssets, "db.db", File.DirInternal, "db")
	End If
	If FirstTime Then
		db.Initialize(File.DirInternal,"db", False)
	End If
	pnlBg.Initialize("pnlBg")
	lblName.Initialize("")
	lblScore.Initialize("")
	screen.KeepAlive(True)
	screen.PartialLock
	Activity.LoadLayout("3")
	scroScore.Panel.Height = 1500dip
	scroScore.Panel.AddView(lblName, 0, 0, 80%x, scroScore.Panel.Height)
	scroScore.Panel.AddView(lblScore, 80%x, 0, 20%x, scroScore.Panel.Height)
	lblName.TextSize = fontsize
	lblScore.TextSize = fontsize
	lblName.TextColor = Colors.White
	lblScore.TextColor = Colors.White

		Dim cursor As Cursor
		Dim name, name2, score, score2, aaa As String
		Dim totalrows As Int
		cursor = db.ExecQuery("SELECT * FROM hscore ORDER BY rscore DESC LIMIT 20")
		totalrows = cursor.RowCount
		For i = 0 To totalrows - 1
			cursor.Position = i
			name = cursor.GetString("name")
			score = cursor.GetString("score")
			name2 = (i+1) &"." & TAB & name.Replace(CRLF, CRLF&TAB&"  " ) & CRLF & CRLF
			score2 = score & CRLF & CRLF
			lblName.Text = lblName.Text  & name2
			lblScore.Text =  lblScore.Text & score2
		Next
		finalheight = su.MeasureMultilineTextHeight(lblName, lblName.Text)
	lblName.Height = finalheight
	lblScore.Height = finalheight
	scroScore.Panel.Height = finalheight
End Sub

Sub Activity_Resume
	Dim fstat = StateManager.GetSetting2("fullscreen", "0") As Int
	If fstat == 1 Then
		function.FullScreen(True, "Score")
	Else
		function.FullScreen(False, "Score")
	End If
	Activity.Invalidate
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	screen.ReleaseKeepAlive
	screen.ReleasePartialLock
End Sub

Sub btnBack_Click
	Activity.Finish
	StartActivity(Main)
	function.SetAnimation("animtoright", "animfromleft")
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean
    If KeyCode = KeyCodes.KEYCODE_BACK Then
		Return True
	Else If KeyCode = KeyCodes.KEYCODE_MENU And btnSet.Enabled == True Then
		btnSet_Click
	End If
End Sub

Sub btnSet_Click
	Dim sld As StateListDrawable
	Dim checked As BitmapDrawable
	Dim unchecked As ColorDrawable
	Dim sld2 As StateListDrawable
	Dim enabled2, pressed2 As BitmapDrawable
	
	enabled2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button.png", 40%x, 13%x))
	pressed2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button1.png", 40%x, 13%x))
	sld2.Initialize
	sld2.AddState(sld.State_Pressed, pressed2)
	sld2.AddState(sld.State_Enabled, enabled2)
	
	checked.Initialize(LoadBitmap(File.DirAssets, "tglc.png"))
	unchecked.Initialize(Colors.White, 0)
	sld.Initialize
	sld.AddState(sld.State_Unchecked, unchecked)
	sld.AddState(sld.State_Checked, checked)
	Dim fstat2 = StateManager.GetSetting2("fullscreen", "0") As Int
	Dim s As Boolean
	Dim vol = StateManager.GetSetting2("volume", "5") As Float
	If fstat2 == 1 Then
		s = True
	Else
		s = False
	End If
	If setclick == False Then
		Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9%x)
		pnlBg.LoadLayout("5")
		pnlCur.Width = (vol/10)*pnlVol.Width
		pnlBg.Color = 0x64000000
		tglFull.Background = sld
		tglFull.Checked = s
		btnReset.Background = sld2
		anim.SlideFromTop("", slideval, durval)
		anim.StartAnim(pnlSet)
		setclick = True
	Else
		anim.SlideToTop("anim",slideval, durval)
		anim.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlBg_Click
	If setclick == True Then
		anim.SlideToTop("anim",slideval, durval)
		anim.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlSet_Click

End Sub

Sub pnlVol_Touch (Action As Int, X As Float, Y As Float)
	Dim a As Float
	If X < 0 Then
		X = 0
	Else If X > pnlVol.Width Then
		X = pnlVol.Width
	End If
	a = (X/pnlVol.Width)*10
	StateManager.SetSetting("volume", a)
	StateManager.SaveSettings
	pnlCur.Width = X
End Sub

Sub tglFull_CheckedChange(Checked As Boolean)
	If Checked Then
		StateManager.SetSetting("fullscreen", "1")
	Else
		StateManager.SetSetting("fullscreen", "0")
	End If
	StateManager.SaveSettings
	function.FullScreen(Checked, "Score")
End Sub

Sub btnReset_Click
	Dim res As Int
	res = Msgbox2("Do you really want reset the game?"&CRLF&"*note that data will deleted including game progress and library.", "Reset game", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		function.resetall(db)
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub

Sub anim_animationend
	pnlBg.RemoveAllViews
	pnlBg.RemoveView
	btnSet.Enabled = True
End Sub

Sub btnClearHS_Click
	Dim res As Int
	res = Msgbox2("Do you really want to clear the highscore?", "Reset", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		db.ExecNonQuery("DELETE FROM hscore")
		StateManager.SetSetting("pnum", "0")
		StateManager.SaveSettings
		lblName.Text = ""
		lblScore.Text = ""
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub