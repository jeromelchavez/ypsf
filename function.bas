﻿Type=StaticCode
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
'Code module
'Subs in this code module will be accessible from all modules.
Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
End Sub


Sub getImgExt(val As String) As String
	Dim fileext As String
	If File.Exists(File.DirAssets, val &".png") Then
		fileext = val & ".png"
	Else
		fileext = val & ".jpg"
	End If
	Return fileext
End Sub

Sub getSndExt(val2 As String) As String
	Dim fileext2 As String
	If File.Exists(File.DirAssets, val2 & ".mp3") Then
		fileext2 = val2 & ".mp3"
	Else If File.Exists(File.DirAssets, val2 & ".wav") Then
		fileext2 = val2 & ".wav"
	Else If File.Exists(File.DirAssets, val2 & ".mid") Then
		fileext2 = val2 & ".mid"
	Else
		fileext2 = val2 & ".ogg"
	End If
	Return fileext2
End Sub

Sub FullScreen(Active As Boolean, ActivityName As String)
    Dim obj1 As Reflector
    Dim i As Int
    i = 1024  'FLAG_FULLSCREEN
    obj1.Target = obj1.GetMostCurrent(ActivityName)
    obj1.Target = obj1.RunMethod("getWindow")
    If Active Then
        obj1.RunMethod2("addFlags",i,"java.lang.int")
    Else
        obj1.RunMethod2("clearFlags",i,"java.lang.int")
    End If
End Sub

Sub resetall(sql1 As SQL)
'	sql1.ExecNonQuery("DELETE FROM hscore")
	sql1.ExecNonQuery("UPDATE game SET stat='0', lib='0'")
	StateManager.SetSetting("currentid", "0")
	StateManager.SetSetting("pnum", "0")
	StateManager.SaveSettings
End Sub

Sub SetAnimation(InAnimation As String, OutAnimation As String)
    Dim r As Reflector
    Dim package As String
    Dim In, out As Int
	package = r.GetStaticField("anywheresoftware.b4a.BA", "packageName")
    In = r.GetStaticField(package & ".R$anim", InAnimation)
    out = r.GetStaticField(package & ".R$anim", OutAnimation)
    r.Target = r.GetActivity
    r.RunMethod4("overridePendingTransition", Array As Object(In, out), Array As String("java.lang.int", "java.lang.int"))
End Sub

Sub SetProgressDrawable(p As ProgressBar, drawable As Object)
	Dim r As Reflector
	Dim clipDrawable As Object
		clipDrawable = r.CreateObject2("android.graphics.drawable.ClipDrawable", _
		Array As Object(drawable, Gravity.LEFT, 1), _
		Array As String("android.graphics.drawable.Drawable", "java.lang.int", "java.lang.int"))
	r.Target = p
	r.Target = r.RunMethod("getProgressDrawable") 'Gets the layerDrawable
	r.RunMethod4("setDrawableByLayerId", _ 
		Array As Object(r.GetStaticField("android.R$id", "progress"), clipDrawable), _
		Array As String("java.lang.int", "android.graphics.drawable.Drawable"))
End Sub