﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim db As SQL
	Private play As MediaPlayer

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private btnBack As Button
	Private btnPlay As Button
	Private img1 As ImageView
	Private img2 As ImageView
	Private img3 As ImageView
	Private img4 As ImageView
	Dim pic1, pic2, pic3, pic4, sound, ans, trivia, anPic, animalName As String
	Private lblLevel As Label
	Dim animate As AnimationDrawable
	Dim thislevel As String
	Dim screen As PhoneWakeState
	Private pnlPics As Panel
	Private lblTrivia As Label
	Dim anim2 As ICOSSlideAnimation
	Dim aniFade As ICOSFadeAnimation
	Dim phone As Phone
	Dim slideval = 700, durval = 1500 , fadedur = 300 As Float
	'popup trivia
	Private pnlCorPop, pnlCorTrivia As Panel
	Private lblCorTrivia, lblCorName, lblCorStat, lblCongrat As Label
	Private imgCorAnimal As ImageView
	Private scroCorTrivia As ScrollView
	Private btnNext, btnSubmit As Button
	Private edtName As EditText
	
	Private pnlSet As Panel
	Private pnlVol As Panel
	Private pnlCur As Panel
	Private tglFull As ToggleButton
	Private btnReset As Button
	Private btnSet As Button
	Private pnlBg As Panel
	Dim setclick As Boolean
	Dim finalscore As Int
	Private btnClearHS As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Dim curID = StateManager.GetSetting2("currentid","0") As String
	Dim cursor As Cursor
	Dim bd1, bd2, bd3, bd4 As BitmapDrawable
	
	If File.Exists(File.DirInternal,"db") == False Then
		File.Copy(File.DirAssets, "db.db", File.DirInternal, "db")
		StateManager.SetSetting("currentid", "0")
		StateManager.SaveSettings
	End If
	If FirstTime Then
		db.Initialize(File.DirInternal, "db",False)
		play.Initialize2("play")
	End If
	If count(0) == 0 Then
		reset
	End If
	screen.KeepAlive(True)
	screen.PartialLock
	'initialize popup views
	pnlCorPop.Initialize("pnlCorPop")
	pnlCorTrivia.Initialize("")
	lblCorStat.Initialize("")
	lblCorName.Initialize("")
	lblCorTrivia.Initialize("")
	imgCorAnimal.Initialize("")
	scroCorTrivia.Initialize(1000dip)
	btnNext.Initialize("btnNext")
	edtName.Initialize("")
	btnSubmit.Initialize("btnSubmit")
	lblCongrat.Initialize("")
	pnlBg.Initialize("pnlBg")
	
	Activity.LoadLayout("2")

	lblTrivia.Initialize("")
	If curID == "0" Then
		new
	Else
		cursor = db.ExecQuery("SELECT * FROM game WHERE id='" & curID & "'")
		cursor.Position = 0
		thislevel = cursor.GetString("id")
		pic1 = cursor.GetString("1")
		pic2 = cursor.GetString("2")
		pic3 = cursor.GetString("3")
		pic4 = cursor.GetString("4")
		sound = cursor.GetString("sound")
		ans = cursor.GetString("ans")
		anPic = cursor.GetString(ans)
		trivia = cursor.GetString("trivia")
		animalName = cursor.GetString("name")
		'done reading database so we close the cursor
		cursor.Close
		'loading sound question to meadiaplayer
		play.Load(File.DirAssets, function.getSndExt(sound))
		'intializing the 4 pictures
		bd1.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic1), img1.Width, img1.Height))
		bd2.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic2), img1.Width, img1.Height))
		bd3.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic3), img1.Width, img1.Height))
		bd4.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic4), img1.Width, img1.Height))
		'setting pictures to 4 imageview
		img1.Background = bd1
		img2.Background = bd2
		img3.Background = bd3
		img4.Background = bd4
		'fade in animation
		aniFade.FadeIn("", fadedur)
		aniFade.StartAnim(pnlPics)
	End If
End Sub

Sub Activity_Resume
	Dim fstat = StateManager.GetSetting2("fullscreen", "0"), maxvol As Int
	Dim vol = StateManager.GetSetting2("volume", "5") As Int
	If fstat == 1 Then
		function.FullScreen(True, "Play")
	Else
		function.FullScreen(False, "Play")
	End If
	Activity.Invalidate
	lblLevel.Text = count(1)
	animate.Initialize
	play.SetVolume(vol/10, vol/10)
	
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	If play.IsPlaying Then
		play.Stop
	End If
	screen.ReleaseKeepAlive
	screen.ReleasePartialLock
End Sub

Sub new
	Dim totalrows , randlevel As Int
	Dim cursor As Cursor
	Dim bd1, bd2, bd3, bd4 As BitmapDrawable
	
	'get all unaswered question
	cursor = db.ExecQuery("SELECT * FROM game WHERE STAT='0'")
	totalrows = cursor.RowCount
	'if remaining row is 1, set position to first result
	'else randomize result
	If totalrows == 1 Then
		cursor.Position = 0
	Else
		randlevel = Rnd(0,totalrows)
		cursor.Position = randlevel
	End If
	'reload panel
	pnlPics.Invalidate
	'store data to variables
	thislevel = cursor.GetString("id")
	pic1 = cursor.GetString("1")
	pic2 = cursor.GetString("2")
	pic3 = cursor.GetString("3")
	pic4 = cursor.GetString("4")
	sound = cursor.GetString("sound")
	ans = cursor.GetString("ans")
	anPic = cursor.GetString(ans)
	trivia = cursor.GetString("trivia")
	animalName = cursor.GetString("name")
	'done reading database so we close the cursor
	cursor.Close
	
	Log("id: " & thislevel & CRLF & "answer: " & ans)
	'loading sound question to meadiaplayer
	play.Load(File.DirAssets, function.getSndExt(sound))
	'intializing the 4 pictures
	bd1.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic1), img1.Width, img1.Height))
	bd2.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic2), img1.Width, img1.Height))
	bd3.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic3), img1.Width, img1.Height))
	bd4.Initialize(LoadBitmapSample(File.DirAssets, function.getImgExt(pic4), img1.Width, img1.Height))
	'setting pictures to 4 imageview
	img1.Background = bd1
	img2.Background = bd2
	img3.Background = bd3
	img4.Background = bd4
	'fade in animation
	aniFade.FadeIn("", fadedur)
	aniFade.StartAnim(pnlPics)
	
	StateManager.SetSetting("currentid", thislevel)
	StateManager.SaveSettings
End Sub

Sub btnEvent_Click
	Dim su As StringUtils
	Dim fheight As Float
	Private btnSender As Button
	Dim name As String
	Dim border, enabled3, pressed3 As ColorDrawable
	Dim sld, sld2 As StateListDrawable
	Dim enabled, pressed, enabled2, pressed2 As BitmapDrawable
	
	'initializing background(blue) with border(yellow)
	border.Initialize2(0xFF28507B, 0, 4dip, Colors.Yellow)
	Try
	
	btnSender = Sender
	Select btnSender.Tag
	
	'if correct
	
	Case ans
		If play.IsPlaying Then
			play.Stop
		End If
		'load and play correct sound
		play.Load(File.DirAssets, "correct-sound-effect.mp3")
		play.Play
		
		
		
		enabled.Initialize(LoadBitmapSample(File.DirAssets, "nextlevel_button.png", 50%x, 10%x))
		pressed.Initialize(LoadBitmapSample(File.DirAssets, "nextlevel_button1.png", 50%x, 10%x))
		sld.Initialize
		sld.AddState(sld.State_Pressed, pressed)
		sld.AddState(sld.State_Enabled, enabled)
		
		'Popup trivia
		'popup bg
		Activity.AddView(pnlCorPop, 0, 9%x, 100%x, 100%y - 9%x)
		pnlCorPop.Color = 0x64000000
		'trivia
		pnlCorPop.AddView(pnlCorTrivia, 2%x, 25%x, 96%x, 96%x)
		pnlCorTrivia.Background = border
		'status
		pnlCorTrivia.AddView(lblCorStat, 60%x, 2%x, 35%x, 4%x)
		lblCorStat.Gravity = Gravity.RIGHT
		lblCorStat.TextSize = (10%x/100)*20
		lblCorStat.TextColor = Colors.White
		lblCorStat.Text = checkLib(thislevel)
		'animals name
		pnlCorTrivia.AddView(lblCorName, 28%x, 6%x, 40%x, 7%x) 
		lblCorName.Text = animalName
		lblCorName.TextColor = Colors.White
		lblCorName.Gravity = Gravity.CENTER_HORIZONTAL
		lblCorName.TextSize = (10%x/100)*35
		'scrollview info
		pnlCorTrivia.AddView(scroCorTrivia, 2%x, 13%x, 92%x, 70%x)
		scroCorTrivia.Panel.AddView(imgCorAnimal, 22%x, 0, 50%x, 50%x)
		imgCorAnimal.Bitmap = LoadBitmapSample(File.DirAssets, function.getImgExt(anPic), imgCorAnimal.Width, imgCorAnimal.Height)
		imgCorAnimal.Gravity = Gravity.FILL
		scroCorTrivia.Panel.AddView(lblCorTrivia, 2%x, 50%x, 90%x, 90%x)
		lblCorTrivia.Text = trivia
		lblCorTrivia.Gravity = Gravity.CENTER_HORIZONTAL
		lblCorTrivia.TextSize = (9%x/100)*40
		scroCorTrivia.Panel.Height = 150%x
		scroCorTrivia.ScrollPosition = 0
		'next level button
		pnlCorTrivia.AddView(btnNext,23%x, 84%x, 50%x, 10%x)
		btnNext.Background = sld
'		btnNext.Typeface = Typeface.DEFAULT_BOLD
'		btnNext.TextSize = (10%x/100)*30
'		btnNext.TextColor = Colors.White
'		btnNext.Gravity = Gravity.CENTER_HORIZONTAL
		'auto resize label and scrollview panel height
		fheight = su.MeasureMultilineTextHeight(lblCorTrivia, lblCorTrivia.Text)
		lblCorTrivia.Height = fheight
		scroCorTrivia.Panel.Height = imgCorAnimal.Height + fheight
		
		'update status to answered(1) and add trivia to library
		db.ExecNonQuery("UPDATE game SET stat='1', lib='1' WHERE id='" & thislevel &"'")
		'update score
		lblLevel.Text = count(1)
		'stop the play animation
		animate.Stop
		btnPlay.SetBackgroundImage(LoadBitmapSample(File.DirAssets,"volume_3.png", btnPlay.Width, btnPlay.Height))
		
		'check if finished
		If count(0) == 0 Then
		
			enabled3.Initialize(0xFF1367B2, 0)
			pressed3.Initialize(0xFF0A1015, 0)			
			sld2.Initialize
			sld2.AddState(sld2.State_Pressed, pressed3)
			sld2.AddState(sld2.State_Enabled, enabled3)
			btnNext.RemoveView
			
			pnlCorTrivia.AddView(lblCongrat, 2%x, 2%x, 60%x, 4%x)
			lblCongrat.Text = "Congratulations! You completed all level!"
			lblCongrat.TextSize = (10%x/100)*17
			lblCongrat.TextColor = Colors.White
			'edittext
			pnlCorTrivia.AddView(edtName, 2%x, 84%x, 60%x, 10%x)
			edtName.Color = 0xFF233451
			edtName.Text = "Player" & playerNum
			edtName.TextSize = (10%x/100)*30
			edtName.SingleLine = True
			edtName.RequestFocus
			'submit button
			pnlCorTrivia.AddView(btnSubmit, 62%x, 84%x, 32%x, 10%x)
			btnSubmit.Background = sld2
			btnSubmit.Text = "SUBMIT"
			btnSubmit.Typeface = Typeface.DEFAULT_BOLD
			btnSubmit.TextSize = (10%x/100)*30
			btnSubmit.TextColor = Colors.White
			btnSubmit.Gravity = Gravity.CENTER
		
			finalscore = count(1)
			reset
		End If
		'animating trivia from top to original position 
		anim2.SlideFromTop("", slideval, durval)
		anim2.StartAnim(pnlCorTrivia)
		
		
	'if wrong
	Case Else
		If play.IsPlaying Then
			play.Stop
		End If
		'play the wrong sound
		play.Load(File.DirAssets, "fail-sound-effect.mp3")
		play.Play
		
		enabled3.Initialize(0xFF1367B2, 0)
		pressed3.Initialize(0xFF0A1015, 0)
		sld2.Initialize
		sld2.AddState(sld2.State_Pressed, pressed3)
		sld2.AddState(sld2.State_Enabled, enabled3)
		'popup bg
		Activity.AddView(pnlCorPop, 0, 9%x, 100%x, 100%y - 9%x)
		pnlCorPop.Color = 0x64000000
		'message panel
		pnlCorPop.AddView(pnlCorTrivia, 2%x, 40%x, 96%x, 23%x)
		pnlCorTrivia.Background = border
		'message
		pnlCorTrivia.AddView(lblCongrat,2%x, 2%x, 92%x, 7%x)
		lblCongrat.Text = "Sorry, You Lose."
		lblCongrat.TextColor = Colors.White
		lblCongrat.TextSize = (lblCongrat.Height/100)*50		
		lblCongrat.Gravity = Gravity.CENTER
		'edittext
		pnlCorTrivia.AddView(edtName, 2%x, 11%x, 60%x, 10%x)
		edtName.Color = 0xFF233451
		edtName.Text = "Player" & playerNum
		edtName.TextSize = (10%x/100)*30
		edtName.SingleLine = True
		edtName.RequestFocus
		'submit button
		pnlCorTrivia.AddView(btnSubmit, 62%x, 11%x, 32%x, 10%x)
		btnSubmit.Background = sld2
		btnSubmit.Text = "SUBMIT"
		btnSubmit.Typeface = Typeface.DEFAULT_BOLD
		btnSubmit.TextSize = (10%x/100)*30
		btnSubmit.TextColor = Colors.White
		btnSubmit.Gravity = Gravity.CENTER
		
		anim2.SlideFromTop("", slideval, durval)
		anim2.StartAnim(pnlCorTrivia)
		'stop the animation
		animate.Stop
		btnPlay.SetBackgroundImage(LoadBitmapSample(File.DirAssets,"volume_3.png", btnPlay.Width, btnPlay.Height))
		'setting the score to variable then reset the game
		finalscore = count(1)
		reset
	End Select
	
	Catch
		'do nothing
	End Try
End Sub

Sub btnPlay_Click
	Dim vol = StateManager.GetSetting2("volume", "5") As Float
	'adding frames to playbutton
	animate.AddFrame(LoadBitmapSample(File.DirAssets, "volume_0.png", btnPlay.Width, btnPlay.Height), 150)
	animate.AddFrame(LoadBitmapSample(File.DirAssets, "volume_1.png", btnPlay.Width, btnPlay.Height), 150)
	animate.AddFrame(LoadBitmapSample(File.DirAssets, "volume_2.png", btnPlay.Width, btnPlay.Height), 150)
	animate.AddFrame(LoadBitmapSample(File.DirAssets, "volume_3.png", btnPlay.Width, btnPlay.Height), 150)
	animate.OneShot = False
	btnPlay.Background = animate
	'setting the mediaplayer volume
	play.SetVolume(vol/10, vol/10)
	'playing the sound question
	play.Play
	'starting the play animation
	animate.Start
	
	
End Sub
'back button
Sub btnBack_Click
	Activity.Finish
	StartActivity(Main)
	function.SetAnimation("animtoright", "animfromleft")
End Sub
'reset game progress function
Sub reset
	db.ExecNonQuery("UPDATE game SET stat='0'")
	StateManager.SetSetting("currentid", "0")
	StateManager.SaveSettings
End Sub
'count answered or unanswered function
Sub	count(value As Int) As Int
	Dim row As Cursor
	Dim all As Int
	row = db.ExecQuery("SELECT * FROM game WHERE stat='" & value & "'")
	all = row.RowCount
	Return all
End Sub
'get the current player number extension eg. Player12, 12 is the pnum
Sub playerNum As Int
	Dim num = StateManager.GetSetting2("pnum", "0"), num2 As Int
	num2 = num + 1
	Return num2
End Sub
'stop the play button animation when the sound stop loading
Sub play_Complete
	animate.Stop
	btnPlay.SetBackgroundImage(LoadBitmapSample(File.DirAssets, "volume_3.png", btnPlay.Width, btnPlay.Height))
End Sub
'saving score
Sub savescore(pname As String)
	Dim oldnam, oldsco As String
	Dim t = finalscore, playN = StateManager.GetSetting2("pnum", "0") As Int
	Dim cursor2 As Cursor
	'check if name is blank then set the default name
	If pname == "" Then
		pname = "Player" & playerNum
	End If
	'check name if deafault then add 1 to pnum 
	If pname.StartsWith("Player") Then
		StateManager.SetSetting("pnum", playN + 1)
		StateManager.SaveSettings
	End If
	'check the database if the current score is already existing in database
	'then modify
	'Else add
	cursor2 = db.ExecQuery("SELECT * FROM hscore WHERE rscore='" & t & "'")
	If cursor2.RowCount == 1 Then
		cursor2.Position = 0
		oldnam = cursor2.GetString("name")
		oldsco = cursor2.GetString("score")
		cursor2.Close
		db.ExecNonQuery("UPDATE hscore SET	name='" & oldnam & CRLF & pname & "', score='" & oldsco & CRLF & t & "' WHERE rscore='" & t & "'")
	Else
		db.ExecNonQuery2("INSERT INTO hscore VALUES (?,?,?)", Array As Object(pname, t, t))
	End If
End Sub

Sub pnlCorPop_Click
	'do nothing
End Sub
'check the trivia if already existing in library
Sub checkLib(val As String) As String
	Dim cursor As Cursor
	Dim lib As Int
	Dim stat As String
	cursor = db.ExecQuery("SELECT * FROM game WHERE id='" & thislevel & "'")
	cursor.Position = 0
	lib = cursor.GetInt("lib")
	If lib == 1 Then
		stat = "(Already in Library!)"
	Else
		stat = "(Added to Library!)"
	End If
	Return stat
End Sub

Sub btnNext_Click
	anim2.SlideToTop("anim2", slideval, durval)
	anim2.StartAnim(pnlCorTrivia)
End Sub

Sub anim2_animationend
	pnlCorPop.RemoveView
	pnlCorTrivia.RemoveView
	lblCorName.RemoveView
	lblCorTrivia.RemoveView
	imgCorAnimal.RemoveView
	lblCorStat.RemoveView
	scroCorTrivia.RemoveView
	btnNext.RemoveView
	aniFade.FadeOut("fade", fadedur)
	aniFade.StartAnim(pnlPics)
End Sub

Sub fade_animationend
	new
End Sub

Sub btnSubmit_Click
	Dim playername As String
	playername = edtName.Text
	savescore(playername)
	Activity.Finish
	StartActivity(Score)
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean
    If KeyCode = KeyCodes.KEYCODE_BACK Then
		Return True
	Else If KeyCode = KeyCodes.KEYCODE_MENU And btnSet.Enabled == True Then
		btnSet_Click
	End If
End Sub


Sub btnSet_Click
	Dim sld As StateListDrawable
	Dim checked As BitmapDrawable
	Dim unchecked As ColorDrawable
	Dim sld2 As StateListDrawable
	Dim enabled2, pressed2 As BitmapDrawable
	
	enabled2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button.png", 40%x, 13%x))
	pressed2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button1.png", 40%x, 13%x))
	sld2.Initialize
	sld2.AddState(sld.State_Pressed, pressed2)
	sld2.AddState(sld.State_Enabled, enabled2)
	
	checked.Initialize(LoadBitmap(File.DirAssets, "tglc.png"))
	unchecked.Initialize(Colors.White, 0)
	sld.Initialize
	sld.AddState(sld.State_Unchecked, unchecked)
	sld.AddState(sld.State_Checked, checked)
	Dim fstat2 = StateManager.GetSetting2("fullscreen", "0") As Int
	Dim s As Boolean
	Dim vol = StateManager.GetSetting2("volume", "5") As Float
	If fstat2 == 1 Then
		s = True
	Else
		s = False
	End If
	If setclick == False Then
		Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9%x)
		pnlBg.LoadLayout("5")
		pnlCur.Width = (vol/10)*pnlVol.Width
		pnlBg.Color = 0x64000000
		tglFull.Background = sld
		tglFull.Checked = s
		btnReset.Background = sld2
		anim2.SlideFromTop("", slideval, durval)
		anim2.StartAnim(pnlSet)
		setclick = True
	Else
		anim2.SlideToTop("anim4",slideval, durval)
		anim2.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlBg_Click
	If setclick == True Then
		anim2.SlideToTop("anim4",slideval, durval)
		anim2.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlSet_Click

End Sub


Sub pnlVol_Touch (Action As Int, X As Float, Y As Float)
	Dim a As Float
	If X < 0 Then
		X = 0
	Else If X > pnlVol.Width Then
		X = pnlVol.Width
	End If
	a = (X/pnlVol.Width)*10
	StateManager.SetSetting("volume", a)
	StateManager.SaveSettings
	pnlCur.Width = X
	play.SetVolume(a/10, a/10)
End Sub

Sub tglFull_CheckedChange(Checked As Boolean)
	If Checked Then
		StateManager.SetSetting("fullscreen", "1")
	Else
		StateManager.SetSetting("fullscreen", "0")
	End If
	StateManager.SaveSettings
	function.FullScreen(Checked, "Play")
	Activity.Invalidate
End Sub

Sub btnReset_Click
	Dim res As Int
	res = Msgbox2("Do you really want reset the game?"&CRLF&"*note that data will deleted including game progress and library.", "Reset game", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		function.resetall(db)
		ToastMessageShow("Success!", False)
		new
		lblLevel.Text = "0"
	Else
		Return True
	End If
End Sub

Sub anim4_animationend
	pnlBg.RemoveAllViews
	pnlBg.RemoveView
	btnSet.Enabled = True
End Sub

Sub btnClearHS_Click
	Dim res As Int
	res = Msgbox2("Do you really want to clear the highscore?", "Reset", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		db.ExecNonQuery("DELETE FROM hscore")
		StateManager.SetSetting("pnum", "0")
		StateManager.SaveSettings
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub
