package com.BUPC.YPSF.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_4{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.5d);
anywheresoftware.b4a.keywords.LayoutBuilder.scaleAll(views);
views.get("pnltop").vw.setWidth((int)((100d / 100 * width)));
views.get("pnltop").vw.setHeight((int)((9d / 100 * width)));
views.get("pnltop").vw.setTop((int)(0d));
views.get("pnltop").vw.setLeft((int)(0d));
views.get("btnback").vw.setTop((int)(0d));
views.get("btnback").vw.setLeft((int)(0d));
views.get("btnback").vw.setWidth((int)((9d / 100 * width)));
views.get("btnback").vw.setHeight((int)((9d / 100 * width)));
views.get("lbltitle").vw.setLeft((int)((50d / 100 * width) - (views.get("lbltitle").vw.getWidth() / 2)));
views.get("lbltitle").vw.setTop((int)(0d));
views.get("lbltitle").vw.setHeight((int)((views.get("pnltop").vw.getHeight())));
views.get("lbltitle").vw.setWidth((int)((23.5d / 100 * width)));
views.get("lbltitle").vw.setLeft((int)((50d / 100 * width) - (views.get("lbltitle").vw.getWidth() / 2)));
views.get("btnset").vw.setTop((int)(0d));
views.get("btnset").vw.setLeft((int)((views.get("pnltop").vw.getLeft() + views.get("pnltop").vw.getWidth())-(9d / 100 * width)));
views.get("btnset").vw.setHeight((int)((9d / 100 * width)));
views.get("btnset").vw.setWidth((int)((9d / 100 * width)));
views.get("listtrivia").vw.setTop((int)((views.get("pnltop").vw.getTop() + views.get("pnltop").vw.getHeight())));
views.get("listtrivia").vw.setHeight((int)((100d / 100 * height) - ((views.get("pnltop").vw.getTop() + views.get("pnltop").vw.getHeight()))));
views.get("listtrivia").vw.setLeft((int)(0d));
views.get("listtrivia").vw.setWidth((int)((100d / 100 * width) - (0d)));

}
}