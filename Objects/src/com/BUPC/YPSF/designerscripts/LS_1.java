package com.BUPC.YPSF.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_1{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
//BA.debugLineNum = 2;BA.debugLine="AutoScaleRate(0.5)"[1/General script]
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.5d);
//BA.debugLineNum = 3;BA.debugLine="AutoScaleAll"[1/General script]
anywheresoftware.b4a.keywords.LayoutBuilder.scaleAll(views);
//BA.debugLineNum = 5;BA.debugLine="pnlTop.Width = 100%x"[1/General script]
views.get("pnltop").vw.setWidth((int)((100d / 100 * width)));
//BA.debugLineNum = 6;BA.debugLine="pnlTop.Height = 9%x"[1/General script]
views.get("pnltop").vw.setHeight((int)((9d / 100 * width)));
//BA.debugLineNum = 7;BA.debugLine="pnlTop.Top = 0"[1/General script]
views.get("pnltop").vw.setTop((int)(0d));
//BA.debugLineNum = 8;BA.debugLine="pnlTop.Left = 0"[1/General script]
views.get("pnltop").vw.setLeft((int)(0d));
//BA.debugLineNum = 10;BA.debugLine="btnExit.Top = 0"[1/General script]
views.get("btnexit").vw.setTop((int)(0d));
//BA.debugLineNum = 11;BA.debugLine="btnExit.Left = 0"[1/General script]
views.get("btnexit").vw.setLeft((int)(0d));
//BA.debugLineNum = 12;BA.debugLine="btnExit.Width = 9%x"[1/General script]
views.get("btnexit").vw.setWidth((int)((9d / 100 * width)));
//BA.debugLineNum = 13;BA.debugLine="btnExit.Height = 9%x"[1/General script]
views.get("btnexit").vw.setHeight((int)((9d / 100 * width)));
//BA.debugLineNum = 15;BA.debugLine="btnSet.Top = 0"[1/General script]
views.get("btnset").vw.setTop((int)(0d));
//BA.debugLineNum = 16;BA.debugLine="btnSet.Left = pnlTop.Right - 9%x"[1/General script]
views.get("btnset").vw.setLeft((int)((views.get("pnltop").vw.getLeft() + views.get("pnltop").vw.getWidth())-(9d / 100 * width)));
//BA.debugLineNum = 17;BA.debugLine="btnSet.Height = 9%x"[1/General script]
views.get("btnset").vw.setHeight((int)((9d / 100 * width)));
//BA.debugLineNum = 18;BA.debugLine="btnSet.Width = 9%x"[1/General script]
views.get("btnset").vw.setWidth((int)((9d / 100 * width)));
//BA.debugLineNum = 20;BA.debugLine="imgBanner.Top = pnlTop.Bottom"[1/General script]
views.get("imgbanner").vw.setTop((int)((views.get("pnltop").vw.getTop() + views.get("pnltop").vw.getHeight())));
//BA.debugLineNum = 21;BA.debugLine="imgBanner.Width = 100%x"[1/General script]
views.get("imgbanner").vw.setWidth((int)((100d / 100 * width)));
//BA.debugLineNum = 22;BA.debugLine="imgBanner.Height = 40%x"[1/General script]
views.get("imgbanner").vw.setHeight((int)((40d / 100 * width)));
//BA.debugLineNum = 24;BA.debugLine="pnlButton.SetTopAndBottom(imgBanner.Bottom, 100%y)"[1/General script]
views.get("pnlbutton").vw.setTop((int)((views.get("imgbanner").vw.getTop() + views.get("imgbanner").vw.getHeight())));
views.get("pnlbutton").vw.setHeight((int)((100d / 100 * height) - ((views.get("imgbanner").vw.getTop() + views.get("imgbanner").vw.getHeight()))));
//BA.debugLineNum = 25;BA.debugLine="pnlButton.SetLeftAndRight(10%x, 90%x)"[1/General script]
views.get("pnlbutton").vw.setLeft((int)((10d / 100 * width)));
views.get("pnlbutton").vw.setWidth((int)((90d / 100 * width) - ((10d / 100 * width))));
//BA.debugLineNum = 27;BA.debugLine="btnPlay.Top = 3%x"[1/General script]
views.get("btnplay").vw.setTop((int)((3d / 100 * width)));
//BA.debugLineNum = 28;BA.debugLine="btnPlay.Left = 3%x"[1/General script]
views.get("btnplay").vw.setLeft((int)((3d / 100 * width)));
//BA.debugLineNum = 29;BA.debugLine="btnPlay.Width = pnlButton.Width - 6%x"[1/General script]
views.get("btnplay").vw.setWidth((int)((views.get("pnlbutton").vw.getWidth())-(6d / 100 * width)));
//BA.debugLineNum = 30;BA.debugLine="btnPlay.Height = 17%x"[1/General script]
views.get("btnplay").vw.setHeight((int)((17d / 100 * width)));
//BA.debugLineNum = 32;BA.debugLine="btnScore.Top = btnPlay.Bottom + 3%x"[1/General script]
views.get("btnscore").vw.setTop((int)((views.get("btnplay").vw.getTop() + views.get("btnplay").vw.getHeight())+(3d / 100 * width)));
//BA.debugLineNum = 33;BA.debugLine="btnScore.Left = 3%x"[1/General script]
views.get("btnscore").vw.setLeft((int)((3d / 100 * width)));
//BA.debugLineNum = 34;BA.debugLine="btnScore.Width = pnlButton.Width - 6%x"[1/General script]
views.get("btnscore").vw.setWidth((int)((views.get("pnlbutton").vw.getWidth())-(6d / 100 * width)));
//BA.debugLineNum = 35;BA.debugLine="btnScore.Height = 17%x"[1/General script]
views.get("btnscore").vw.setHeight((int)((17d / 100 * width)));
//BA.debugLineNum = 37;BA.debugLine="btnLibrary.Top = btnScore.Bottom + 3%x"[1/General script]
views.get("btnlibrary").vw.setTop((int)((views.get("btnscore").vw.getTop() + views.get("btnscore").vw.getHeight())+(3d / 100 * width)));
//BA.debugLineNum = 38;BA.debugLine="btnLibrary.Left = 3%x"[1/General script]
views.get("btnlibrary").vw.setLeft((int)((3d / 100 * width)));
//BA.debugLineNum = 39;BA.debugLine="btnLibrary.Width = pnlButton.Width - 6%x"[1/General script]
views.get("btnlibrary").vw.setWidth((int)((views.get("pnlbutton").vw.getWidth())-(6d / 100 * width)));
//BA.debugLineNum = 40;BA.debugLine="btnLibrary.Height = 17%x"[1/General script]
views.get("btnlibrary").vw.setHeight((int)((17d / 100 * width)));
//BA.debugLineNum = 42;BA.debugLine="btnTut.Top = btnLibrary.Bottom + 3%x"[1/General script]
views.get("btntut").vw.setTop((int)((views.get("btnlibrary").vw.getTop() + views.get("btnlibrary").vw.getHeight())+(3d / 100 * width)));
//BA.debugLineNum = 43;BA.debugLine="btnTut.Left = 3%x"[1/General script]
views.get("btntut").vw.setLeft((int)((3d / 100 * width)));
//BA.debugLineNum = 44;BA.debugLine="btnTut.Width = pnlButton.Width - 6%x"[1/General script]
views.get("btntut").vw.setWidth((int)((views.get("pnlbutton").vw.getWidth())-(6d / 100 * width)));
//BA.debugLineNum = 45;BA.debugLine="btnTut.Height = 17%x"[1/General script]
views.get("btntut").vw.setHeight((int)((17d / 100 * width)));

}
}