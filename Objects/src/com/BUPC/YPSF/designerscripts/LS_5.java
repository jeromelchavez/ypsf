package com.BUPC.YPSF.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_5{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.5d);
anywheresoftware.b4a.keywords.LayoutBuilder.scaleAll(views);
views.get("pnlset").vw.setTop((int)((10d / 100 * width)));
views.get("pnlset").vw.setLeft((int)((2d / 100 * width)));
views.get("pnlset").vw.setWidth((int)((96d / 100 * width)));
views.get("pnlset").vw.setHeight((int)((100d / 100 * height)-(40d / 100 * width)));
views.get("lblvol").vw.setTop((int)((10d / 100 * width)));
views.get("lblvol").vw.setWidth((int)((40d / 100 * width)));
views.get("lblvol").vw.setLeft((int)((48d / 100 * width) - (views.get("lblvol").vw.getWidth() / 2)));
views.get("lblvol").vw.setHeight((int)((10d / 100 * width)));
views.get("pnlvol").vw.setTop((int)((22d / 100 * width)));
views.get("pnlvol").vw.setWidth((int)((60d / 100 * width)));
views.get("pnlvol").vw.setLeft((int)((48d / 100 * width) - (views.get("pnlvol").vw.getWidth() / 2)));
views.get("pnlvol").vw.setHeight((int)((10d / 100 * width)));
views.get("pnlcur").vw.setLeft((int)(0d));
views.get("pnlcur").vw.setTop((int)(0d));
views.get("pnlcur").vw.setHeight((int)((10d / 100 * width)));
views.get("lblfull").vw.setTop((int)((40d / 100 * width)));
views.get("lblfull").vw.setLeft((int)((25d / 100 * width)));
views.get("lblfull").vw.setWidth((int)((30d / 100 * width)));
views.get("lblfull").vw.setHeight((int)((10d / 100 * width)));
views.get("tglfull").vw.setTop((int)((41d / 100 * width)));
views.get("tglfull").vw.setLeft((int)((57d / 100 * width)));
views.get("tglfull").vw.setWidth((int)((9d / 100 * width)));
views.get("tglfull").vw.setHeight((int)((9d / 100 * width)));
views.get("btnclearhs").vw.setTop((int)((55d / 100 * width)));
views.get("btnclearhs").vw.setLeft((int)((28d / 100 * width)));
views.get("btnclearhs").vw.setWidth((int)((40d / 100 * width)));
views.get("btnclearhs").vw.setHeight((int)((13d / 100 * width)));
views.get("btnreset").vw.setTop((int)((70d / 100 * width)));
views.get("btnreset").vw.setWidth((int)((40d / 100 * width)));
views.get("btnreset").vw.setLeft((int)((28d / 100 * width)));
views.get("btnreset").vw.setHeight((int)((13d / 100 * width)));

}
}