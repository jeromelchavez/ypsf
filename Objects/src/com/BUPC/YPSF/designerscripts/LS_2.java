package com.BUPC.YPSF.designerscripts;
import anywheresoftware.b4a.objects.TextViewWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.BA;


public class LS_2{

public static void LS_general(java.util.LinkedHashMap<String, anywheresoftware.b4a.keywords.LayoutBuilder.ViewWrapperAndAnchor> views, int width, int height, float scale) {
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.3);
String _imgh="";
anywheresoftware.b4a.keywords.LayoutBuilder.setScaleRate(0.5d);
anywheresoftware.b4a.keywords.LayoutBuilder.scaleAll(views);
views.get("pnltop").vw.setWidth((int)((100d / 100 * width)));
views.get("pnltop").vw.setHeight((int)((9d / 100 * width)));
views.get("pnltop").vw.setTop((int)(0d));
views.get("pnltop").vw.setLeft((int)(0d));
views.get("btnback").vw.setTop((int)(0d));
views.get("btnback").vw.setLeft((int)(0d));
views.get("btnback").vw.setWidth((int)((9d / 100 * width)));
views.get("btnback").vw.setHeight((int)((9d / 100 * width)));
views.get("btnset").vw.setTop((int)(0d));
views.get("btnset").vw.setHeight((int)((9d / 100 * width) - (0d)));
views.get("btnset").vw.setWidth((int)((9d / 100 * width)));
views.get("btnset").vw.setLeft((int)((views.get("pnltop").vw.getLeft() + views.get("pnltop").vw.getWidth())-(9d / 100 * width)));
views.get("lbllevel").vw.setTop((int)(0d));
views.get("lbllevel").vw.setWidth((int)((13d / 100 * width)));
views.get("lbllevel").vw.setHeight((int)((9d / 100 * width)));
views.get("lbllevel").vw.setLeft((int)((50d / 100 * width) - (views.get("lbllevel").vw.getWidth() / 2)));
views.get("pnlpics").vw.setTop((int)((18d / 100 * width)));
views.get("pnlpics").vw.setHeight((int)((80d / 100 * width)));
views.get("pnlpics").vw.setWidth((int)((80d / 100 * width)));
views.get("pnlpics").vw.setLeft((int)((50d / 100 * width) - (views.get("pnlpics").vw.getWidth() / 2)));
_imgh = BA.NumberToString((39d / 100 * width));
views.get("img1").vw.setTop((int)(0d));
views.get("img1").vw.setLeft((int)(0d));
views.get("img1").vw.setHeight((int)(Double.parseDouble(_imgh)));
views.get("img1").vw.setWidth((int)(Double.parseDouble(_imgh)));
views.get("btnpic1").vw.setTop((int)(0d));
views.get("btnpic1").vw.setLeft((int)(0d));
views.get("btnpic1").vw.setHeight((int)(Double.parseDouble(_imgh)));
views.get("btnpic1").vw.setWidth((int)(Double.parseDouble(_imgh)));
views.get("img2").vw.setTop((int)(0d));
views.get("img2").vw.setLeft((int)((views.get("pnlpics").vw.getWidth())-Double.parseDouble(_imgh)));
views.get("img2").vw.setHeight((int)(Double.parseDouble(_imgh)));
views.get("img2").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 47;BA.debugLine="btnPic2.Top = 0"[2/General script]
views.get("btnpic2").vw.setTop((int)(0d));
//BA.debugLineNum = 48;BA.debugLine="btnPic2.Left = pnlPics.Width - imgH"[2/General script]
views.get("btnpic2").vw.setLeft((int)((views.get("pnlpics").vw.getWidth())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 49;BA.debugLine="btnPic2.Height = imgH"[2/General script]
views.get("btnpic2").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 50;BA.debugLine="btnPic2.Width = imgH"[2/General script]
views.get("btnpic2").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 53;BA.debugLine="img3.Top = pnlPics.Height - imgH"[2/General script]
views.get("img3").vw.setTop((int)((views.get("pnlpics").vw.getHeight())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 54;BA.debugLine="img3.Left = 0"[2/General script]
views.get("img3").vw.setLeft((int)(0d));
//BA.debugLineNum = 55;BA.debugLine="img3.Height = imgH"[2/General script]
views.get("img3").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 56;BA.debugLine="img3.Width = imgH"[2/General script]
views.get("img3").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 58;BA.debugLine="btnPic3.Top = pnlPics.Height - imgH"[2/General script]
views.get("btnpic3").vw.setTop((int)((views.get("pnlpics").vw.getHeight())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 59;BA.debugLine="btnPic3.Left = 0"[2/General script]
views.get("btnpic3").vw.setLeft((int)(0d));
//BA.debugLineNum = 60;BA.debugLine="btnPic3.Height = imgH"[2/General script]
views.get("btnpic3").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 61;BA.debugLine="btnPic3.Width = imgH"[2/General script]
views.get("btnpic3").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 64;BA.debugLine="img4.Top = pnlPics.Height - imgH"[2/General script]
views.get("img4").vw.setTop((int)((views.get("pnlpics").vw.getHeight())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 65;BA.debugLine="img4.Left = pnlPics.Width - imgH"[2/General script]
views.get("img4").vw.setLeft((int)((views.get("pnlpics").vw.getWidth())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 66;BA.debugLine="img4.Height = imgH"[2/General script]
views.get("img4").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 67;BA.debugLine="img4.Width = imgH"[2/General script]
views.get("img4").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 69;BA.debugLine="btnPic4.Top = pnlPics.Height - imgH"[2/General script]
views.get("btnpic4").vw.setTop((int)((views.get("pnlpics").vw.getHeight())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 70;BA.debugLine="btnPic4.Left = pnlPics.Width - imgH"[2/General script]
views.get("btnpic4").vw.setLeft((int)((views.get("pnlpics").vw.getWidth())-Double.parseDouble(_imgh)));
//BA.debugLineNum = 71;BA.debugLine="btnPic4.Height = imgH"[2/General script]
views.get("btnpic4").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 72;BA.debugLine="btnPic4.Width = imgH"[2/General script]
views.get("btnpic4").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 75;BA.debugLine="btnPlay.Top = pnlPics.Bottom + 10%x"[2/General script]
views.get("btnplay").vw.setTop((int)((views.get("pnlpics").vw.getTop() + views.get("pnlpics").vw.getHeight())+(10d / 100 * width)));
//BA.debugLineNum = 76;BA.debugLine="btnPlay.Height = imgH"[2/General script]
views.get("btnplay").vw.setHeight((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 77;BA.debugLine="btnPlay.Width = imgH"[2/General script]
views.get("btnplay").vw.setWidth((int)(Double.parseDouble(_imgh)));
//BA.debugLineNum = 78;BA.debugLine="btnPlay.HorizontalCenter = 50%x"[2/General script]
views.get("btnplay").vw.setLeft((int)((50d / 100 * width) - (views.get("btnplay").vw.getWidth() / 2)));

}
}