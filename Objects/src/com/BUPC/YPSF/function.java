package com.BUPC.YPSF;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.debug.*;

public class function {
private static function mostCurrent = new function();
public static Object getObject() {
    throw new RuntimeException("Code module does not support this method.");
}
 public anywheresoftware.b4a.keywords.Common __c = null;
public com.BUPC.YPSF.main _main = null;
public com.BUPC.YPSF.score _score = null;
public com.BUPC.YPSF.play _play = null;
public com.BUPC.YPSF.statemanager _statemanager = null;
public com.BUPC.YPSF.tutorial _tutorial = null;
public com.BUPC.YPSF.library _library = null;
public static String  _fullscreen(anywheresoftware.b4a.BA _ba,boolean _active,String _activityname) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _obj1 = null;
int _i = 0;
 //BA.debugLineNum = 33;BA.debugLine="Sub FullScreen(Active As Boolean, ActivityName As";
 //BA.debugLineNum = 34;BA.debugLine="Dim obj1 As Reflector";
_obj1 = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 35;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 36;BA.debugLine="i = 1024  'FLAG_FULLSCREEN";
_i = (int) (1024);
 //BA.debugLineNum = 37;BA.debugLine="obj1.Target = obj1.GetMostCurrent(ActivityName";
_obj1.Target = _obj1.GetMostCurrent(_activityname);
 //BA.debugLineNum = 38;BA.debugLine="obj1.Target = obj1.RunMethod(\"getWindow\")";
_obj1.Target = _obj1.RunMethod("getWindow");
 //BA.debugLineNum = 39;BA.debugLine="If Active Then";
if (_active) { 
 //BA.debugLineNum = 40;BA.debugLine="obj1.RunMethod2(\"addFlags\",i,\"java.lang.in";
_obj1.RunMethod2("addFlags",BA.NumberToString(_i),"java.lang.int");
 }else {
 //BA.debugLineNum = 42;BA.debugLine="obj1.RunMethod2(\"clearFlags\",i,\"java.lang.";
_obj1.RunMethod2("clearFlags",BA.NumberToString(_i),"java.lang.int");
 };
 //BA.debugLineNum = 44;BA.debugLine="End Sub";
return "";
}
public static String  _getimgext(anywheresoftware.b4a.BA _ba,String _val) throws Exception{
String _fileext = "";
 //BA.debugLineNum = 9;BA.debugLine="Sub getImgExt(val As String) As String";
 //BA.debugLineNum = 10;BA.debugLine="Dim fileext As String";
_fileext = "";
 //BA.debugLineNum = 11;BA.debugLine="If File.Exists(File.DirAssets, val &\".png\") Then";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_val+".png")) { 
 //BA.debugLineNum = 12;BA.debugLine="fileext = val & \".png\"";
_fileext = _val+".png";
 }else {
 //BA.debugLineNum = 14;BA.debugLine="fileext = val & \".jpg\"";
_fileext = _val+".jpg";
 };
 //BA.debugLineNum = 16;BA.debugLine="Return fileext";
if (true) return _fileext;
 //BA.debugLineNum = 17;BA.debugLine="End Sub";
return "";
}
public static String  _getsndext(anywheresoftware.b4a.BA _ba,String _val2) throws Exception{
String _fileext2 = "";
 //BA.debugLineNum = 19;BA.debugLine="Sub getSndExt(val2 As String) As String";
 //BA.debugLineNum = 20;BA.debugLine="Dim fileext2 As String";
_fileext2 = "";
 //BA.debugLineNum = 21;BA.debugLine="If File.Exists(File.DirAssets, val2 & \".mp3\") The";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_val2+".mp3")) { 
 //BA.debugLineNum = 22;BA.debugLine="fileext2 = val2 & \".mp3\"";
_fileext2 = _val2+".mp3";
 }else if(anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_val2+".wav")) { 
 //BA.debugLineNum = 24;BA.debugLine="fileext2 = val2 & \".wav\"";
_fileext2 = _val2+".wav";
 }else if(anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_val2+".mid")) { 
 //BA.debugLineNum = 26;BA.debugLine="fileext2 = val2 & \".mid\"";
_fileext2 = _val2+".mid";
 }else {
 //BA.debugLineNum = 28;BA.debugLine="fileext2 = val2 & \".ogg\"";
_fileext2 = _val2+".ogg";
 };
 //BA.debugLineNum = 30;BA.debugLine="Return fileext2";
if (true) return _fileext2;
 //BA.debugLineNum = 31;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 3;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 6;BA.debugLine="End Sub";
return "";
}
public static String  _resetall(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.sql.SQL _sql1) throws Exception{
 //BA.debugLineNum = 46;BA.debugLine="Sub resetall(sql1 As SQL)";
 //BA.debugLineNum = 48;BA.debugLine="sql1.ExecNonQuery(\"UPDATE game SET stat='0', lib=";
_sql1.ExecNonQuery("UPDATE game SET stat='0', lib='0'");
 //BA.debugLineNum = 49;BA.debugLine="StateManager.SetSetting(\"currentid\", \"0\")";
mostCurrent._statemanager._setsetting(_ba,"currentid","0");
 //BA.debugLineNum = 50;BA.debugLine="StateManager.SetSetting(\"pnum\", \"0\")";
mostCurrent._statemanager._setsetting(_ba,"pnum","0");
 //BA.debugLineNum = 51;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(_ba);
 //BA.debugLineNum = 52;BA.debugLine="End Sub";
return "";
}
public static String  _setanimation(anywheresoftware.b4a.BA _ba,String _inanimation,String _outanimation) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
String _package = "";
int _in = 0;
int _out = 0;
 //BA.debugLineNum = 54;BA.debugLine="Sub SetAnimation(InAnimation As String, OutAnimati";
 //BA.debugLineNum = 55;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 56;BA.debugLine="Dim package As String";
_package = "";
 //BA.debugLineNum = 57;BA.debugLine="Dim In, out As Int";
_in = 0;
_out = 0;
 //BA.debugLineNum = 58;BA.debugLine="package = r.GetStaticField(\"anywheresoftware.b4a.";
_package = BA.ObjectToString(_r.GetStaticField("anywheresoftware.b4a.BA","packageName"));
 //BA.debugLineNum = 59;BA.debugLine="In = r.GetStaticField(package & \".R$anim\", InA";
_in = (int)(BA.ObjectToNumber(_r.GetStaticField(_package+".R$anim",_inanimation)));
 //BA.debugLineNum = 60;BA.debugLine="out = r.GetStaticField(package & \".R$anim\", Ou";
_out = (int)(BA.ObjectToNumber(_r.GetStaticField(_package+".R$anim",_outanimation)));
 //BA.debugLineNum = 61;BA.debugLine="r.Target = r.GetActivity";
_r.Target = (Object)(_r.GetActivity((_ba.processBA == null ? _ba : _ba.processBA)));
 //BA.debugLineNum = 62;BA.debugLine="r.RunMethod4(\"overridePendingTransition\", Arra";
_r.RunMethod4("overridePendingTransition",new Object[]{(Object)(_in),(Object)(_out)},new String[]{"java.lang.int","java.lang.int"});
 //BA.debugLineNum = 63;BA.debugLine="End Sub";
return "";
}
public static String  _setprogressdrawable(anywheresoftware.b4a.BA _ba,anywheresoftware.b4a.objects.ProgressBarWrapper _p,Object _drawable) throws Exception{
anywheresoftware.b4a.agraham.reflection.Reflection _r = null;
Object _clipdrawable = null;
 //BA.debugLineNum = 65;BA.debugLine="Sub SetProgressDrawable(p As ProgressBar, drawable";
 //BA.debugLineNum = 66;BA.debugLine="Dim r As Reflector";
_r = new anywheresoftware.b4a.agraham.reflection.Reflection();
 //BA.debugLineNum = 67;BA.debugLine="Dim clipDrawable As Object";
_clipdrawable = new Object();
 //BA.debugLineNum = 68;BA.debugLine="clipDrawable = r.CreateObject2(\"android.graphics";
_clipdrawable = _r.CreateObject2("android.graphics.drawable.ClipDrawable",new Object[]{_drawable,(Object)(anywheresoftware.b4a.keywords.Common.Gravity.LEFT),(Object)(1)},new String[]{"android.graphics.drawable.Drawable","java.lang.int","java.lang.int"});
 //BA.debugLineNum = 71;BA.debugLine="r.Target = p";
_r.Target = (Object)(_p.getObject());
 //BA.debugLineNum = 72;BA.debugLine="r.Target = r.RunMethod(\"getProgressDrawable\") 'Ge";
_r.Target = _r.RunMethod("getProgressDrawable");
 //BA.debugLineNum = 73;BA.debugLine="r.RunMethod4(\"setDrawableByLayerId\", _  		Array A";
_r.RunMethod4("setDrawableByLayerId",new Object[]{_r.GetStaticField("android.R$id","progress"),_clipdrawable},new String[]{"java.lang.int","android.graphics.drawable.Drawable"});
 //BA.debugLineNum = 76;BA.debugLine="End Sub";
return "";
}
}
