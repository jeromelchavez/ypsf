package com.BUPC.YPSF;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class tutorial extends Activity implements B4AActivity{
	public static tutorial mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "com.BUPC.YPSF", "com.BUPC.YPSF.tutorial");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (tutorial).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
		BA.handler.postDelayed(new WaitForLayout(), 5);

	}
	private static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "com.BUPC.YPSF", "com.BUPC.YPSF.tutorial");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "com.BUPC.YPSF.tutorial", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (tutorial) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (tutorial) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return tutorial.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (tutorial) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (tutorial) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.objects.MediaPlayerWrapper _mp = null;
public static anywheresoftware.b4a.sql.SQL _db = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnpic1 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnpic2 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnpic4 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnpic3 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnback = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnset = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbllevel = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img1 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img4 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img3 = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnplay = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlpics = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltutbg = null;
public anywheresoftware.b4a.phone.Phone.PhoneWakeState _screen = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlbg = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnltut = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblins = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnnext = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btndone = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnyes = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnno = null;
public giuseppe.salvi.icos.library.ICOSSlideAnimation _anislide = null;
public giuseppe.salvi.icos.library.ICOSFadeAnimation _anifade = null;
public flm.b4a.animationplus.AnimationDrawable _playanim = null;
public static float _slideval = 0f;
public static float _durval = 0f;
public static float _fadedur = 0f;
public anywheresoftware.b4a.objects.ButtonWrapper _btnclearhs = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcorname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcortrivia = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _scrocortrivia = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _scrotut = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgcoranimal = null;
public static String _trivia = "";
public static String _msg1 = "";
public static String _msg2 = "";
public static String _msg3 = "";
public anywheresoftware.b4a.objects.PanelWrapper _pnlset = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlvol = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcur = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _tglfull = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnreset = null;
public static boolean _setclick = false;
public anywheresoftware.b4a.objects.drawable.StateListDrawable _sld = null;
public anywheresoftware.b4a.objects.drawable.StateListDrawable _sld2 = null;
public anywheresoftware.b4a.objects.drawable.ColorDrawable _border = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _ed1 = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _pd1 = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _ed2 = null;
public anywheresoftware.b4a.objects.drawable.BitmapDrawable _pd2 = null;
public static String _sname = "";
public com.BUPC.YPSF.main _main = null;
public com.BUPC.YPSF.score _score = null;
public com.BUPC.YPSF.play _play = null;
public com.BUPC.YPSF.statemanager _statemanager = null;
public com.BUPC.YPSF.function _function = null;
public com.BUPC.YPSF.library _library = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
 //BA.debugLineNum = 71;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 72;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 73;BA.debugLine="MP.Initialize2(\"MP\")";
_mp.Initialize2(processBA,"MP");
 };
 //BA.debugLineNum = 76;BA.debugLine="Activity.LoadLayout(\"2\")";
mostCurrent._activity.LoadLayout("2",mostCurrent.activityBA);
 //BA.debugLineNum = 77;BA.debugLine="lblLevel.Text = \"0\"";
mostCurrent._lbllevel.setText((Object)("0"));
 //BA.debugLineNum = 78;BA.debugLine="pnlBG.Initialize(\"pnlBg\")";
mostCurrent._pnlbg.Initialize(mostCurrent.activityBA,"pnlBg");
 //BA.debugLineNum = 79;BA.debugLine="pnlTut.Initialize(\"\")";
mostCurrent._pnltut.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 80;BA.debugLine="lblIns.Initialize(\"\")";
mostCurrent._lblins.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 81;BA.debugLine="btnNext.Initialize(\"btnNext\")";
mostCurrent._btnnext.Initialize(mostCurrent.activityBA,"btnNext");
 //BA.debugLineNum = 82;BA.debugLine="btnYes.Initialize(\"btnYes\")";
mostCurrent._btnyes.Initialize(mostCurrent.activityBA,"btnYes");
 //BA.debugLineNum = 83;BA.debugLine="btnNo.Initialize(\"btnNo\")";
mostCurrent._btnno.Initialize(mostCurrent.activityBA,"btnNo");
 //BA.debugLineNum = 84;BA.debugLine="lblCorName.Initialize(\"\")";
mostCurrent._lblcorname.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 85;BA.debugLine="lblCortrivia.Initialize(\"\")";
mostCurrent._lblcortrivia.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 86;BA.debugLine="scroCorTrivia.Initialize(1000dip)";
mostCurrent._scrocortrivia.Initialize(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1000)));
 //BA.debugLineNum = 87;BA.debugLine="imgCoranimal.Initialize(\"\")";
mostCurrent._imgcoranimal.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 88;BA.debugLine="pnlTutBG.Initialize(\"\")";
mostCurrent._pnltutbg.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 89;BA.debugLine="scroTut.Initialize(500)";
mostCurrent._scrotut.Initialize(mostCurrent.activityBA,(int) (500));
 //BA.debugLineNum = 90;BA.debugLine="btnDone.Initialize(\"btnDone\")";
mostCurrent._btndone.Initialize(mostCurrent.activityBA,"btnDone");
 //BA.debugLineNum = 93;BA.debugLine="level(\"dog1.jpg\", \"cat1.jpg\", \"tiger1.jpg\", \"elep";
_level("dog1.jpg","cat1.jpg","tiger1.jpg","elephant1.jpg");
 //BA.debugLineNum = 94;BA.debugLine="pop(msg1, \"1\")";
_pop(mostCurrent._msg1,"1");
 //BA.debugLineNum = 96;BA.debugLine="screen.KeepAlive(True)";
mostCurrent._screen.KeepAlive(processBA,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 97;BA.debugLine="screen.PartialLock";
mostCurrent._screen.PartialLock(processBA);
 //BA.debugLineNum = 98;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
 //BA.debugLineNum = 310;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 311;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 312;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 }else if(_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_MENU && mostCurrent._btnset.getEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 314;BA.debugLine="btnSet_Click";
_btnset_click();
 };
 //BA.debugLineNum = 316;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 113;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 114;BA.debugLine="screen.ReleaseKeepAlive";
mostCurrent._screen.ReleaseKeepAlive();
 //BA.debugLineNum = 115;BA.debugLine="screen.ReleasePartialLock";
mostCurrent._screen.ReleasePartialLock();
 //BA.debugLineNum = 116;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
int _fstat = 0;
int _vol = 0;
 //BA.debugLineNum = 100;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 101;BA.debugLine="Dim fstat = StateManager.GetSetting2(\"fullscreen\"";
_fstat = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 102;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 103;BA.debugLine="If fstat == 1 Then";
if (_fstat==1) { 
 //BA.debugLineNum = 104;BA.debugLine="function.FullScreen(True, \"Tutorial\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.True,"Tutorial");
 }else {
 //BA.debugLineNum = 106;BA.debugLine="function.FullScreen(False, \"Tutorial\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.False,"Tutorial");
 };
 //BA.debugLineNum = 108;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 109;BA.debugLine="playAnim.Initialize";
mostCurrent._playanim.Initialize();
 //BA.debugLineNum = 110;BA.debugLine="MP.SetVolume(vol/10, vol/10)";
_mp.SetVolume((float) (_vol/(double)10),(float) (_vol/(double)10));
 //BA.debugLineNum = 111;BA.debugLine="End Sub";
return "";
}
public static String  _anim_animationend() throws Exception{
 //BA.debugLineNum = 412;BA.debugLine="Sub anim_animationend";
 //BA.debugLineNum = 413;BA.debugLine="pnlBG.RemoveAllViews";
mostCurrent._pnlbg.RemoveAllViews();
 //BA.debugLineNum = 414;BA.debugLine="pnlBG.RemoveView";
mostCurrent._pnlbg.RemoveView();
 //BA.debugLineNum = 415;BA.debugLine="btnSet.Enabled = True";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 416;BA.debugLine="End Sub";
return "";
}
public static String  _btnback_click() throws Exception{
 //BA.debugLineNum = 304;BA.debugLine="Sub btnBack_Click";
 //BA.debugLineNum = 305;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 306;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 307;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromlef";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 //BA.debugLineNum = 308;BA.debugLine="End Sub";
return "";
}
public static String  _btnclearhs_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 418;BA.debugLine="Sub btnClearHS_Click";
 //BA.debugLineNum = 419;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 420;BA.debugLine="res = Msgbox2(\"Do you really want to clear the hi";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want to clear the highscore?","Reset","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 421;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 422;BA.debugLine="db.ExecNonQuery(\"DELETE FROM hscore\")";
_db.ExecNonQuery("DELETE FROM hscore");
 //BA.debugLineNum = 423;BA.debugLine="StateManager.SetSetting(\"pnum\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"pnum","0");
 //BA.debugLineNum = 424;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 425;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 427;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 429;BA.debugLine="End Sub";
return "";
}
public static String  _btndone_click() throws Exception{
 //BA.debugLineNum = 255;BA.debugLine="Sub btnDone_Click";
 //BA.debugLineNum = 256;BA.debugLine="pnlTut.RemoveAllViews";
mostCurrent._pnltut.RemoveAllViews();
 //BA.debugLineNum = 257;BA.debugLine="pnlTutBG.RemoveAllViews";
mostCurrent._pnltutbg.RemoveAllViews();
 //BA.debugLineNum = 258;BA.debugLine="pnlTutBG.RemoveView";
mostCurrent._pnltutbg.RemoveView();
 //BA.debugLineNum = 259;BA.debugLine="lblLevel.Text = \"0\"";
mostCurrent._lbllevel.setText((Object)("0"));
 //BA.debugLineNum = 260;BA.debugLine="aniFade.FadeOut(\"newlevel\", durval)";
mostCurrent._anifade.FadeOut(mostCurrent.activityBA,"newlevel",(long) (_durval));
 //BA.debugLineNum = 261;BA.debugLine="aniFade.StartAnim(pnlPics)";
mostCurrent._anifade.StartAnim((android.view.View)(mostCurrent._pnlpics.getObject()));
 //BA.debugLineNum = 262;BA.debugLine="End Sub";
return "";
}
public static String  _btnevent_click() throws Exception{
 //BA.debugLineNum = 207;BA.debugLine="Sub btnEvent_Click";
 //BA.debugLineNum = 208;BA.debugLine="border.Initialize2(0xFF28507B, 0, 4dip, Colors.Ye";
mostCurrent._border.Initialize2((int) (0xff28507b),(int) (0),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4)),anywheresoftware.b4a.keywords.Common.Colors.Yellow);
 //BA.debugLineNum = 209;BA.debugLine="enabled.Initialize(LoadBitmapSample(File.DirAsset";
mostCurrent._enabled.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"nextlevel_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 210;BA.debugLine="pressed.Initialize(LoadBitmapSample(File.DirAsset";
mostCurrent._pressed.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"nextlevel_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 211;BA.debugLine="sld.Initialize";
mostCurrent._sld.Initialize();
 //BA.debugLineNum = 212;BA.debugLine="sld.AddState(sld.State_Pressed, pressed)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Pressed,(android.graphics.drawable.Drawable)(mostCurrent._pressed.getObject()));
 //BA.debugLineNum = 213;BA.debugLine="sld.AddState(sld.State_Enabled, enabled)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Enabled,(android.graphics.drawable.Drawable)(mostCurrent._enabled.getObject()));
 //BA.debugLineNum = 215;BA.debugLine="Activity.AddView(pnlTutBG, 0, 9%x, 100%x, 100%y";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnltutbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 216;BA.debugLine="pnlTutBG.Color = 0x64000000";
mostCurrent._pnltutbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 218;BA.debugLine="pnlTutBG.AddView(pnlTut, 2%x, 25%x, 96%x, 96%x)";
mostCurrent._pnltutbg.AddView((android.view.View)(mostCurrent._pnltut.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (25),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA));
 //BA.debugLineNum = 219;BA.debugLine="pnlTut.Background = border";
mostCurrent._pnltut.setBackground((android.graphics.drawable.Drawable)(mostCurrent._border.getObject()));
 //BA.debugLineNum = 221;BA.debugLine="pnlTut.AddView(lblCorName, 28%x, 6%x, 40%x, 7%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._lblcorname.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (28),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (6),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (7),mostCurrent.activityBA));
 //BA.debugLineNum = 222;BA.debugLine="lblCorName.Text = \"Dog\"";
mostCurrent._lblcorname.setText((Object)("Dog"));
 //BA.debugLineNum = 223;BA.debugLine="lblCorName.TextColor = Colors.White";
mostCurrent._lblcorname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 224;BA.debugLine="lblCorName.Gravity = Gravity.CENTER_HORIZONTAL";
mostCurrent._lblcorname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 225;BA.debugLine="lblCorName.TextSize = (10%x/100)*35";
mostCurrent._lblcorname.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*35));
 //BA.debugLineNum = 227;BA.debugLine="pnlTut.AddView(scroCorTrivia, 2%x, 13%x, 92%x, 7";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._scrocortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 229;BA.debugLine="scroCorTrivia.Panel.AddView(imgCoranimal, 22%x,";
mostCurrent._scrocortrivia.getPanel().AddView((android.view.View)(mostCurrent._imgcoranimal.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (22),mostCurrent.activityBA),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA));
 //BA.debugLineNum = 231;BA.debugLine="imgCoranimal.Bitmap = LoadBitmapSample(File.DirA";
mostCurrent._imgcoranimal.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"dog1.jpg",mostCurrent._imgcoranimal.getWidth(),mostCurrent._imgcoranimal.getHeight()).getObject()));
 //BA.debugLineNum = 232;BA.debugLine="imgCoranimal.Gravity = Gravity.FILL";
mostCurrent._imgcoranimal.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.FILL);
 //BA.debugLineNum = 234;BA.debugLine="scroCorTrivia.Panel.AddView(lblCortrivia, 2%x, 5";
mostCurrent._scrocortrivia.getPanel().AddView((android.view.View)(mostCurrent._lblcortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (90),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (90),mostCurrent.activityBA));
 //BA.debugLineNum = 235;BA.debugLine="lblCortrivia.Text = trivia";
mostCurrent._lblcortrivia.setText((Object)(mostCurrent._trivia));
 //BA.debugLineNum = 236;BA.debugLine="lblCortrivia.Gravity = Gravity.CENTER_HORIZONTAL";
mostCurrent._lblcortrivia.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 237;BA.debugLine="lblCortrivia.TextSize = (10%x/100)*30";
mostCurrent._lblcortrivia.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*30));
 //BA.debugLineNum = 238;BA.debugLine="scroCorTrivia.Panel.Height = 150%x";
mostCurrent._scrocortrivia.getPanel().setHeight(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (150),mostCurrent.activityBA));
 //BA.debugLineNum = 239;BA.debugLine="scroCorTrivia.ScrollPosition = 0";
mostCurrent._scrocortrivia.setScrollPosition((int) (0));
 //BA.debugLineNum = 241;BA.debugLine="pnlTut.AddView(btnDone,23%x, 84%x, 50%x, 10%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._btndone.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (23),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (84),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 242;BA.debugLine="btnDone.Background = sld";
mostCurrent._btndone.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld.getObject()));
 //BA.debugLineNum = 248;BA.debugLine="aniSlide.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anislide.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 249;BA.debugLine="aniSlide.StartAnim(pnlTut)";
mostCurrent._anislide.StartAnim((android.view.View)(mostCurrent._pnltut.getObject()));
 //BA.debugLineNum = 253;BA.debugLine="End Sub";
return "";
}
public static String  _btnnext_click() throws Exception{
 //BA.debugLineNum = 193;BA.debugLine="Sub btnNext_Click";
 //BA.debugLineNum = 194;BA.debugLine="btnNext.RemoveView";
mostCurrent._btnnext.RemoveView();
 //BA.debugLineNum = 195;BA.debugLine="lblIns.RemoveView";
mostCurrent._lblins.RemoveView();
 //BA.debugLineNum = 196;BA.debugLine="scroTut.RemoveView";
mostCurrent._scrotut.RemoveView();
 //BA.debugLineNum = 197;BA.debugLine="pnlTut.RemoveView";
mostCurrent._pnltut.RemoveView();
 //BA.debugLineNum = 198;BA.debugLine="pnlTutBG.RemoveView";
mostCurrent._pnltutbg.RemoveView();
 //BA.debugLineNum = 199;BA.debugLine="If sname == \"1\" Then";
if ((mostCurrent._sname).equals("1")) { 
 //BA.debugLineNum = 200;BA.debugLine="btnPlay.Enabled = True";
mostCurrent._btnplay.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 }else if((mostCurrent._sname).equals("2")) { 
 //BA.debugLineNum = 202;BA.debugLine="btnPic1.Enabled = True";
mostCurrent._btnpic1.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 203;BA.debugLine="btnPlay.Enabled = False";
mostCurrent._btnplay.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 205;BA.debugLine="End Sub";
return "";
}
public static String  _btnno_click() throws Exception{
 //BA.debugLineNum = 298;BA.debugLine="Sub btnNo_Click";
 //BA.debugLineNum = 299;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 300;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 301;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromlef";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 //BA.debugLineNum = 302;BA.debugLine="End Sub";
return "";
}
public static String  _btnplay_click() throws Exception{
 //BA.debugLineNum = 274;BA.debugLine="Sub btnPlay_Click";
 //BA.debugLineNum = 275;BA.debugLine="MP.Load(File.DirAssets, \"dog.mp3\")";
_mp.Load(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"dog.mp3");
 //BA.debugLineNum = 276;BA.debugLine="MP.Play";
_mp.Play();
 //BA.debugLineNum = 277;BA.debugLine="playAnim.AddFrame(LoadBitmapSample(File.DirAssets";
mostCurrent._playanim.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_0.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 278;BA.debugLine="playAnim.AddFrame(LoadBitmapSample(File.DirAssets";
mostCurrent._playanim.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_1.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 279;BA.debugLine="playAnim.AddFrame(LoadBitmapSample(File.DirAssets";
mostCurrent._playanim.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_2.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 280;BA.debugLine="playAnim.AddFrame(LoadBitmapSample(File.DirAssets";
mostCurrent._playanim.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 281;BA.debugLine="playAnim.OneShot = False";
mostCurrent._playanim.setOneShot(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 282;BA.debugLine="btnPlay.Background = playAnim";
mostCurrent._btnplay.setBackground((android.graphics.drawable.Drawable)(mostCurrent._playanim.getObject()));
 //BA.debugLineNum = 283;BA.debugLine="playAnim.Start";
mostCurrent._playanim.Start();
 //BA.debugLineNum = 284;BA.debugLine="End Sub";
return "";
}
public static String  _btnreset_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 401;BA.debugLine="Sub btnReset_Click";
 //BA.debugLineNum = 402;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 403;BA.debugLine="res = Msgbox2(\"Do you really want reset the game?";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want reset the game?"+anywheresoftware.b4a.keywords.Common.CRLF+"*note that data will deleted including game progress and library.","Reset game","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 404;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 405;BA.debugLine="function.resetall(db)";
mostCurrent._function._resetall(mostCurrent.activityBA,_db);
 //BA.debugLineNum = 406;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 408;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 410;BA.debugLine="End Sub";
return "";
}
public static String  _btnset_click() throws Exception{
anywheresoftware.b4a.objects.drawable.BitmapDrawable _checked = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _unchecked = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed2 = null;
int _fstat2 = 0;
boolean _s = false;
float _vol = 0f;
 //BA.debugLineNum = 318;BA.debugLine="Sub btnSet_Click";
 //BA.debugLineNum = 319;BA.debugLine="Dim sld As StateListDrawable";
mostCurrent._sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 320;BA.debugLine="Dim checked As BitmapDrawable";
_checked = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 321;BA.debugLine="Dim unchecked As ColorDrawable";
_unchecked = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 322;BA.debugLine="Dim sld2 As StateListDrawable";
mostCurrent._sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 323;BA.debugLine="Dim enabled2, pressed2 As BitmapDrawable";
_enabled2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 325;BA.debugLine="enabled2.Initialize(LoadBitmapSample(File.DirAsse";
_enabled2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 326;BA.debugLine="pressed2.Initialize(LoadBitmapSample(File.DirAsse";
_pressed2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 327;BA.debugLine="sld2.Initialize";
mostCurrent._sld2.Initialize();
 //BA.debugLineNum = 328;BA.debugLine="sld2.AddState(sld2.State_Pressed, pressed2)";
mostCurrent._sld2.AddState(mostCurrent._sld2.State_Pressed,(android.graphics.drawable.Drawable)(_pressed2.getObject()));
 //BA.debugLineNum = 329;BA.debugLine="sld2.AddState(sld2.State_Enabled, enabled2)";
mostCurrent._sld2.AddState(mostCurrent._sld2.State_Enabled,(android.graphics.drawable.Drawable)(_enabled2.getObject()));
 //BA.debugLineNum = 331;BA.debugLine="checked.Initialize(LoadBitmap(File.DirAssets, \"tg";
_checked.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"tglc.png").getObject()));
 //BA.debugLineNum = 332;BA.debugLine="unchecked.Initialize(Colors.White, 0)";
_unchecked.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 333;BA.debugLine="sld.Initialize";
mostCurrent._sld.Initialize();
 //BA.debugLineNum = 334;BA.debugLine="sld.AddState(sld.State_Unchecked, unchecked)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Unchecked,(android.graphics.drawable.Drawable)(_unchecked.getObject()));
 //BA.debugLineNum = 335;BA.debugLine="sld.AddState(sld.State_Checked, checked)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Checked,(android.graphics.drawable.Drawable)(_checked.getObject()));
 //BA.debugLineNum = 336;BA.debugLine="Dim fstat2 = StateManager.GetSetting2(\"fullscreen";
_fstat2 = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 337;BA.debugLine="Dim s As Boolean";
_s = false;
 //BA.debugLineNum = 338;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (float)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 339;BA.debugLine="If fstat2 == 1 Then";
if (_fstat2==1) { 
 //BA.debugLineNum = 340;BA.debugLine="s = True";
_s = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 342;BA.debugLine="s = False";
_s = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 344;BA.debugLine="If setclick == False Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 345;BA.debugLine="Activity.AddView(pnlBG, 0, 9%x, 100%x, 100%y - 9";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 346;BA.debugLine="pnlBG.LoadLayout(\"5\")";
mostCurrent._pnlbg.LoadLayout("5",mostCurrent.activityBA);
 //BA.debugLineNum = 347;BA.debugLine="pnlCur.Width = (vol/10)*pnlVol.Width";
mostCurrent._pnlcur.setWidth((int) ((_vol/(double)10)*mostCurrent._pnlvol.getWidth()));
 //BA.debugLineNum = 348;BA.debugLine="pnlBG.Color = 0x64000000";
mostCurrent._pnlbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 349;BA.debugLine="tglFull.Background = sld";
mostCurrent._tglfull.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld.getObject()));
 //BA.debugLineNum = 350;BA.debugLine="tglFull.Checked = s";
mostCurrent._tglfull.setChecked(_s);
 //BA.debugLineNum = 351;BA.debugLine="btnReset.Background = sld2";
mostCurrent._btnreset.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld2.getObject()));
 //BA.debugLineNum = 352;BA.debugLine="aniSlide.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anislide.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 353;BA.debugLine="aniSlide.StartAnim(pnlSet)";
mostCurrent._anislide.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 354;BA.debugLine="setclick = True";
_setclick = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 356;BA.debugLine="aniSlide.SlideToTop(\"anim\",slideval, durval)";
mostCurrent._anislide.SlideToTop(mostCurrent.activityBA,"anim",_slideval,(long) (_durval));
 //BA.debugLineNum = 357;BA.debugLine="aniSlide.StartAnim(pnlSet)";
mostCurrent._anislide.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 358;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 359;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 361;BA.debugLine="End Sub";
return "";
}
public static String  _btnyes_click() throws Exception{
 //BA.debugLineNum = 292;BA.debugLine="Sub btnYes_Click";
 //BA.debugLineNum = 293;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 294;BA.debugLine="StartActivity(Play)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._play.getObject()));
 //BA.debugLineNum = 295;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromlef";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 //BA.debugLineNum = 296;BA.debugLine="End Sub";
return "";
}
public static String  _endtutor_animationend() throws Exception{
 //BA.debugLineNum = 270;BA.debugLine="Sub endtutor_animationend";
 //BA.debugLineNum = 271;BA.debugLine="pop(msg3, \"last\")";
_pop(mostCurrent._msg3,"last");
 //BA.debugLineNum = 272;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 17;BA.debugLine="Private btnPic1 As Button";
mostCurrent._btnpic1 = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private btnPic2 As Button";
mostCurrent._btnpic2 = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private btnPic4 As Button";
mostCurrent._btnpic4 = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private btnPic3 As Button";
mostCurrent._btnpic3 = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private btnBack As Button";
mostCurrent._btnback = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private btnSet As Button";
mostCurrent._btnset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Private lblLevel As Label";
mostCurrent._lbllevel = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private img1 As ImageView";
mostCurrent._img1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private img2 As ImageView";
mostCurrent._img2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Private img4 As ImageView";
mostCurrent._img4 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 27;BA.debugLine="Private img3 As ImageView";
mostCurrent._img3 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Private btnPlay As Button";
mostCurrent._btnplay = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private pnlPics As Panel";
mostCurrent._pnlpics = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Private pnlTutBG As Panel";
mostCurrent._pnltutbg = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Dim screen As PhoneWakeState";
mostCurrent._screen = new anywheresoftware.b4a.phone.Phone.PhoneWakeState();
 //BA.debugLineNum = 32;BA.debugLine="Private pnlBG, pnlTut As Panel";
mostCurrent._pnlbg = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._pnltut = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 33;BA.debugLine="Private lblIns As Label";
mostCurrent._lblins = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 34;BA.debugLine="Private btnNext, btnDone, btnYes, btnNo As Button";
mostCurrent._btnnext = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._btndone = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._btnyes = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._btnno = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Dim aniSlide As ICOSSlideAnimation";
mostCurrent._anislide = new giuseppe.salvi.icos.library.ICOSSlideAnimation();
 //BA.debugLineNum = 36;BA.debugLine="Dim aniFade As ICOSFadeAnimation";
mostCurrent._anifade = new giuseppe.salvi.icos.library.ICOSFadeAnimation();
 //BA.debugLineNum = 37;BA.debugLine="Dim playAnim As AnimationDrawable";
mostCurrent._playanim = new flm.b4a.animationplus.AnimationDrawable();
 //BA.debugLineNum = 38;BA.debugLine="Dim slideval = 700, durval = 1500 , fadedur = 300";
_slideval = (float) (700);
_durval = (float) (1500);
_fadedur = (float) (300);
 //BA.debugLineNum = 39;BA.debugLine="Private btnClearHS As Button";
mostCurrent._btnclearhs = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Private lblCorName, lblCortrivia As Label";
mostCurrent._lblcorname = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcortrivia = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private scroCorTrivia, scroTut As ScrollView";
mostCurrent._scrocortrivia = new anywheresoftware.b4a.objects.ScrollViewWrapper();
mostCurrent._scrotut = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private imgCoranimal As ImageView";
mostCurrent._imgcoranimal = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Dim trivia As String";
mostCurrent._trivia = "";
 //BA.debugLineNum = 45;BA.debugLine="trivia =	\"Dogs are thought to have been first dom";
mostCurrent._trivia = "Dogs are thought to have been first domesticated in East Asia thousands of years ago. People primarily used dogs for guarding the hunters and areas of land."+"Todays domestic dog Is actually a subspecies of the gray wolf, a Type of dog that Is feared by most humans. Many people today, in all countries around the world, keep dogs As household pets And many even regard their dog As a family member.";
 //BA.debugLineNum = 48;BA.debugLine="Dim msg1, msg2, msg3 As String";
mostCurrent._msg1 = "";
mostCurrent._msg2 = "";
mostCurrent._msg3 = "";
 //BA.debugLineNum = 49;BA.debugLine="msg1 =	\"To pass the level, first, click the play";
mostCurrent._msg1 = "To pass the level, first, click the play button to hear the sound question.";
 //BA.debugLineNum = 50;BA.debugLine="msg2 =	\"The four pictures below are the choices,\"";
mostCurrent._msg2 = "The four pictures below are the choices,"+anywheresoftware.b4a.keywords.Common.CRLF+"if you click the picture that matches the sound, a trivia will popup."+anywheresoftware.b4a.keywords.Common.CRLF+"in this example, the first picture(dog) is the right answer, click it.";
 //BA.debugLineNum = 53;BA.debugLine="msg3 =	\"You completed the tutorial.\" & CRLF & _";
mostCurrent._msg3 = "You completed the tutorial."+anywheresoftware.b4a.keywords.Common.CRLF+"Do you want to start the game now?";
 //BA.debugLineNum = 56;BA.debugLine="Private pnlSet As Panel";
mostCurrent._pnlset = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 57;BA.debugLine="Private pnlVol As Panel";
mostCurrent._pnlvol = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 58;BA.debugLine="Private pnlCur As Panel";
mostCurrent._pnlcur = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 59;BA.debugLine="Private tglFull As ToggleButton";
mostCurrent._tglfull = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
 //BA.debugLineNum = 60;BA.debugLine="Private btnReset As Button";
mostCurrent._btnreset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 61;BA.debugLine="Dim setclick As Boolean";
_setclick = false;
 //BA.debugLineNum = 63;BA.debugLine="Dim sld, sld2 As StateListDrawable";
mostCurrent._sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
mostCurrent._sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 64;BA.debugLine="Dim border As ColorDrawable";
mostCurrent._border = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 65;BA.debugLine="Dim enabled, pressed, ed1, pd1, ed2, pd2 As Bitma";
mostCurrent._enabled = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
mostCurrent._pressed = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
mostCurrent._ed1 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
mostCurrent._pd1 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
mostCurrent._ed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
mostCurrent._pd2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 67;BA.debugLine="Dim sname As String";
mostCurrent._sname = "";
 //BA.debugLineNum = 69;BA.debugLine="End Sub";
return "";
}
public static String  _level(String _pic1,String _pic2,String _pic3,String _pic4) throws Exception{
anywheresoftware.b4a.objects.drawable.BitmapDrawable _i1 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _i2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _i3 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _i4 = null;
 //BA.debugLineNum = 118;BA.debugLine="Sub level(pic1 As String, pic2 As String, pic3 As";
 //BA.debugLineNum = 119;BA.debugLine="Dim i1, i2, i3, i4 As BitmapDrawable";
_i1 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_i2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_i3 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_i4 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 120;BA.debugLine="i1.Initialize(LoadBitmapSample(File.DirAssets, pi";
_i1.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_pic1,mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 121;BA.debugLine="i2.Initialize(LoadBitmapSample(File.DirAssets, pi";
_i2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_pic2,mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 122;BA.debugLine="i3.Initialize(LoadBitmapSample(File.DirAssets, pi";
_i3.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_pic3,mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 123;BA.debugLine="i4.Initialize(LoadBitmapSample(File.DirAssets, pi";
_i4.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_pic4,mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 124;BA.debugLine="img1.Background = i1";
mostCurrent._img1.setBackground((android.graphics.drawable.Drawable)(_i1.getObject()));
 //BA.debugLineNum = 125;BA.debugLine="img2.Background = i2";
mostCurrent._img2.setBackground((android.graphics.drawable.Drawable)(_i2.getObject()));
 //BA.debugLineNum = 126;BA.debugLine="img3.Background = i3";
mostCurrent._img3.setBackground((android.graphics.drawable.Drawable)(_i3.getObject()));
 //BA.debugLineNum = 127;BA.debugLine="img4.Background = i4";
mostCurrent._img4.setBackground((android.graphics.drawable.Drawable)(_i4.getObject()));
 //BA.debugLineNum = 128;BA.debugLine="btnPic1.Enabled = False";
mostCurrent._btnpic1.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 129;BA.debugLine="btnPic2.Enabled = False";
mostCurrent._btnpic2.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 130;BA.debugLine="btnPic3.Enabled = False";
mostCurrent._btnpic3.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 131;BA.debugLine="btnPic4.Enabled = False";
mostCurrent._btnpic4.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 132;BA.debugLine="btnPlay.Enabled = False";
mostCurrent._btnplay.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 133;BA.debugLine="End Sub";
return "";
}
public static String  _mp_complete() throws Exception{
 //BA.debugLineNum = 286;BA.debugLine="Sub MP_Complete";
 //BA.debugLineNum = 287;BA.debugLine="playAnim.Stop";
mostCurrent._playanim.Stop();
 //BA.debugLineNum = 288;BA.debugLine="btnPlay.SetBackgroundImage(LoadBitmapSample(File.";
mostCurrent._btnplay.SetBackgroundImage((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()));
 //BA.debugLineNum = 289;BA.debugLine="pop(msg2, \"2\")";
_pop(mostCurrent._msg2,"2");
 //BA.debugLineNum = 290;BA.debugLine="End Sub";
return "";
}
public static String  _newlevel_animationend() throws Exception{
 //BA.debugLineNum = 264;BA.debugLine="Sub newlevel_animationend";
 //BA.debugLineNum = 265;BA.debugLine="level(\"beetle1.jpg\", \"bee1.jpg\", \"cricket2.jpg\",";
_level("beetle1.jpg","bee1.jpg","cricket2.jpg","deer1.jpg");
 //BA.debugLineNum = 266;BA.debugLine="aniFade.FadeIn(\"endtutor\", fadedur)";
mostCurrent._anifade.FadeIn(mostCurrent.activityBA,"endtutor",(long) (_fadedur));
 //BA.debugLineNum = 267;BA.debugLine="aniFade.StartAnim(pnlPics)";
mostCurrent._anifade.StartAnim((android.view.View)(mostCurrent._pnlpics.getObject()));
 //BA.debugLineNum = 268;BA.debugLine="End Sub";
return "";
}
public static String  _pnlbg_click() throws Exception{
 //BA.debugLineNum = 363;BA.debugLine="Sub pnlBg_Click";
 //BA.debugLineNum = 364;BA.debugLine="If setclick == True Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 365;BA.debugLine="aniSlide.SlideToTop(\"anim\",slideval, durval)";
mostCurrent._anislide.SlideToTop(mostCurrent.activityBA,"anim",_slideval,(long) (_durval));
 //BA.debugLineNum = 366;BA.debugLine="aniSlide.StartAnim(pnlSet)";
mostCurrent._anislide.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 367;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 368;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 370;BA.debugLine="End Sub";
return "";
}
public static String  _pnlset_click() throws Exception{
 //BA.debugLineNum = 372;BA.debugLine="Sub pnlSet_Click";
 //BA.debugLineNum = 374;BA.debugLine="End Sub";
return "";
}
public static String  _pnlvol_touch(int _action,float _x,float _y) throws Exception{
float _a = 0f;
 //BA.debugLineNum = 376;BA.debugLine="Sub pnlVol_Touch (Action As Int, X As Float, Y As";
 //BA.debugLineNum = 377;BA.debugLine="Dim a As Float";
_a = 0f;
 //BA.debugLineNum = 378;BA.debugLine="If X < 0 Then";
if (_x<0) { 
 //BA.debugLineNum = 379;BA.debugLine="X = 0";
_x = (float) (0);
 }else if(_x>mostCurrent._pnlvol.getWidth()) { 
 //BA.debugLineNum = 381;BA.debugLine="X = pnlVol.Width";
_x = (float) (mostCurrent._pnlvol.getWidth());
 };
 //BA.debugLineNum = 383;BA.debugLine="a = (X/pnlVol.Width)*10";
_a = (float) ((_x/(double)mostCurrent._pnlvol.getWidth())*10);
 //BA.debugLineNum = 384;BA.debugLine="StateManager.SetSetting(\"volume\", a)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"volume",BA.NumberToString(_a));
 //BA.debugLineNum = 385;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 386;BA.debugLine="pnlCur.Width = X";
mostCurrent._pnlcur.setWidth((int) (_x));
 //BA.debugLineNum = 387;BA.debugLine="MP.SetVolume(a/10, a/10)";
_mp.SetVolume((float) (_a/(double)10),(float) (_a/(double)10));
 //BA.debugLineNum = 388;BA.debugLine="End Sub";
return "";
}
public static String  _pop(String _msg,String _seq) throws Exception{
anywheresoftware.b4a.objects.StringUtils _stringu = null;
 //BA.debugLineNum = 135;BA.debugLine="Sub pop(msg As String, seq As String)";
 //BA.debugLineNum = 136;BA.debugLine="Dim stringu As StringUtils";
_stringu = new anywheresoftware.b4a.objects.StringUtils();
 //BA.debugLineNum = 137;BA.debugLine="border.Initialize2(0xFF28507B, 0, 4dip, Colors.Ye";
mostCurrent._border.Initialize2((int) (0xff28507b),(int) (0),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4)),anywheresoftware.b4a.keywords.Common.Colors.Yellow);
 //BA.debugLineNum = 139;BA.debugLine="Activity.AddView(pnlTutBG, 0, 9%x, 100%x, 100%y -";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnltutbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 140;BA.debugLine="pnlTutBG.Color = 0x64000000";
mostCurrent._pnltutbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 141;BA.debugLine="pnlTutBG.AddView(pnlTut, 2%x, 30%x, 96%x, 46%x)";
mostCurrent._pnltutbg.AddView((android.view.View)(mostCurrent._pnltut.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (30),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (46),mostCurrent.activityBA));
 //BA.debugLineNum = 142;BA.debugLine="pnlTut.Background = border";
mostCurrent._pnltut.setBackground((android.graphics.drawable.Drawable)(mostCurrent._border.getObject()));
 //BA.debugLineNum = 143;BA.debugLine="pnlTut.AddView(scroTut, 2%x, 2%x,92%x, 30%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._scrotut.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (30),mostCurrent.activityBA));
 //BA.debugLineNum = 144;BA.debugLine="scroTut.Panel.AddView(lblIns, 0, 0, 92%x,200)";
mostCurrent._scrotut.getPanel().AddView((android.view.View)(mostCurrent._lblins.getObject()),(int) (0),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),(int) (200));
 //BA.debugLineNum = 145;BA.debugLine="lblIns.TextSize = (9%x/100)*35";
mostCurrent._lblins.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)/(double)100)*35));
 //BA.debugLineNum = 146;BA.debugLine="lblIns.Gravity = Gravity.CENTER";
mostCurrent._lblins.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 147;BA.debugLine="lblIns.TextColor = Colors.White";
mostCurrent._lblins.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 148;BA.debugLine="lblIns.Text = msg";
mostCurrent._lblins.setText((Object)(_msg));
 //BA.debugLineNum = 149;BA.debugLine="lblIns.Height = stringu.MeasureMultilineTextHeigh";
mostCurrent._lblins.setHeight(_stringu.MeasureMultilineTextHeight((android.widget.TextView)(mostCurrent._lblins.getObject()),mostCurrent._lblins.getText()));
 //BA.debugLineNum = 150;BA.debugLine="scroTut.Panel.Height = stringu.MeasureMultilineTe";
mostCurrent._scrotut.getPanel().setHeight(_stringu.MeasureMultilineTextHeight((android.widget.TextView)(mostCurrent._lblins.getObject()),mostCurrent._lblins.getText()));
 //BA.debugLineNum = 151;BA.debugLine="If seq == \"last\" Then";
if ((_seq).equals("last")) { 
 //BA.debugLineNum = 153;BA.debugLine="ed1.Initialize(LoadBitmapSample(File.DirAssets,";
mostCurrent._ed1.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"yes_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 154;BA.debugLine="pd1.Initialize(LoadBitmapSample(File.DirAssets,";
mostCurrent._pd1.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"yes_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 156;BA.debugLine="ed2.Initialize(LoadBitmapSample(File.DirAssets,";
mostCurrent._ed2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"no_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 157;BA.debugLine="pd2.Initialize(LoadBitmapSample(File.DirAssets,";
mostCurrent._pd2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"no_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 158;BA.debugLine="sld.Initialize";
mostCurrent._sld.Initialize();
 //BA.debugLineNum = 159;BA.debugLine="sld.AddState(sld.State_Pressed, pd1)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Pressed,(android.graphics.drawable.Drawable)(mostCurrent._pd1.getObject()));
 //BA.debugLineNum = 160;BA.debugLine="sld.AddState(sld.State_Enabled, ed1)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Enabled,(android.graphics.drawable.Drawable)(mostCurrent._ed1.getObject()));
 //BA.debugLineNum = 162;BA.debugLine="sld2.Initialize";
mostCurrent._sld2.Initialize();
 //BA.debugLineNum = 163;BA.debugLine="sld2.AddState(sld2.State_Pressed, pd2)";
mostCurrent._sld2.AddState(mostCurrent._sld2.State_Pressed,(android.graphics.drawable.Drawable)(mostCurrent._pd2.getObject()));
 //BA.debugLineNum = 164;BA.debugLine="sld2.AddState(sld2.State_Enabled, ed2)";
mostCurrent._sld2.AddState(mostCurrent._sld2.State_Enabled,(android.graphics.drawable.Drawable)(mostCurrent._ed2.getObject()));
 //BA.debugLineNum = 166;BA.debugLine="pnlTut.AddView(btnYes,2%x, 34%x, 45%x, 10%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._btnyes.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (34),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (45),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 167;BA.debugLine="btnYes.Background = sld";
mostCurrent._btnyes.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld.getObject()));
 //BA.debugLineNum = 171;BA.debugLine="pnlTut.AddView(btnNo,49%x, 34%x, 45%x, 10%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._btnno.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (49),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (34),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (45),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 175;BA.debugLine="btnNo.Background = sld2";
mostCurrent._btnno.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld2.getObject()));
 }else {
 //BA.debugLineNum = 178;BA.debugLine="enabled.Initialize(LoadBitmapSample(File.DirAsse";
mostCurrent._enabled.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"next_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 179;BA.debugLine="pressed.Initialize(LoadBitmapSample(File.DirAsse";
mostCurrent._pressed.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"next_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 180;BA.debugLine="sld.Initialize";
mostCurrent._sld.Initialize();
 //BA.debugLineNum = 181;BA.debugLine="sld.AddState(sld.State_Pressed, pressed)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Pressed,(android.graphics.drawable.Drawable)(mostCurrent._pressed.getObject()));
 //BA.debugLineNum = 182;BA.debugLine="sld.AddState(sld.State_Enabled, enabled)";
mostCurrent._sld.AddState(mostCurrent._sld.State_Enabled,(android.graphics.drawable.Drawable)(mostCurrent._enabled.getObject()));
 //BA.debugLineNum = 184;BA.debugLine="pnlTut.AddView(btnNext,23%x, 34%x, 50%x, 10%x)";
mostCurrent._pnltut.AddView((android.view.View)(mostCurrent._btnnext.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (23),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (34),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 185;BA.debugLine="btnNext.Background = sld";
mostCurrent._btnnext.setBackground((android.graphics.drawable.Drawable)(mostCurrent._sld.getObject()));
 };
 //BA.debugLineNum = 190;BA.debugLine="sname = seq";
mostCurrent._sname = _seq;
 //BA.debugLineNum = 191;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim MP As MediaPlayer";
_mp = new anywheresoftware.b4a.objects.MediaPlayerWrapper();
 //BA.debugLineNum = 10;BA.debugLine="Dim db As SQL";
_db = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 12;BA.debugLine="End Sub";
return "";
}
public static String  _tglfull_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 390;BA.debugLine="Sub tglFull_CheckedChange(Checked As Boolean)";
 //BA.debugLineNum = 391;BA.debugLine="If Checked Then";
if (_checked) { 
 //BA.debugLineNum = 392;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"1\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","1");
 }else {
 //BA.debugLineNum = 394;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","0");
 };
 //BA.debugLineNum = 396;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 397;BA.debugLine="function.FullScreen(Checked, \"Tutorial\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,_checked,"Tutorial");
 //BA.debugLineNum = 398;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 399;BA.debugLine="End Sub";
return "";
}
}
