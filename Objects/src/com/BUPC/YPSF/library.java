package com.BUPC.YPSF;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class library extends Activity implements B4AActivity{
	public static library mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "com.BUPC.YPSF", "com.BUPC.YPSF.library");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (library).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
		BA.handler.postDelayed(new WaitForLayout(), 5);

	}
	private static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "com.BUPC.YPSF", "com.BUPC.YPSF.library");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "com.BUPC.YPSF.library", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (library) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (library) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return library.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (library) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (library) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.sql.SQL _sql = null;
public static anywheresoftware.b4a.objects.Timer _timer = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnback = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnset = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imginfo = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlpop = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlinfo = null;
public static String _name = "";
public static String _imgani = "";
public static String _anitrivia = "";
public static int _id = 0;
public anywheresoftware.b4a.objects.ListViewWrapper _listtrivia = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblinfo = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltrivia = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _scroinfo = null;
public giuseppe.salvi.icos.library.ICOSSlideAnimation _anim = null;
public anywheresoftware.b4a.objects.LabelWrapper _label1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label2 = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlload = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlloadbg = null;
public anywheresoftware.b4a.objects.ProgressBarWrapper _progload = null;
public anywheresoftware.b4a.phone.Phone.PhoneWakeState _screen = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblprog = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblloading = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblprocessed = null;
public static int _n = 0;
public static int _total = 0;
public anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlset = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlvol = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcur = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _tglfull = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnreset = null;
public static boolean _setclick = false;
public anywheresoftware.b4a.objects.PanelWrapper _pnlbg = null;
public static float _slideval = 0f;
public static float _durval = 0f;
public anywheresoftware.b4a.objects.ButtonWrapper _btnclearhs = null;
public com.BUPC.YPSF.main _main = null;
public com.BUPC.YPSF.score _score = null;
public com.BUPC.YPSF.play _play = null;
public com.BUPC.YPSF.statemanager _statemanager = null;
public com.BUPC.YPSF.function _function = null;
public com.BUPC.YPSF.tutorial _tutorial = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
anywheresoftware.b4a.objects.drawable.ColorDrawable _border = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _pbcd = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _pbcdbg = null;
 //BA.debugLineNum = 45;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 46;BA.debugLine="Dim border As ColorDrawable";
_border = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 47;BA.debugLine="Dim pbcd, pbcdbg As ColorDrawable";
_pbcd = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
_pbcdbg = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 48;BA.debugLine="border.Initialize2(0xFF28507B, 0, 4dip, Colors.Ye";
_border.Initialize2((int) (0xff28507b),(int) (0),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4)),anywheresoftware.b4a.keywords.Common.Colors.Yellow);
 //BA.debugLineNum = 49;BA.debugLine="pbcd.Initialize(Colors.Red, 0)";
_pbcd.Initialize(anywheresoftware.b4a.keywords.Common.Colors.Red,(int) (0));
 //BA.debugLineNum = 50;BA.debugLine="pbcdbg.Initialize(Colors.White, 0)";
_pbcdbg.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 52;BA.debugLine="If File.Exists(File.DirInternal,\"db\") == False Th";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db")==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 53;BA.debugLine="File.Copy(File.DirAssets, \"db.db\", File.DirInter";
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"db.db",anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db");
 };
 //BA.debugLineNum = 55;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 56;BA.debugLine="sql.Initialize(File.DirInternal, \"db\",False)";
_sql.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db",anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 58;BA.debugLine="timer.Initialize(\"timer\", 50)";
_timer.Initialize(processBA,"timer",(long) (50));
 //BA.debugLineNum = 59;BA.debugLine="timer.Enabled = True";
_timer.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 60;BA.debugLine="pnlLoadBG.Initialize(\"pnlLoadBG\")";
mostCurrent._pnlloadbg.Initialize(mostCurrent.activityBA,"pnlLoadBG");
 //BA.debugLineNum = 61;BA.debugLine="pnlLoad.Initialize(\"\")";
mostCurrent._pnlload.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 62;BA.debugLine="progLoad.Initialize(\"\")";
mostCurrent._progload.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 63;BA.debugLine="lblProg.Initialize(\"\")";
mostCurrent._lblprog.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 64;BA.debugLine="lblLoading.Initialize(\"\")";
mostCurrent._lblloading.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 65;BA.debugLine="lblProcessed.Initialize(\"\")";
mostCurrent._lblprocessed.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 66;BA.debugLine="pnlBg.Initialize(\"pnlBg\")";
mostCurrent._pnlbg.Initialize(mostCurrent.activityBA,"pnlBg");
 //BA.debugLineNum = 68;BA.debugLine="Activity.AddView(pnlLoadBG, 0, 9%x, 100%x, 100%y";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlloadbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 69;BA.debugLine="pnlLoadBG.Color = 0x64000000";
mostCurrent._pnlloadbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 70;BA.debugLine="pnlLoadBG.AddView(pnlLoad, 2%x, 30%x, 96%x, 33%x)";
mostCurrent._pnlloadbg.AddView((android.view.View)(mostCurrent._pnlload.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (30),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (33),mostCurrent.activityBA));
 //BA.debugLineNum = 71;BA.debugLine="pnlLoad.Background = border";
mostCurrent._pnlload.setBackground((android.graphics.drawable.Drawable)(_border.getObject()));
 //BA.debugLineNum = 72;BA.debugLine="pnlLoad.AddView(lblLoading, 2%x, 2%x, 30%x, 10%x)";
mostCurrent._pnlload.AddView((android.view.View)(mostCurrent._lblloading.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (30),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 73;BA.debugLine="lblLoading.Text = \"Loading...\"";
mostCurrent._lblloading.setText((Object)("Loading..."));
 //BA.debugLineNum = 74;BA.debugLine="lblLoading.TextColor = Colors.White";
mostCurrent._lblloading.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 75;BA.debugLine="lblLoading.TextSize = (10%x/100)*40";
mostCurrent._lblloading.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*40));
 //BA.debugLineNum = 76;BA.debugLine="pnlLoad.AddView(progLoad, 2%x, 12%x, 92%x, 5%x)";
mostCurrent._pnlload.AddView((android.view.View)(mostCurrent._progload.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (12),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (5),mostCurrent.activityBA));
 //BA.debugLineNum = 77;BA.debugLine="pnlLoad.AddView(lblProg, 2%x, 21%x, 20%x, 10%x)";
mostCurrent._pnlload.AddView((android.view.View)(mostCurrent._lblprog.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (21),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (20),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 78;BA.debugLine="lblProg.TextColor = Colors.White";
mostCurrent._lblprog.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 79;BA.debugLine="pnlLoad.AddView(lblProcessed, 74%x, 21%x, 20%x, 1";
mostCurrent._pnlload.AddView((android.view.View)(mostCurrent._lblprocessed.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (74),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (21),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (20),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 80;BA.debugLine="lblProcessed.TextColor = Colors.White";
mostCurrent._lblprocessed.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 81;BA.debugLine="lblProcessed.Gravity = Gravity.RIGHT";
mostCurrent._lblprocessed.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 82;BA.debugLine="function.SetProgressDrawable(progLoad, pbcd)";
mostCurrent._function._setprogressdrawable(mostCurrent.activityBA,mostCurrent._progload,(Object)(_pbcd.getObject()));
 //BA.debugLineNum = 83;BA.debugLine="progLoad.Background = pbcdbg";
mostCurrent._progload.setBackground((android.graphics.drawable.Drawable)(_pbcdbg.getObject()));
 //BA.debugLineNum = 84;BA.debugLine="Activity.LoadLayout(\"4\")";
mostCurrent._activity.LoadLayout("4",mostCurrent.activityBA);
 //BA.debugLineNum = 86;BA.debugLine="pnlLoadBG.BringToFront";
mostCurrent._pnlloadbg.BringToFront();
 //BA.debugLineNum = 87;BA.debugLine="pnlPop.Initialize(\"pnlPop\")";
mostCurrent._pnlpop.Initialize(mostCurrent.activityBA,"pnlPop");
 //BA.debugLineNum = 88;BA.debugLine="pnlInfo.Initialize(\"\")";
mostCurrent._pnlinfo.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 89;BA.debugLine="imgInfo.Initialize(\"\")";
mostCurrent._imginfo.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 90;BA.debugLine="lblName.Initialize(\"\")";
mostCurrent._lblname.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 91;BA.debugLine="lblTrivia.Initialize(\"\")";
mostCurrent._lbltrivia.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 92;BA.debugLine="scroInfo.Initialize(1000dip)";
mostCurrent._scroinfo.Initialize(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1000)));
 //BA.debugLineNum = 94;BA.debugLine="cursor = sql.ExecQuery(\"SELECT * FROM game WHERE";
mostCurrent._cursor.setObject((android.database.Cursor)(_sql.ExecQuery("SELECT * FROM game WHERE lib='1'")));
 //BA.debugLineNum = 95;BA.debugLine="total = cursor.RowCount";
_total = mostCurrent._cursor.getRowCount();
 //BA.debugLineNum = 97;BA.debugLine="label1 = listTrivia.TwoLinesAndBitmap.Label";
mostCurrent._label1 = mostCurrent._listtrivia.getTwoLinesAndBitmap().Label;
 //BA.debugLineNum = 98;BA.debugLine="label1.TextSize = (10%x/100)*40";
mostCurrent._label1.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*40));
 //BA.debugLineNum = 99;BA.debugLine="label2 = listTrivia.TwoLinesAndBitmap.SecondLabel";
mostCurrent._label2 = mostCurrent._listtrivia.getTwoLinesAndBitmap().SecondLabel;
 //BA.debugLineNum = 100;BA.debugLine="label2.TextSize = (10%x/100)*25";
mostCurrent._label2.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*25));
 //BA.debugLineNum = 102;BA.debugLine="screen.KeepAlive(True)";
mostCurrent._screen.KeepAlive(processBA,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 103;BA.debugLine="screen.PartialLock";
mostCurrent._screen.PartialLock(processBA);
 //BA.debugLineNum = 104;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
 //BA.debugLineNum = 214;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 215;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 216;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 }else if(_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_MENU && mostCurrent._btnset.getEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 218;BA.debugLine="btnSet_Click";
_btnset_click();
 };
 //BA.debugLineNum = 220;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 116;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 117;BA.debugLine="screen.ReleaseKeepAlive";
mostCurrent._screen.ReleaseKeepAlive();
 //BA.debugLineNum = 118;BA.debugLine="screen.ReleasePartialLock";
mostCurrent._screen.ReleasePartialLock();
 //BA.debugLineNum = 119;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
int _fstat = 0;
 //BA.debugLineNum = 106;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 107;BA.debugLine="Dim fstat = StateManager.GetSetting2(\"fullscreen\"";
_fstat = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 108;BA.debugLine="If fstat == 1 Then";
if (_fstat==1) { 
 //BA.debugLineNum = 109;BA.debugLine="function.FullScreen(True, \"Library\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.True,"Library");
 }else {
 //BA.debugLineNum = 111;BA.debugLine="function.FullScreen(False, \"Library\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.False,"Library");
 };
 //BA.debugLineNum = 113;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 114;BA.debugLine="End Sub";
return "";
}
public static String  _anim_animationend() throws Exception{
 //BA.debugLineNum = 227;BA.debugLine="Sub anim_animationend";
 //BA.debugLineNum = 228;BA.debugLine="pnlPop.RemoveView";
mostCurrent._pnlpop.RemoveView();
 //BA.debugLineNum = 229;BA.debugLine="pnlInfo.RemoveView";
mostCurrent._pnlinfo.RemoveView();
 //BA.debugLineNum = 230;BA.debugLine="imgInfo.RemoveView";
mostCurrent._imginfo.RemoveView();
 //BA.debugLineNum = 231;BA.debugLine="lblName.RemoveView";
mostCurrent._lblname.RemoveView();
 //BA.debugLineNum = 232;BA.debugLine="scroInfo.RemoveView";
mostCurrent._scroinfo.RemoveView();
 //BA.debugLineNum = 233;BA.debugLine="lblTrivia.RemoveView";
mostCurrent._lbltrivia.RemoveView();
 //BA.debugLineNum = 234;BA.debugLine="End Sub";
return "";
}
public static String  _anim1_animationend() throws Exception{
 //BA.debugLineNum = 331;BA.debugLine="Sub anim1_animationend";
 //BA.debugLineNum = 332;BA.debugLine="pnlBg.RemoveAllViews";
mostCurrent._pnlbg.RemoveAllViews();
 //BA.debugLineNum = 333;BA.debugLine="pnlBg.RemoveView";
mostCurrent._pnlbg.RemoveView();
 //BA.debugLineNum = 334;BA.debugLine="btnSet.Enabled = True";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 335;BA.debugLine="End Sub";
return "";
}
public static String  _btnback_click() throws Exception{
 //BA.debugLineNum = 121;BA.debugLine="Sub btnBack_Click";
 //BA.debugLineNum = 122;BA.debugLine="If timer.Enabled == True Then";
if (_timer.getEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 123;BA.debugLine="ToastMessageShow(\"You cant go back while loading";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("You cant go back while loading the data",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 125;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 126;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 127;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromle";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 };
 //BA.debugLineNum = 129;BA.debugLine="End Sub";
return "";
}
public static String  _btnclearhs_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 337;BA.debugLine="Sub btnClearHS_Click";
 //BA.debugLineNum = 338;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 339;BA.debugLine="res = Msgbox2(\"Do you really want to clear the hi";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want to clear the highscore?","Reset","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 340;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 341;BA.debugLine="sql.ExecNonQuery(\"DELETE FROM hscore\")";
_sql.ExecNonQuery("DELETE FROM hscore");
 //BA.debugLineNum = 342;BA.debugLine="StateManager.SetSetting(\"pnum\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"pnum","0");
 //BA.debugLineNum = 343;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 344;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 346;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 348;BA.debugLine="End Sub";
return "";
}
public static String  _btnreset_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 319;BA.debugLine="Sub btnReset_Click";
 //BA.debugLineNum = 320;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 321;BA.debugLine="res = Msgbox2(\"Do you really want reset the game?";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want reset the game?"+anywheresoftware.b4a.keywords.Common.CRLF+"*note that data will deleted including game progress and library.","Reset game","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 322;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 323;BA.debugLine="function.resetall(sql)";
mostCurrent._function._resetall(mostCurrent.activityBA,_sql);
 //BA.debugLineNum = 324;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 325;BA.debugLine="listTrivia.Clear";
mostCurrent._listtrivia.Clear();
 }else {
 //BA.debugLineNum = 327;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 329;BA.debugLine="End Sub";
return "";
}
public static String  _btnset_click() throws Exception{
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _checked = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _unchecked = null;
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed2 = null;
int _fstat2 = 0;
boolean _s = false;
float _vol = 0f;
 //BA.debugLineNum = 236;BA.debugLine="Sub btnSet_Click";
 //BA.debugLineNum = 237;BA.debugLine="Dim sld As StateListDrawable";
_sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 238;BA.debugLine="Dim checked As BitmapDrawable";
_checked = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 239;BA.debugLine="Dim unchecked As ColorDrawable";
_unchecked = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 240;BA.debugLine="Dim sld2 As StateListDrawable";
_sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 241;BA.debugLine="Dim enabled2, pressed2 As BitmapDrawable";
_enabled2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 244;BA.debugLine="enabled2.Initialize(LoadBitmapSample(File.DirAsse";
_enabled2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 245;BA.debugLine="pressed2.Initialize(LoadBitmapSample(File.DirAsse";
_pressed2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 246;BA.debugLine="sld2.Initialize";
_sld2.Initialize();
 //BA.debugLineNum = 247;BA.debugLine="sld2.AddState(sld2.State_Pressed, pressed2)";
_sld2.AddState(_sld2.State_Pressed,(android.graphics.drawable.Drawable)(_pressed2.getObject()));
 //BA.debugLineNum = 248;BA.debugLine="sld2.AddState(sld2.State_Enabled, enabled2)";
_sld2.AddState(_sld2.State_Enabled,(android.graphics.drawable.Drawable)(_enabled2.getObject()));
 //BA.debugLineNum = 250;BA.debugLine="checked.Initialize(LoadBitmap(File.DirAssets, \"tg";
_checked.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"tglc.png").getObject()));
 //BA.debugLineNum = 251;BA.debugLine="unchecked.Initialize(Colors.White, 0)";
_unchecked.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 252;BA.debugLine="sld.Initialize";
_sld.Initialize();
 //BA.debugLineNum = 253;BA.debugLine="sld.AddState(sld.State_Unchecked, unchecked)";
_sld.AddState(_sld.State_Unchecked,(android.graphics.drawable.Drawable)(_unchecked.getObject()));
 //BA.debugLineNum = 254;BA.debugLine="sld.AddState(sld.State_Checked, checked)";
_sld.AddState(_sld.State_Checked,(android.graphics.drawable.Drawable)(_checked.getObject()));
 //BA.debugLineNum = 255;BA.debugLine="Dim fstat2 = StateManager.GetSetting2(\"fullscreen";
_fstat2 = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 256;BA.debugLine="Dim s As Boolean";
_s = false;
 //BA.debugLineNum = 257;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (float)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 258;BA.debugLine="If fstat2 == 1 Then";
if (_fstat2==1) { 
 //BA.debugLineNum = 259;BA.debugLine="s = True";
_s = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 261;BA.debugLine="s = False";
_s = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 263;BA.debugLine="If setclick == False Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 264;BA.debugLine="Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 265;BA.debugLine="pnlBg.LoadLayout(\"5\")";
mostCurrent._pnlbg.LoadLayout("5",mostCurrent.activityBA);
 //BA.debugLineNum = 266;BA.debugLine="pnlCur.Width = (vol/10)*pnlVol.Width";
mostCurrent._pnlcur.setWidth((int) ((_vol/(double)10)*mostCurrent._pnlvol.getWidth()));
 //BA.debugLineNum = 267;BA.debugLine="pnlBg.Color = 0x64000000";
mostCurrent._pnlbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 268;BA.debugLine="tglFull.Background = sld";
mostCurrent._tglfull.setBackground((android.graphics.drawable.Drawable)(_sld.getObject()));
 //BA.debugLineNum = 269;BA.debugLine="tglFull.Checked = s";
mostCurrent._tglfull.setChecked(_s);
 //BA.debugLineNum = 270;BA.debugLine="btnReset.Background = sld2";
mostCurrent._btnreset.setBackground((android.graphics.drawable.Drawable)(_sld2.getObject()));
 //BA.debugLineNum = 271;BA.debugLine="anim.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anim.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 272;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 273;BA.debugLine="setclick = True";
_setclick = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 275;BA.debugLine="anim.SlideToTop(\"anim1\",slideval, durval)";
mostCurrent._anim.SlideToTop(mostCurrent.activityBA,"anim1",_slideval,(long) (_durval));
 //BA.debugLineNum = 276;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 277;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 278;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 280;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 13;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 17;BA.debugLine="Private btnBack As Button";
mostCurrent._btnback = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private btnSet As Button";
mostCurrent._btnset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private imgInfo As ImageView";
mostCurrent._imginfo = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private pnlPop ,pnlInfo As Panel";
mostCurrent._pnlpop = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._pnlinfo = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Dim name, imgAni, aniTrivia As String";
mostCurrent._name = "";
mostCurrent._imgani = "";
mostCurrent._anitrivia = "";
 //BA.debugLineNum = 22;BA.debugLine="Dim id As Int";
_id = 0;
 //BA.debugLineNum = 23;BA.debugLine="Private listTrivia As ListView";
mostCurrent._listtrivia = new anywheresoftware.b4a.objects.ListViewWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private lblInfo ,lblName ,lblTrivia As Label";
mostCurrent._lblinfo = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblname = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lbltrivia = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private scroInfo As ScrollView";
mostCurrent._scroinfo = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Dim anim As ICOSSlideAnimation";
mostCurrent._anim = new giuseppe.salvi.icos.library.ICOSSlideAnimation();
 //BA.debugLineNum = 27;BA.debugLine="Dim label1, label2 As Label";
mostCurrent._label1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 28;BA.debugLine="Dim pnlLoad, pnlLoadBG As Panel";
mostCurrent._pnlload = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._pnlloadbg = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Dim progLoad As ProgressBar";
mostCurrent._progload = new anywheresoftware.b4a.objects.ProgressBarWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Dim screen As PhoneWakeState";
mostCurrent._screen = new anywheresoftware.b4a.phone.Phone.PhoneWakeState();
 //BA.debugLineNum = 31;BA.debugLine="Dim lblProg, lblLoading, lblProcessed As Label";
mostCurrent._lblprog = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblloading = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblprocessed = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Dim n = 0 , total As Int";
_n = (int) (0);
_total = 0;
 //BA.debugLineNum = 33;BA.debugLine="Dim cursor As Cursor";
mostCurrent._cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 34;BA.debugLine="Private pnlSet As Panel";
mostCurrent._pnlset = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 35;BA.debugLine="Private pnlVol As Panel";
mostCurrent._pnlvol = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private pnlCur As Panel";
mostCurrent._pnlcur = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private tglFull As ToggleButton";
mostCurrent._tglfull = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private btnReset As Button";
mostCurrent._btnreset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Dim setclick As Boolean";
_setclick = false;
 //BA.debugLineNum = 40;BA.debugLine="Private pnlBg As Panel";
mostCurrent._pnlbg = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 41;BA.debugLine="Dim slideval = 700, durval = 1500 As Float";
_slideval = (float) (700);
_durval = (float) (1500);
 //BA.debugLineNum = 42;BA.debugLine="Private btnClearHS As Button";
mostCurrent._btnclearhs = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 43;BA.debugLine="End Sub";
return "";
}
public static String  _listtrivia_itemclick(int _position,Object _value) throws Exception{
int _val = 0;
float _fheight = 0f;
String _animal = "";
String _pic = "";
String _trivia = "";
String _ans = "";
anywheresoftware.b4a.objects.drawable.ColorDrawable _border = null;
anywheresoftware.b4a.objects.StringUtils _su = null;
 //BA.debugLineNum = 156;BA.debugLine="Sub listTrivia_ItemClick (Position As Int, Value A";
 //BA.debugLineNum = 157;BA.debugLine="Dim val = Value As Int";
_val = (int)(BA.ObjectToNumber(_value));
 //BA.debugLineNum = 158;BA.debugLine="Dim fheight As Float";
_fheight = 0f;
 //BA.debugLineNum = 159;BA.debugLine="Dim animal, pic, trivia ,ans As String";
_animal = "";
_pic = "";
_trivia = "";
_ans = "";
 //BA.debugLineNum = 160;BA.debugLine="Dim cursor As Cursor";
mostCurrent._cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 161;BA.debugLine="Dim border As ColorDrawable";
_border = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 162;BA.debugLine="Dim su As StringUtils";
_su = new anywheresoftware.b4a.objects.StringUtils();
 //BA.debugLineNum = 164;BA.debugLine="border.Initialize2(0xFF28507B, 0, 4dip, Colors.Ye";
_border.Initialize2((int) (0xff28507b),(int) (0),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4)),anywheresoftware.b4a.keywords.Common.Colors.Yellow);
 //BA.debugLineNum = 166;BA.debugLine="cursor = sql.ExecQuery(\"SELECT * FROM game WHERE";
mostCurrent._cursor.setObject((android.database.Cursor)(_sql.ExecQuery("SELECT * FROM game WHERE id='"+BA.NumberToString(_val)+"'")));
 //BA.debugLineNum = 167;BA.debugLine="cursor.Position = 0";
mostCurrent._cursor.setPosition((int) (0));
 //BA.debugLineNum = 168;BA.debugLine="ans = cursor.GetString(\"ans\")";
_ans = mostCurrent._cursor.GetString("ans");
 //BA.debugLineNum = 169;BA.debugLine="animal = cursor.GetString(\"name\")";
_animal = mostCurrent._cursor.GetString("name");
 //BA.debugLineNum = 170;BA.debugLine="pic = function.getImgExt(cursor.GetString(ans))";
_pic = mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._cursor.GetString(_ans));
 //BA.debugLineNum = 171;BA.debugLine="trivia = cursor.GetString(\"trivia\")";
_trivia = mostCurrent._cursor.GetString("trivia");
 //BA.debugLineNum = 172;BA.debugLine="cursor.Close";
mostCurrent._cursor.Close();
 //BA.debugLineNum = 174;BA.debugLine="Activity.AddView(pnlPop, 0, 9%x, 100%x, 100%y - 9";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlpop.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 175;BA.debugLine="pnlPop.Color = 0x64000000";
mostCurrent._pnlpop.setColor((int) (0x64000000));
 //BA.debugLineNum = 177;BA.debugLine="pnlPop.AddView(pnlInfo, 2%x, 20%x, 96%x, 102%x)";
mostCurrent._pnlpop.AddView((android.view.View)(mostCurrent._pnlinfo.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (20),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (102),mostCurrent.activityBA));
 //BA.debugLineNum = 178;BA.debugLine="pnlInfo.Background = border";
mostCurrent._pnlinfo.setBackground((android.graphics.drawable.Drawable)(_border.getObject()));
 //BA.debugLineNum = 180;BA.debugLine="pnlInfo.AddView(lblName, 28%x, 2%x, 40%x, 9%x)";
mostCurrent._pnlinfo.AddView((android.view.View)(mostCurrent._lblname.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (28),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA));
 //BA.debugLineNum = 181;BA.debugLine="lblName.TextColor = 0xFFFFFFFF";
mostCurrent._lblname.setTextColor((int) (0xffffffff));
 //BA.debugLineNum = 182;BA.debugLine="lblName.TextSize = (9%x/100)*45";
mostCurrent._lblname.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)/(double)100)*45));
 //BA.debugLineNum = 183;BA.debugLine="lblName.Text = animal";
mostCurrent._lblname.setText((Object)(_animal));
 //BA.debugLineNum = 184;BA.debugLine="lblName.Gravity = Gravity.CENTER";
mostCurrent._lblname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 186;BA.debugLine="pnlInfo.AddView(imgInfo, 23%x, 11%x, 50%x, 50%x)";
mostCurrent._pnlinfo.AddView((android.view.View)(mostCurrent._imginfo.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (23),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (11),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA));
 //BA.debugLineNum = 187;BA.debugLine="imgInfo.Gravity = Gravity.FILL";
mostCurrent._imginfo.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.FILL);
 //BA.debugLineNum = 188;BA.debugLine="imgInfo.Bitmap = LoadBitmapSample(File.DirAssets,";
mostCurrent._imginfo.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),_pic,mostCurrent._imginfo.getWidth(),mostCurrent._imginfo.getHeight()).getObject()));
 //BA.debugLineNum = 190;BA.debugLine="pnlInfo.AddView(scroInfo, 2%x, 61%x, 94%x, 39%x)";
mostCurrent._pnlinfo.AddView((android.view.View)(mostCurrent._scroinfo.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (61),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (94),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (39),mostCurrent.activityBA));
 //BA.debugLineNum = 191;BA.debugLine="scroInfo.ScrollPosition = 0";
mostCurrent._scroinfo.setScrollPosition((int) (0));
 //BA.debugLineNum = 192;BA.debugLine="scroInfo.Panel.Height = 200%x";
mostCurrent._scroinfo.getPanel().setHeight(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (200),mostCurrent.activityBA));
 //BA.debugLineNum = 193;BA.debugLine="scroInfo.Panel.AddView(lblTrivia, 2%x, 0, 90%x, 2";
mostCurrent._scroinfo.getPanel().AddView((android.view.View)(mostCurrent._lbltrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (90),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (200),mostCurrent.activityBA));
 //BA.debugLineNum = 194;BA.debugLine="lblTrivia.TextColor = 0xFFFFFFFF";
mostCurrent._lbltrivia.setTextColor((int) (0xffffffff));
 //BA.debugLineNum = 195;BA.debugLine="lblTrivia.TextSize = (9%x/100)*40";
mostCurrent._lbltrivia.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)/(double)100)*40));
 //BA.debugLineNum = 196;BA.debugLine="lblTrivia.Text = trivia";
mostCurrent._lbltrivia.setText((Object)(_trivia));
 //BA.debugLineNum = 197;BA.debugLine="lblTrivia.Gravity = Gravity.CENTER_HORIZONTAL";
mostCurrent._lbltrivia.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 199;BA.debugLine="anim.SlideFromTop(\"\", 650, 1500)";
mostCurrent._anim.SlideFromTop(mostCurrent.activityBA,"",(float) (650),(long) (1500));
 //BA.debugLineNum = 200;BA.debugLine="anim.StartAnim(pnlInfo)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlinfo.getObject()));
 //BA.debugLineNum = 202;BA.debugLine="fheight = su.MeasureMultilineTextHeight(lblTrivia";
_fheight = (float) (_su.MeasureMultilineTextHeight((android.widget.TextView)(mostCurrent._lbltrivia.getObject()),mostCurrent._lbltrivia.getText()));
 //BA.debugLineNum = 203;BA.debugLine="lblTrivia.Height = fheight";
mostCurrent._lbltrivia.setHeight((int) (_fheight));
 //BA.debugLineNum = 204;BA.debugLine="scroInfo.Panel.Height = fheight";
mostCurrent._scroinfo.getPanel().setHeight((int) (_fheight));
 //BA.debugLineNum = 208;BA.debugLine="End Sub";
return "";
}
public static String  _pnlbg_click() throws Exception{
 //BA.debugLineNum = 282;BA.debugLine="Sub pnlBg_Click";
 //BA.debugLineNum = 283;BA.debugLine="If setclick == True Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 284;BA.debugLine="anim.SlideToTop(\"anim1\",slideval, durval)";
mostCurrent._anim.SlideToTop(mostCurrent.activityBA,"anim1",_slideval,(long) (_durval));
 //BA.debugLineNum = 285;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 286;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 287;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 289;BA.debugLine="End Sub";
return "";
}
public static String  _pnlloadbg_click() throws Exception{
 //BA.debugLineNum = 210;BA.debugLine="Sub pnlLoadBG_Click";
 //BA.debugLineNum = 212;BA.debugLine="End Sub";
return "";
}
public static String  _pnlpop_click() throws Exception{
 //BA.debugLineNum = 222;BA.debugLine="Sub pnlPop_Click";
 //BA.debugLineNum = 223;BA.debugLine="anim.SlideToTop(\"anim\",650, 1000)";
mostCurrent._anim.SlideToTop(mostCurrent.activityBA,"anim",(float) (650),(long) (1000));
 //BA.debugLineNum = 224;BA.debugLine="anim.StartAnim(pnlInfo)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlinfo.getObject()));
 //BA.debugLineNum = 225;BA.debugLine="End Sub";
return "";
}
public static String  _pnlset_click() throws Exception{
 //BA.debugLineNum = 291;BA.debugLine="Sub pnlSet_Click";
 //BA.debugLineNum = 293;BA.debugLine="End Sub";
return "";
}
public static String  _pnlvol_touch(int _action,float _x,float _y) throws Exception{
float _a = 0f;
 //BA.debugLineNum = 295;BA.debugLine="Sub pnlVol_Touch (Action As Int, X As Float, Y As";
 //BA.debugLineNum = 296;BA.debugLine="Dim a As Float";
_a = 0f;
 //BA.debugLineNum = 297;BA.debugLine="If X < 0 Then";
if (_x<0) { 
 //BA.debugLineNum = 298;BA.debugLine="X = 0";
_x = (float) (0);
 }else if(_x>mostCurrent._pnlvol.getWidth()) { 
 //BA.debugLineNum = 300;BA.debugLine="X = pnlVol.Width";
_x = (float) (mostCurrent._pnlvol.getWidth());
 };
 //BA.debugLineNum = 302;BA.debugLine="a = (X/pnlVol.Width)*10";
_a = (float) ((_x/(double)mostCurrent._pnlvol.getWidth())*10);
 //BA.debugLineNum = 303;BA.debugLine="StateManager.SetSetting(\"volume\", a)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"volume",BA.NumberToString(_a));
 //BA.debugLineNum = 304;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 305;BA.debugLine="pnlCur.Width = X";
mostCurrent._pnlcur.setWidth((int) (_x));
 //BA.debugLineNum = 306;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim sql As SQL";
_sql = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 10;BA.debugLine="Dim timer As Timer";
_timer = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return "";
}
public static String  _tglfull_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 308;BA.debugLine="Sub tglFull_CheckedChange(Checked As Boolean)";
 //BA.debugLineNum = 309;BA.debugLine="If Checked Then";
if (_checked) { 
 //BA.debugLineNum = 310;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"1\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","1");
 }else {
 //BA.debugLineNum = 312;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","0");
 };
 //BA.debugLineNum = 314;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 315;BA.debugLine="function.FullScreen(Checked, \"Library\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,_checked,"Library");
 //BA.debugLineNum = 316;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 317;BA.debugLine="End Sub";
return "";
}
public static String  _timer_tick() throws Exception{
 //BA.debugLineNum = 131;BA.debugLine="Sub timer_Tick";
 //BA.debugLineNum = 132;BA.debugLine="n = n + 1";
_n = (int) (_n+1);
 //BA.debugLineNum = 133;BA.debugLine="If n > 0 And n < total+1 Then";
if (_n>0 && _n<_total+1) { 
 //BA.debugLineNum = 134;BA.debugLine="progLoad.Progress = (n/total)*100";
mostCurrent._progload.setProgress((int) ((_n/(double)_total)*100));
 //BA.debugLineNum = 135;BA.debugLine="cursor.Position = n-1";
mostCurrent._cursor.setPosition((int) (_n-1));
 //BA.debugLineNum = 136;BA.debugLine="name = cursor.GetString(\"name\")";
mostCurrent._name = mostCurrent._cursor.GetString("name");
 //BA.debugLineNum = 137;BA.debugLine="id = cursor.GetInt(\"id\")";
_id = mostCurrent._cursor.GetInt("id");
 //BA.debugLineNum = 138;BA.debugLine="aniTrivia = cursor.GetString(\"trivia\").SubString";
mostCurrent._anitrivia = mostCurrent._cursor.GetString("trivia").substring((int) (0),(int) (50))+"...";
 //BA.debugLineNum = 139;BA.debugLine="imgAni = cursor.GetString(cursor.GetString(\"ans\"";
mostCurrent._imgani = mostCurrent._cursor.GetString(mostCurrent._cursor.GetString("ans"));
 //BA.debugLineNum = 140;BA.debugLine="lblProg.Text = progLoad.Progress & \"%\"";
mostCurrent._lblprog.setText((Object)(BA.NumberToString(mostCurrent._progload.getProgress())+"%"));
 //BA.debugLineNum = 141;BA.debugLine="lblProcessed.Text = n & \"/\" & total";
mostCurrent._lblprocessed.setText((Object)(BA.NumberToString(_n)+"/"+BA.NumberToString(_total)));
 //BA.debugLineNum = 142;BA.debugLine="listTrivia.AddTwoLinesAndBitmap2(name, aniTrivia";
mostCurrent._listtrivia.AddTwoLinesAndBitmap2(mostCurrent._name,mostCurrent._anitrivia,(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._imgani),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (15),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (15),mostCurrent.activityBA)).getObject()),(Object)(_id));
 }else if(_n==_total+1) { 
 //BA.debugLineNum = 144;BA.debugLine="lblLoading.Text = \"Done\"";
mostCurrent._lblloading.setText((Object)("Done"));
 //BA.debugLineNum = 145;BA.debugLine="lblProg.Text = \"100%\"";
mostCurrent._lblprog.setText((Object)("100%"));
 //BA.debugLineNum = 146;BA.debugLine="lblProcessed.Text = total & \"/\" & total";
mostCurrent._lblprocessed.setText((Object)(BA.NumberToString(_total)+"/"+BA.NumberToString(_total)));
 }else if(_n==_total+20) { 
 //BA.debugLineNum = 148;BA.debugLine="pnlLoad.RemoveAllViews";
mostCurrent._pnlload.RemoveAllViews();
 //BA.debugLineNum = 149;BA.debugLine="pnlLoadBG.RemoveAllViews";
mostCurrent._pnlloadbg.RemoveAllViews();
 //BA.debugLineNum = 150;BA.debugLine="pnlLoadBG.RemoveView";
mostCurrent._pnlloadbg.RemoveView();
 //BA.debugLineNum = 151;BA.debugLine="timer.Enabled = False";
_timer.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 152;BA.debugLine="cursor.Close";
mostCurrent._cursor.Close();
 };
 //BA.debugLineNum = 154;BA.debugLine="End Sub";
return "";
}
}
