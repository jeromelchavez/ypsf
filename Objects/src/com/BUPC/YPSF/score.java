package com.BUPC.YPSF;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class score extends Activity implements B4AActivity{
	public static score mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "com.BUPC.YPSF", "com.BUPC.YPSF.score");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (score).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
		BA.handler.postDelayed(new WaitForLayout(), 5);

	}
	private static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "com.BUPC.YPSF", "com.BUPC.YPSF.score");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "com.BUPC.YPSF.score", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (score) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (score) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return score.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (score) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (score) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.sql.SQL _db = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnback = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnset = null;
public anywheresoftware.b4a.phone.Phone.PhoneWakeState _screen = null;
public anywheresoftware.b4a.objects.LabelWrapper _label1 = null;
public anywheresoftware.b4a.objects.LabelWrapper _label2 = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlvol = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcur = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _tglfull = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnreset = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlset = null;
public giuseppe.salvi.icos.library.ICOSSlideAnimation _anim = null;
public static boolean _setclick = false;
public static float _slideval = 0f;
public static float _durval = 0f;
public anywheresoftware.b4a.objects.PanelWrapper _pnlbg = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _scroscore = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblscore = null;
public anywheresoftware.b4a.objects.StringUtils _su = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnclearhs = null;
public com.BUPC.YPSF.main _main = null;
public com.BUPC.YPSF.play _play = null;
public com.BUPC.YPSF.statemanager _statemanager = null;
public com.BUPC.YPSF.function _function = null;
public com.BUPC.YPSF.tutorial _tutorial = null;
public com.BUPC.YPSF.library _library = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
int _fontsize = 0;
float _finalheight = 0f;
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor = null;
String _name = "";
String _name2 = "";
String _score = "";
String _score2 = "";
String _aaa = "";
int _totalrows = 0;
int _i = 0;
 //BA.debugLineNum = 36;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 37;BA.debugLine="Dim fontsize = (10%x/100)*40 As Int";
_fontsize = (int) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*40);
 //BA.debugLineNum = 38;BA.debugLine="Dim finalheight As Float";
_finalheight = 0f;
 //BA.debugLineNum = 39;BA.debugLine="If File.Exists(File.DirInternal,\"db\") == False Th";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db")==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 40;BA.debugLine="File.Copy(File.DirAssets, \"db.db\", File.DirInter";
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"db.db",anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db");
 };
 //BA.debugLineNum = 42;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 43;BA.debugLine="db.Initialize(File.DirInternal,\"db\", False)";
_db.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db",anywheresoftware.b4a.keywords.Common.False);
 };
 //BA.debugLineNum = 45;BA.debugLine="pnlBg.Initialize(\"pnlBg\")";
mostCurrent._pnlbg.Initialize(mostCurrent.activityBA,"pnlBg");
 //BA.debugLineNum = 46;BA.debugLine="lblName.Initialize(\"\")";
mostCurrent._lblname.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 47;BA.debugLine="lblScore.Initialize(\"\")";
mostCurrent._lblscore.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 48;BA.debugLine="screen.KeepAlive(True)";
mostCurrent._screen.KeepAlive(processBA,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 49;BA.debugLine="screen.PartialLock";
mostCurrent._screen.PartialLock(processBA);
 //BA.debugLineNum = 50;BA.debugLine="Activity.LoadLayout(\"3\")";
mostCurrent._activity.LoadLayout("3",mostCurrent.activityBA);
 //BA.debugLineNum = 51;BA.debugLine="scroScore.Panel.Height = 1500dip";
mostCurrent._scroscore.getPanel().setHeight(anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1500)));
 //BA.debugLineNum = 52;BA.debugLine="scroScore.Panel.AddView(lblName, 0, 0, 80%x, scro";
mostCurrent._scroscore.getPanel().AddView((android.view.View)(mostCurrent._lblname.getObject()),(int) (0),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (80),mostCurrent.activityBA),mostCurrent._scroscore.getPanel().getHeight());
 //BA.debugLineNum = 53;BA.debugLine="scroScore.Panel.AddView(lblScore, 80%x, 0, 20%x,";
mostCurrent._scroscore.getPanel().AddView((android.view.View)(mostCurrent._lblscore.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (80),mostCurrent.activityBA),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (20),mostCurrent.activityBA),mostCurrent._scroscore.getPanel().getHeight());
 //BA.debugLineNum = 54;BA.debugLine="lblName.TextSize = fontsize";
mostCurrent._lblname.setTextSize((float) (_fontsize));
 //BA.debugLineNum = 55;BA.debugLine="lblScore.TextSize = fontsize";
mostCurrent._lblscore.setTextSize((float) (_fontsize));
 //BA.debugLineNum = 56;BA.debugLine="lblName.TextColor = Colors.White";
mostCurrent._lblname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 57;BA.debugLine="lblScore.TextColor = Colors.White";
mostCurrent._lblscore.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 59;BA.debugLine="Dim cursor As Cursor";
_cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 60;BA.debugLine="Dim name, name2, score, score2, aaa As String";
_name = "";
_name2 = "";
_score = "";
_score2 = "";
_aaa = "";
 //BA.debugLineNum = 61;BA.debugLine="Dim totalrows As Int";
_totalrows = 0;
 //BA.debugLineNum = 62;BA.debugLine="cursor = db.ExecQuery(\"SELECT * FROM hscore ORDE";
_cursor.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM hscore ORDER BY rscore DESC LIMIT 20")));
 //BA.debugLineNum = 63;BA.debugLine="totalrows = cursor.RowCount";
_totalrows = _cursor.getRowCount();
 //BA.debugLineNum = 64;BA.debugLine="For i = 0 To totalrows - 1";
{
final int step49 = 1;
final int limit49 = (int) (_totalrows-1);
for (_i = (int) (0); (step49 > 0 && _i <= limit49) || (step49 < 0 && _i >= limit49); _i = ((int)(0 + _i + step49))) {
 //BA.debugLineNum = 65;BA.debugLine="cursor.Position = i";
_cursor.setPosition(_i);
 //BA.debugLineNum = 66;BA.debugLine="name = cursor.GetString(\"name\")";
_name = _cursor.GetString("name");
 //BA.debugLineNum = 67;BA.debugLine="score = cursor.GetString(\"score\")";
_score = _cursor.GetString("score");
 //BA.debugLineNum = 68;BA.debugLine="name2 = (i+1) &\".\" & TAB & name.Replace(CRLF, C";
_name2 = BA.NumberToString((_i+1))+"."+anywheresoftware.b4a.keywords.Common.TAB+_name.replace(anywheresoftware.b4a.keywords.Common.CRLF,anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.TAB+"  ")+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF;
 //BA.debugLineNum = 69;BA.debugLine="score2 = score & CRLF & CRLF";
_score2 = _score+anywheresoftware.b4a.keywords.Common.CRLF+anywheresoftware.b4a.keywords.Common.CRLF;
 //BA.debugLineNum = 70;BA.debugLine="lblName.Text = lblName.Text  & name2";
mostCurrent._lblname.setText((Object)(mostCurrent._lblname.getText()+_name2));
 //BA.debugLineNum = 71;BA.debugLine="lblScore.Text =  lblScore.Text & score2";
mostCurrent._lblscore.setText((Object)(mostCurrent._lblscore.getText()+_score2));
 }
};
 //BA.debugLineNum = 73;BA.debugLine="finalheight = su.MeasureMultilineTextHeight(lblN";
_finalheight = (float) (mostCurrent._su.MeasureMultilineTextHeight((android.widget.TextView)(mostCurrent._lblname.getObject()),mostCurrent._lblname.getText()));
 //BA.debugLineNum = 74;BA.debugLine="lblName.Height = finalheight";
mostCurrent._lblname.setHeight((int) (_finalheight));
 //BA.debugLineNum = 75;BA.debugLine="lblScore.Height = finalheight";
mostCurrent._lblscore.setHeight((int) (_finalheight));
 //BA.debugLineNum = 76;BA.debugLine="scroScore.Panel.Height = finalheight";
mostCurrent._scroscore.getPanel().setHeight((int) (_finalheight));
 //BA.debugLineNum = 77;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
 //BA.debugLineNum = 100;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 101;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 102;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 }else if(_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_MENU && mostCurrent._btnset.getEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 104;BA.debugLine="btnSet_Click";
_btnset_click();
 };
 //BA.debugLineNum = 106;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 89;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 90;BA.debugLine="screen.ReleaseKeepAlive";
mostCurrent._screen.ReleaseKeepAlive();
 //BA.debugLineNum = 91;BA.debugLine="screen.ReleasePartialLock";
mostCurrent._screen.ReleasePartialLock();
 //BA.debugLineNum = 92;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
int _fstat = 0;
 //BA.debugLineNum = 79;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 80;BA.debugLine="Dim fstat = StateManager.GetSetting2(\"fullscreen\"";
_fstat = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 81;BA.debugLine="If fstat == 1 Then";
if (_fstat==1) { 
 //BA.debugLineNum = 82;BA.debugLine="function.FullScreen(True, \"Score\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.True,"Score");
 }else {
 //BA.debugLineNum = 84;BA.debugLine="function.FullScreen(False, \"Score\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.False,"Score");
 };
 //BA.debugLineNum = 86;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 87;BA.debugLine="End Sub";
return "";
}
public static String  _anim_animationend() throws Exception{
 //BA.debugLineNum = 200;BA.debugLine="Sub anim_animationend";
 //BA.debugLineNum = 201;BA.debugLine="pnlBg.RemoveAllViews";
mostCurrent._pnlbg.RemoveAllViews();
 //BA.debugLineNum = 202;BA.debugLine="pnlBg.RemoveView";
mostCurrent._pnlbg.RemoveView();
 //BA.debugLineNum = 203;BA.debugLine="btnSet.Enabled = True";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 204;BA.debugLine="End Sub";
return "";
}
public static String  _btnback_click() throws Exception{
 //BA.debugLineNum = 94;BA.debugLine="Sub btnBack_Click";
 //BA.debugLineNum = 95;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 96;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 97;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromlef";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 //BA.debugLineNum = 98;BA.debugLine="End Sub";
return "";
}
public static String  _btnclearhs_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 206;BA.debugLine="Sub btnClearHS_Click";
 //BA.debugLineNum = 207;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 208;BA.debugLine="res = Msgbox2(\"Do you really want to clear the hi";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want to clear the highscore?","Reset","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 209;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 210;BA.debugLine="db.ExecNonQuery(\"DELETE FROM hscore\")";
_db.ExecNonQuery("DELETE FROM hscore");
 //BA.debugLineNum = 211;BA.debugLine="StateManager.SetSetting(\"pnum\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"pnum","0");
 //BA.debugLineNum = 212;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 213;BA.debugLine="lblName.Text = \"\"";
mostCurrent._lblname.setText((Object)(""));
 //BA.debugLineNum = 214;BA.debugLine="lblScore.Text = \"\"";
mostCurrent._lblscore.setText((Object)(""));
 //BA.debugLineNum = 215;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 217;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 219;BA.debugLine="End Sub";
return "";
}
public static String  _btnreset_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 189;BA.debugLine="Sub btnReset_Click";
 //BA.debugLineNum = 190;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 191;BA.debugLine="res = Msgbox2(\"Do you really want reset the game?";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want reset the game?"+anywheresoftware.b4a.keywords.Common.CRLF+"*note that data will deleted including game progress and library.","Reset game","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 192;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 193;BA.debugLine="function.resetall(db)";
mostCurrent._function._resetall(mostCurrent.activityBA,_db);
 //BA.debugLineNum = 194;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 196;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 198;BA.debugLine="End Sub";
return "";
}
public static String  _btnset_click() throws Exception{
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _checked = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _unchecked = null;
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed2 = null;
int _fstat2 = 0;
boolean _s = false;
float _vol = 0f;
 //BA.debugLineNum = 108;BA.debugLine="Sub btnSet_Click";
 //BA.debugLineNum = 109;BA.debugLine="Dim sld As StateListDrawable";
_sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 110;BA.debugLine="Dim checked As BitmapDrawable";
_checked = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 111;BA.debugLine="Dim unchecked As ColorDrawable";
_unchecked = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 112;BA.debugLine="Dim sld2 As StateListDrawable";
_sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 113;BA.debugLine="Dim enabled2, pressed2 As BitmapDrawable";
_enabled2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 115;BA.debugLine="enabled2.Initialize(LoadBitmapSample(File.DirAsse";
_enabled2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 116;BA.debugLine="pressed2.Initialize(LoadBitmapSample(File.DirAsse";
_pressed2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 117;BA.debugLine="sld2.Initialize";
_sld2.Initialize();
 //BA.debugLineNum = 118;BA.debugLine="sld2.AddState(sld.State_Pressed, pressed2)";
_sld2.AddState(_sld.State_Pressed,(android.graphics.drawable.Drawable)(_pressed2.getObject()));
 //BA.debugLineNum = 119;BA.debugLine="sld2.AddState(sld.State_Enabled, enabled2)";
_sld2.AddState(_sld.State_Enabled,(android.graphics.drawable.Drawable)(_enabled2.getObject()));
 //BA.debugLineNum = 121;BA.debugLine="checked.Initialize(LoadBitmap(File.DirAssets, \"tg";
_checked.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"tglc.png").getObject()));
 //BA.debugLineNum = 122;BA.debugLine="unchecked.Initialize(Colors.White, 0)";
_unchecked.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 123;BA.debugLine="sld.Initialize";
_sld.Initialize();
 //BA.debugLineNum = 124;BA.debugLine="sld.AddState(sld.State_Unchecked, unchecked)";
_sld.AddState(_sld.State_Unchecked,(android.graphics.drawable.Drawable)(_unchecked.getObject()));
 //BA.debugLineNum = 125;BA.debugLine="sld.AddState(sld.State_Checked, checked)";
_sld.AddState(_sld.State_Checked,(android.graphics.drawable.Drawable)(_checked.getObject()));
 //BA.debugLineNum = 126;BA.debugLine="Dim fstat2 = StateManager.GetSetting2(\"fullscreen";
_fstat2 = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 127;BA.debugLine="Dim s As Boolean";
_s = false;
 //BA.debugLineNum = 128;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (float)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 129;BA.debugLine="If fstat2 == 1 Then";
if (_fstat2==1) { 
 //BA.debugLineNum = 130;BA.debugLine="s = True";
_s = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 132;BA.debugLine="s = False";
_s = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 134;BA.debugLine="If setclick == False Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 135;BA.debugLine="Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 136;BA.debugLine="pnlBg.LoadLayout(\"5\")";
mostCurrent._pnlbg.LoadLayout("5",mostCurrent.activityBA);
 //BA.debugLineNum = 137;BA.debugLine="pnlCur.Width = (vol/10)*pnlVol.Width";
mostCurrent._pnlcur.setWidth((int) ((_vol/(double)10)*mostCurrent._pnlvol.getWidth()));
 //BA.debugLineNum = 138;BA.debugLine="pnlBg.Color = 0x64000000";
mostCurrent._pnlbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 139;BA.debugLine="tglFull.Background = sld";
mostCurrent._tglfull.setBackground((android.graphics.drawable.Drawable)(_sld.getObject()));
 //BA.debugLineNum = 140;BA.debugLine="tglFull.Checked = s";
mostCurrent._tglfull.setChecked(_s);
 //BA.debugLineNum = 141;BA.debugLine="btnReset.Background = sld2";
mostCurrent._btnreset.setBackground((android.graphics.drawable.Drawable)(_sld2.getObject()));
 //BA.debugLineNum = 142;BA.debugLine="anim.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anim.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 143;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 144;BA.debugLine="setclick = True";
_setclick = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 146;BA.debugLine="anim.SlideToTop(\"anim\",slideval, durval)";
mostCurrent._anim.SlideToTop(mostCurrent.activityBA,"anim",_slideval,(long) (_durval));
 //BA.debugLineNum = 147;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 148;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 149;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 151;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 13;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 16;BA.debugLine="Private btnBack As Button";
mostCurrent._btnback = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 17;BA.debugLine="Private btnSet As Button";
mostCurrent._btnset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Dim screen As PhoneWakeState";
mostCurrent._screen = new anywheresoftware.b4a.phone.Phone.PhoneWakeState();
 //BA.debugLineNum = 20;BA.debugLine="Private label1, label2 As Label";
mostCurrent._label1 = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._label2 = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private pnlVol As Panel";
mostCurrent._pnlvol = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private pnlCur As Panel";
mostCurrent._pnlcur = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Private tglFull As ToggleButton";
mostCurrent._tglfull = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
 //BA.debugLineNum = 24;BA.debugLine="Private btnReset As Button";
mostCurrent._btnreset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Private pnlSet As Panel";
mostCurrent._pnlset = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 26;BA.debugLine="Dim anim As ICOSSlideAnimation";
mostCurrent._anim = new giuseppe.salvi.icos.library.ICOSSlideAnimation();
 //BA.debugLineNum = 27;BA.debugLine="Dim setclick As Boolean";
_setclick = false;
 //BA.debugLineNum = 28;BA.debugLine="Dim slideval = 700, durval = 1500 As Float";
_slideval = (float) (700);
_durval = (float) (1500);
 //BA.debugLineNum = 29;BA.debugLine="Private pnlBg As Panel";
mostCurrent._pnlbg = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Private scroScore As ScrollView";
mostCurrent._scroscore = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 31;BA.debugLine="Private lblName, lblScore As Label";
mostCurrent._lblname = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblscore = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 32;BA.debugLine="Dim su As StringUtils";
mostCurrent._su = new anywheresoftware.b4a.objects.StringUtils();
 //BA.debugLineNum = 33;BA.debugLine="Private btnClearHS As Button";
mostCurrent._btnclearhs = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 34;BA.debugLine="End Sub";
return "";
}
public static String  _pnlbg_click() throws Exception{
 //BA.debugLineNum = 153;BA.debugLine="Sub pnlBg_Click";
 //BA.debugLineNum = 154;BA.debugLine="If setclick == True Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 155;BA.debugLine="anim.SlideToTop(\"anim\",slideval, durval)";
mostCurrent._anim.SlideToTop(mostCurrent.activityBA,"anim",_slideval,(long) (_durval));
 //BA.debugLineNum = 156;BA.debugLine="anim.StartAnim(pnlSet)";
mostCurrent._anim.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 157;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 158;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 160;BA.debugLine="End Sub";
return "";
}
public static String  _pnlset_click() throws Exception{
 //BA.debugLineNum = 162;BA.debugLine="Sub pnlSet_Click";
 //BA.debugLineNum = 164;BA.debugLine="End Sub";
return "";
}
public static String  _pnlvol_touch(int _action,float _x,float _y) throws Exception{
float _a = 0f;
 //BA.debugLineNum = 166;BA.debugLine="Sub pnlVol_Touch (Action As Int, X As Float, Y As";
 //BA.debugLineNum = 167;BA.debugLine="Dim a As Float";
_a = 0f;
 //BA.debugLineNum = 168;BA.debugLine="If X < 0 Then";
if (_x<0) { 
 //BA.debugLineNum = 169;BA.debugLine="X = 0";
_x = (float) (0);
 }else if(_x>mostCurrent._pnlvol.getWidth()) { 
 //BA.debugLineNum = 171;BA.debugLine="X = pnlVol.Width";
_x = (float) (mostCurrent._pnlvol.getWidth());
 };
 //BA.debugLineNum = 173;BA.debugLine="a = (X/pnlVol.Width)*10";
_a = (float) ((_x/(double)mostCurrent._pnlvol.getWidth())*10);
 //BA.debugLineNum = 174;BA.debugLine="StateManager.SetSetting(\"volume\", a)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"volume",BA.NumberToString(_a));
 //BA.debugLineNum = 175;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 176;BA.debugLine="pnlCur.Width = X";
mostCurrent._pnlcur.setWidth((int) (_x));
 //BA.debugLineNum = 177;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim  db As SQL";
_db = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 11;BA.debugLine="End Sub";
return "";
}
public static String  _tglfull_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 179;BA.debugLine="Sub tglFull_CheckedChange(Checked As Boolean)";
 //BA.debugLineNum = 180;BA.debugLine="If Checked Then";
if (_checked) { 
 //BA.debugLineNum = 181;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"1\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","1");
 }else {
 //BA.debugLineNum = 183;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","0");
 };
 //BA.debugLineNum = 185;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 186;BA.debugLine="function.FullScreen(Checked, \"Score\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,_checked,"Score");
 //BA.debugLineNum = 187;BA.debugLine="End Sub";
return "";
}
}
