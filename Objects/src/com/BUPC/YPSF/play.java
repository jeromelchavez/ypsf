package com.BUPC.YPSF;


import anywheresoftware.b4a.B4AMenuItem;
import android.app.Activity;
import android.os.Bundle;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.ObjectWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import java.lang.reflect.InvocationTargetException;
import anywheresoftware.b4a.B4AUncaughtException;
import anywheresoftware.b4a.debug.*;
import java.lang.ref.WeakReference;

public class play extends Activity implements B4AActivity{
	public static play mostCurrent;
	static boolean afterFirstLayout;
	static boolean isFirst = true;
    private static boolean processGlobalsRun = false;
	BALayout layout;
	public static BA processBA;
	BA activityBA;
    ActivityWrapper _activity;
    java.util.ArrayList<B4AMenuItem> menuItems;
	public static final boolean fullScreen = false;
	public static final boolean includeTitle = false;
    public static WeakReference<Activity> previousOne;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isFirst) {
			processBA = new BA(this.getApplicationContext(), null, null, "com.BUPC.YPSF", "com.BUPC.YPSF.play");
			processBA.loadHtSubs(this.getClass());
	        float deviceScale = getApplicationContext().getResources().getDisplayMetrics().density;
	        BALayout.setDeviceScale(deviceScale);
            
		}
		else if (previousOne != null) {
			Activity p = previousOne.get();
			if (p != null && p != this) {
                BA.LogInfo("Killing previous instance (play).");
				p.finish();
			}
		}
        processBA.runHook("oncreate", this, null);
		if (!includeTitle) {
        	this.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        }
        if (fullScreen) {
        	getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
        			android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
		mostCurrent = this;
        processBA.sharedProcessBA.activityBA = null;
		layout = new BALayout(this);
		setContentView(layout);
		afterFirstLayout = false;
		BA.handler.postDelayed(new WaitForLayout(), 5);

	}
	private static class WaitForLayout implements Runnable {
		public void run() {
			if (afterFirstLayout)
				return;
			if (mostCurrent == null)
				return;
            
			if (mostCurrent.layout.getWidth() == 0) {
				BA.handler.postDelayed(this, 5);
				return;
			}
			mostCurrent.layout.getLayoutParams().height = mostCurrent.layout.getHeight();
			mostCurrent.layout.getLayoutParams().width = mostCurrent.layout.getWidth();
			afterFirstLayout = true;
			mostCurrent.afterFirstLayout();
		}
	}
	private void afterFirstLayout() {
        if (this != mostCurrent)
			return;
		activityBA = new BA(this, layout, processBA, "com.BUPC.YPSF", "com.BUPC.YPSF.play");
        
        processBA.sharedProcessBA.activityBA = new java.lang.ref.WeakReference<BA>(activityBA);
        anywheresoftware.b4a.objects.ViewWrapper.lastId = 0;
        _activity = new ActivityWrapper(activityBA, "activity");
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (BA.isShellModeRuntimeCheck(processBA)) {
			if (isFirst)
				processBA.raiseEvent2(null, true, "SHELL", false);
			processBA.raiseEvent2(null, true, "CREATE", true, "com.BUPC.YPSF.play", processBA, activityBA, _activity, anywheresoftware.b4a.keywords.Common.Density, mostCurrent);
			_activity.reinitializeForShell(activityBA, "activity");
		}
        initializeProcessGlobals();		
        initializeGlobals();
        
        BA.LogInfo("** Activity (play) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, isFirst);
		isFirst = false;
		if (this != mostCurrent)
			return;
        processBA.setActivityPaused(false);
        BA.LogInfo("** Activity (play) Resume **");
        processBA.raiseEvent(null, "activity_resume");
        if (android.os.Build.VERSION.SDK_INT >= 11) {
			try {
				android.app.Activity.class.getMethod("invalidateOptionsMenu").invoke(this,(Object[]) null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
	public void addMenuItem(B4AMenuItem item) {
		if (menuItems == null)
			menuItems = new java.util.ArrayList<B4AMenuItem>();
		menuItems.add(item);
	}
	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		super.onCreateOptionsMenu(menu);
        try {
            if (processBA.subExists("activity_actionbarhomeclick")) {
                Class.forName("android.app.ActionBar").getMethod("setHomeButtonEnabled", boolean.class).invoke(
                    getClass().getMethod("getActionBar").invoke(this), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (processBA.runHook("oncreateoptionsmenu", this, new Object[] {menu}))
            return true;
		if (menuItems == null)
			return false;
		for (B4AMenuItem bmi : menuItems) {
			android.view.MenuItem mi = menu.add(bmi.title);
			if (bmi.drawable != null)
				mi.setIcon(bmi.drawable);
            if (android.os.Build.VERSION.SDK_INT >= 11) {
				try {
                    if (bmi.addToBar) {
				        android.view.MenuItem.class.getMethod("setShowAsAction", int.class).invoke(mi, 1);
                    }
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			mi.setOnMenuItemClickListener(new B4AMenuItemsClickListener(bmi.eventName.toLowerCase(BA.cul)));
		}
        
		return true;
	}   
 @Override
 public boolean onOptionsItemSelected(android.view.MenuItem item) {
    if (item.getItemId() == 16908332) {
        processBA.raiseEvent(null, "activity_actionbarhomeclick");
        return true;
    }
    else
        return super.onOptionsItemSelected(item); 
}
@Override
 public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    super.onPrepareOptionsMenu(menu);
    processBA.runHook("onprepareoptionsmenu", this, new Object[] {menu});
    return true;
    
 }
 protected void onStart() {
    super.onStart();
    processBA.runHook("onstart", this, null);
}
 protected void onStop() {
    super.onStop();
    processBA.runHook("onstop", this, null);
}
    public void onWindowFocusChanged(boolean hasFocus) {
       super.onWindowFocusChanged(hasFocus);
       if (processBA.subExists("activity_windowfocuschanged"))
           processBA.raiseEvent2(null, true, "activity_windowfocuschanged", false, hasFocus);
    }
	private class B4AMenuItemsClickListener implements android.view.MenuItem.OnMenuItemClickListener {
		private final String eventName;
		public B4AMenuItemsClickListener(String eventName) {
			this.eventName = eventName;
		}
		public boolean onMenuItemClick(android.view.MenuItem item) {
			processBA.raiseEvent(item.getTitle(), eventName + "_click");
			return true;
		}
	}
    public static Class<?> getObject() {
		return play.class;
	}
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;
	@Override
	public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
		if (onKeySubExist == null)
			onKeySubExist = processBA.subExists("activity_keypress");
		if (onKeySubExist) {
			if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK &&
					android.os.Build.VERSION.SDK_INT >= 18) {
				HandleKeyDelayed hk = new HandleKeyDelayed();
				hk.kc = keyCode;
				BA.handler.post(hk);
				return true;
			}
			else {
				boolean res = new HandleKeyDelayed().runDirectly(keyCode);
				if (res)
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	private class HandleKeyDelayed implements Runnable {
		int kc;
		public void run() {
			runDirectly(kc);
		}
		public boolean runDirectly(int keyCode) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keypress", false, keyCode);
			if (res == null || res == true) {
                return true;
            }
            else if (keyCode == anywheresoftware.b4a.keywords.constants.KeyCodes.KEYCODE_BACK) {
				finish();
				return true;
			}
            return false;
		}
		
	}
    @Override
	public boolean onKeyUp(int keyCode, android.view.KeyEvent event) {
		if (onKeyUpSubExist == null)
			onKeyUpSubExist = processBA.subExists("activity_keyup");
		if (onKeyUpSubExist) {
			Boolean res =  (Boolean)processBA.raiseEvent2(_activity, false, "activity_keyup", false, keyCode);
			if (res == null || res == true)
				return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	@Override
	public void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
		this.setIntent(intent);
        processBA.runHook("onnewintent", this, new Object[] {intent});
	}
    @Override 
	public void onPause() {
		super.onPause();
        if (_activity == null) //workaround for emulator bug (Issue 2423)
            return;
		anywheresoftware.b4a.Msgbox.dismiss(true);
        BA.LogInfo("** Activity (play) Pause, UserClosed = " + activityBA.activity.isFinishing() + " **");
        processBA.raiseEvent2(_activity, true, "activity_pause", false, activityBA.activity.isFinishing());		
        processBA.setActivityPaused(true);
        mostCurrent = null;
        if (!activityBA.activity.isFinishing())
			previousOne = new WeakReference<Activity>(this);
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        processBA.runHook("onpause", this, null);
	}

	@Override
	public void onDestroy() {
        super.onDestroy();
		previousOne = null;
        processBA.runHook("ondestroy", this, null);
	}
    @Override 
	public void onResume() {
		super.onResume();
        mostCurrent = this;
        anywheresoftware.b4a.Msgbox.isDismissing = false;
        if (activityBA != null) { //will be null during activity create (which waits for AfterLayout).
        	ResumeMessage rm = new ResumeMessage(mostCurrent);
        	BA.handler.post(rm);
        }
        processBA.runHook("onresume", this, null);
	}
    private static class ResumeMessage implements Runnable {
    	private final WeakReference<Activity> activity;
    	public ResumeMessage(Activity activity) {
    		this.activity = new WeakReference<Activity>(activity);
    	}
		public void run() {
			if (mostCurrent == null || mostCurrent != activity.get())
				return;
			processBA.setActivityPaused(false);
            BA.LogInfo("** Activity (play) Resume **");
		    processBA.raiseEvent(mostCurrent._activity, "activity_resume", (Object[])null);
		}
    }
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
	      android.content.Intent data) {
		processBA.onActivityResult(requestCode, resultCode, data);
        processBA.runHook("onactivityresult", this, new Object[] {requestCode, resultCode});
	}
	private static void initializeGlobals() {
		processBA.raiseEvent2(null, true, "globals", false, (Object[])null);
	}

public anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4a.sql.SQL _db = null;
public static anywheresoftware.b4a.objects.MediaPlayerWrapper _play = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnback = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnplay = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img1 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img2 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img3 = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _img4 = null;
public static String _pic1 = "";
public static String _pic2 = "";
public static String _pic3 = "";
public static String _pic4 = "";
public static String _sound = "";
public static String _ans = "";
public static String _trivia = "";
public static String _anpic = "";
public static String _animalname = "";
public anywheresoftware.b4a.objects.LabelWrapper _lbllevel = null;
public flm.b4a.animationplus.AnimationDrawable _animate = null;
public static String _thislevel = "";
public anywheresoftware.b4a.phone.Phone.PhoneWakeState _screen = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlpics = null;
public anywheresoftware.b4a.objects.LabelWrapper _lbltrivia = null;
public giuseppe.salvi.icos.library.ICOSSlideAnimation _anim2 = null;
public giuseppe.salvi.icos.library.ICOSFadeAnimation _anifade = null;
public anywheresoftware.b4a.phone.Phone _phone = null;
public static float _slideval = 0f;
public static float _durval = 0f;
public static float _fadedur = 0f;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcorpop = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcortrivia = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcortrivia = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcorname = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcorstat = null;
public anywheresoftware.b4a.objects.LabelWrapper _lblcongrat = null;
public anywheresoftware.b4a.objects.ImageViewWrapper _imgcoranimal = null;
public anywheresoftware.b4a.objects.ScrollViewWrapper _scrocortrivia = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnnext = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnsubmit = null;
public anywheresoftware.b4a.objects.EditTextWrapper _edtname = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlset = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlvol = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlcur = null;
public anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper _tglfull = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnreset = null;
public anywheresoftware.b4a.objects.ButtonWrapper _btnset = null;
public anywheresoftware.b4a.objects.PanelWrapper _pnlbg = null;
public static boolean _setclick = false;
public static int _finalscore = 0;
public anywheresoftware.b4a.objects.ButtonWrapper _btnclearhs = null;
public com.BUPC.YPSF.main _main = null;
public com.BUPC.YPSF.score _score = null;
public com.BUPC.YPSF.statemanager _statemanager = null;
public com.BUPC.YPSF.function _function = null;
public com.BUPC.YPSF.tutorial _tutorial = null;
public com.BUPC.YPSF.library _library = null;

public static void initializeProcessGlobals() {
             try {
                Class.forName(BA.applicationContext.getPackageName() + ".main").getMethod("initializeProcessGlobals").invoke(null, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
}
public static String  _activity_create(boolean _firsttime) throws Exception{
String _curid = "";
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd1 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd3 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd4 = null;
 //BA.debugLineNum = 54;BA.debugLine="Sub Activity_Create(FirstTime As Boolean)";
 //BA.debugLineNum = 55;BA.debugLine="Dim curID = StateManager.GetSetting2(\"currentid\",";
_curid = mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"currentid","0");
 //BA.debugLineNum = 56;BA.debugLine="Dim cursor As Cursor";
_cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 57;BA.debugLine="Dim bd1, bd2, bd3, bd4 As BitmapDrawable";
_bd1 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd3 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd4 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 59;BA.debugLine="If File.Exists(File.DirInternal,\"db\") == False Th";
if (anywheresoftware.b4a.keywords.Common.File.Exists(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db")==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 60;BA.debugLine="File.Copy(File.DirAssets, \"db.db\", File.DirInter";
anywheresoftware.b4a.keywords.Common.File.Copy(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"db.db",anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db");
 //BA.debugLineNum = 61;BA.debugLine="StateManager.SetSetting(\"currentid\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"currentid","0");
 //BA.debugLineNum = 62;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 };
 //BA.debugLineNum = 64;BA.debugLine="If FirstTime Then";
if (_firsttime) { 
 //BA.debugLineNum = 65;BA.debugLine="db.Initialize(File.DirInternal, \"db\",False)";
_db.Initialize(anywheresoftware.b4a.keywords.Common.File.getDirInternal(),"db",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 66;BA.debugLine="play.Initialize2(\"play\")";
_play.Initialize2(processBA,"play");
 };
 //BA.debugLineNum = 68;BA.debugLine="If count(0) == 0 Then";
if (_count((int) (0))==0) { 
 //BA.debugLineNum = 69;BA.debugLine="reset";
_reset();
 };
 //BA.debugLineNum = 71;BA.debugLine="screen.KeepAlive(True)";
mostCurrent._screen.KeepAlive(processBA,anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 72;BA.debugLine="screen.PartialLock";
mostCurrent._screen.PartialLock(processBA);
 //BA.debugLineNum = 74;BA.debugLine="pnlCorPop.Initialize(\"pnlCorPop\")";
mostCurrent._pnlcorpop.Initialize(mostCurrent.activityBA,"pnlCorPop");
 //BA.debugLineNum = 75;BA.debugLine="pnlCorTrivia.Initialize(\"\")";
mostCurrent._pnlcortrivia.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 76;BA.debugLine="lblCorStat.Initialize(\"\")";
mostCurrent._lblcorstat.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 77;BA.debugLine="lblCorName.Initialize(\"\")";
mostCurrent._lblcorname.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 78;BA.debugLine="lblCorTrivia.Initialize(\"\")";
mostCurrent._lblcortrivia.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 79;BA.debugLine="imgCorAnimal.Initialize(\"\")";
mostCurrent._imgcoranimal.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 80;BA.debugLine="scroCorTrivia.Initialize(1000dip)";
mostCurrent._scrocortrivia.Initialize(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (1000)));
 //BA.debugLineNum = 81;BA.debugLine="btnNext.Initialize(\"btnNext\")";
mostCurrent._btnnext.Initialize(mostCurrent.activityBA,"btnNext");
 //BA.debugLineNum = 82;BA.debugLine="edtName.Initialize(\"\")";
mostCurrent._edtname.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 83;BA.debugLine="btnSubmit.Initialize(\"btnSubmit\")";
mostCurrent._btnsubmit.Initialize(mostCurrent.activityBA,"btnSubmit");
 //BA.debugLineNum = 84;BA.debugLine="lblCongrat.Initialize(\"\")";
mostCurrent._lblcongrat.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 85;BA.debugLine="pnlBg.Initialize(\"pnlBg\")";
mostCurrent._pnlbg.Initialize(mostCurrent.activityBA,"pnlBg");
 //BA.debugLineNum = 87;BA.debugLine="Activity.LoadLayout(\"2\")";
mostCurrent._activity.LoadLayout("2",mostCurrent.activityBA);
 //BA.debugLineNum = 89;BA.debugLine="lblTrivia.Initialize(\"\")";
mostCurrent._lbltrivia.Initialize(mostCurrent.activityBA,"");
 //BA.debugLineNum = 90;BA.debugLine="If curID == \"0\" Then";
if ((_curid).equals("0")) { 
 //BA.debugLineNum = 91;BA.debugLine="new";
_new();
 }else {
 //BA.debugLineNum = 93;BA.debugLine="cursor = db.ExecQuery(\"SELECT * FROM game WHERE";
_cursor.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM game WHERE id='"+_curid+"'")));
 //BA.debugLineNum = 94;BA.debugLine="cursor.Position = 0";
_cursor.setPosition((int) (0));
 //BA.debugLineNum = 95;BA.debugLine="thislevel = cursor.GetString(\"id\")";
mostCurrent._thislevel = _cursor.GetString("id");
 //BA.debugLineNum = 96;BA.debugLine="pic1 = cursor.GetString(\"1\")";
mostCurrent._pic1 = _cursor.GetString("1");
 //BA.debugLineNum = 97;BA.debugLine="pic2 = cursor.GetString(\"2\")";
mostCurrent._pic2 = _cursor.GetString("2");
 //BA.debugLineNum = 98;BA.debugLine="pic3 = cursor.GetString(\"3\")";
mostCurrent._pic3 = _cursor.GetString("3");
 //BA.debugLineNum = 99;BA.debugLine="pic4 = cursor.GetString(\"4\")";
mostCurrent._pic4 = _cursor.GetString("4");
 //BA.debugLineNum = 100;BA.debugLine="sound = cursor.GetString(\"sound\")";
mostCurrent._sound = _cursor.GetString("sound");
 //BA.debugLineNum = 101;BA.debugLine="ans = cursor.GetString(\"ans\")";
mostCurrent._ans = _cursor.GetString("ans");
 //BA.debugLineNum = 102;BA.debugLine="anPic = cursor.GetString(ans)";
mostCurrent._anpic = _cursor.GetString(mostCurrent._ans);
 //BA.debugLineNum = 103;BA.debugLine="trivia = cursor.GetString(\"trivia\")";
mostCurrent._trivia = _cursor.GetString("trivia");
 //BA.debugLineNum = 104;BA.debugLine="animalName = cursor.GetString(\"name\")";
mostCurrent._animalname = _cursor.GetString("name");
 //BA.debugLineNum = 106;BA.debugLine="cursor.Close";
_cursor.Close();
 //BA.debugLineNum = 108;BA.debugLine="play.Load(File.DirAssets, function.getSndExt(sou";
_play.Load(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getsndext(mostCurrent.activityBA,mostCurrent._sound));
 //BA.debugLineNum = 110;BA.debugLine="bd1.Initialize(LoadBitmapSample(File.DirAssets,";
_bd1.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic1),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 111;BA.debugLine="bd2.Initialize(LoadBitmapSample(File.DirAssets,";
_bd2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic2),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 112;BA.debugLine="bd3.Initialize(LoadBitmapSample(File.DirAssets,";
_bd3.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic3),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 113;BA.debugLine="bd4.Initialize(LoadBitmapSample(File.DirAssets,";
_bd4.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic4),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 115;BA.debugLine="img1.Background = bd1";
mostCurrent._img1.setBackground((android.graphics.drawable.Drawable)(_bd1.getObject()));
 //BA.debugLineNum = 116;BA.debugLine="img2.Background = bd2";
mostCurrent._img2.setBackground((android.graphics.drawable.Drawable)(_bd2.getObject()));
 //BA.debugLineNum = 117;BA.debugLine="img3.Background = bd3";
mostCurrent._img3.setBackground((android.graphics.drawable.Drawable)(_bd3.getObject()));
 //BA.debugLineNum = 118;BA.debugLine="img4.Background = bd4";
mostCurrent._img4.setBackground((android.graphics.drawable.Drawable)(_bd4.getObject()));
 //BA.debugLineNum = 120;BA.debugLine="aniFade.FadeIn(\"\", fadedur)";
mostCurrent._anifade.FadeIn(mostCurrent.activityBA,"",(long) (_fadedur));
 //BA.debugLineNum = 121;BA.debugLine="aniFade.StartAnim(pnlPics)";
mostCurrent._anifade.StartAnim((android.view.View)(mostCurrent._pnlpics.getObject()));
 };
 //BA.debugLineNum = 123;BA.debugLine="End Sub";
return "";
}
public static boolean  _activity_keypress(int _keycode) throws Exception{
 //BA.debugLineNum = 507;BA.debugLine="Sub Activity_KeyPress (KeyCode As Int) As Boolean";
 //BA.debugLineNum = 508;BA.debugLine="If KeyCode = KeyCodes.KEYCODE_BACK Then";
if (_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_BACK) { 
 //BA.debugLineNum = 509;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 }else if(_keycode==anywheresoftware.b4a.keywords.Common.KeyCodes.KEYCODE_MENU && mostCurrent._btnset.getEnabled()==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 511;BA.debugLine="btnSet_Click";
_btnset_click();
 };
 //BA.debugLineNum = 513;BA.debugLine="End Sub";
return false;
}
public static String  _activity_pause(boolean _userclosed) throws Exception{
 //BA.debugLineNum = 140;BA.debugLine="Sub Activity_Pause (UserClosed As Boolean)";
 //BA.debugLineNum = 141;BA.debugLine="If play.IsPlaying Then";
if (_play.IsPlaying()) { 
 //BA.debugLineNum = 142;BA.debugLine="play.Stop";
_play.Stop();
 };
 //BA.debugLineNum = 144;BA.debugLine="screen.ReleaseKeepAlive";
mostCurrent._screen.ReleaseKeepAlive();
 //BA.debugLineNum = 145;BA.debugLine="screen.ReleasePartialLock";
mostCurrent._screen.ReleasePartialLock();
 //BA.debugLineNum = 146;BA.debugLine="End Sub";
return "";
}
public static String  _activity_resume() throws Exception{
int _fstat = 0;
int _maxvol = 0;
int _vol = 0;
 //BA.debugLineNum = 125;BA.debugLine="Sub Activity_Resume";
 //BA.debugLineNum = 126;BA.debugLine="Dim fstat = StateManager.GetSetting2(\"fullscreen\"";
_fstat = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
_maxvol = 0;
 //BA.debugLineNum = 127;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 128;BA.debugLine="If fstat == 1 Then";
if (_fstat==1) { 
 //BA.debugLineNum = 129;BA.debugLine="function.FullScreen(True, \"Play\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.True,"Play");
 }else {
 //BA.debugLineNum = 131;BA.debugLine="function.FullScreen(False, \"Play\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,anywheresoftware.b4a.keywords.Common.False,"Play");
 };
 //BA.debugLineNum = 133;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 134;BA.debugLine="lblLevel.Text = count(1)";
mostCurrent._lbllevel.setText((Object)(_count((int) (1))));
 //BA.debugLineNum = 135;BA.debugLine="animate.Initialize";
mostCurrent._animate.Initialize();
 //BA.debugLineNum = 136;BA.debugLine="play.SetVolume(vol/10, vol/10)";
_play.SetVolume((float) (_vol/(double)10),(float) (_vol/(double)10));
 //BA.debugLineNum = 138;BA.debugLine="End Sub";
return "";
}
public static String  _anim2_animationend() throws Exception{
 //BA.debugLineNum = 482;BA.debugLine="Sub anim2_animationend";
 //BA.debugLineNum = 483;BA.debugLine="pnlCorPop.RemoveView";
mostCurrent._pnlcorpop.RemoveView();
 //BA.debugLineNum = 484;BA.debugLine="pnlCorTrivia.RemoveView";
mostCurrent._pnlcortrivia.RemoveView();
 //BA.debugLineNum = 485;BA.debugLine="lblCorName.RemoveView";
mostCurrent._lblcorname.RemoveView();
 //BA.debugLineNum = 486;BA.debugLine="lblCorTrivia.RemoveView";
mostCurrent._lblcortrivia.RemoveView();
 //BA.debugLineNum = 487;BA.debugLine="imgCorAnimal.RemoveView";
mostCurrent._imgcoranimal.RemoveView();
 //BA.debugLineNum = 488;BA.debugLine="lblCorStat.RemoveView";
mostCurrent._lblcorstat.RemoveView();
 //BA.debugLineNum = 489;BA.debugLine="scroCorTrivia.RemoveView";
mostCurrent._scrocortrivia.RemoveView();
 //BA.debugLineNum = 490;BA.debugLine="btnNext.RemoveView";
mostCurrent._btnnext.RemoveView();
 //BA.debugLineNum = 491;BA.debugLine="aniFade.FadeOut(\"fade\", fadedur)";
mostCurrent._anifade.FadeOut(mostCurrent.activityBA,"fade",(long) (_fadedur));
 //BA.debugLineNum = 492;BA.debugLine="aniFade.StartAnim(pnlPics)";
mostCurrent._anifade.StartAnim((android.view.View)(mostCurrent._pnlpics.getObject()));
 //BA.debugLineNum = 493;BA.debugLine="End Sub";
return "";
}
public static String  _anim4_animationend() throws Exception{
 //BA.debugLineNum = 613;BA.debugLine="Sub anim4_animationend";
 //BA.debugLineNum = 614;BA.debugLine="pnlBg.RemoveAllViews";
mostCurrent._pnlbg.RemoveAllViews();
 //BA.debugLineNum = 615;BA.debugLine="pnlBg.RemoveView";
mostCurrent._pnlbg.RemoveView();
 //BA.debugLineNum = 616;BA.debugLine="btnSet.Enabled = True";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 617;BA.debugLine="End Sub";
return "";
}
public static String  _btnback_click() throws Exception{
 //BA.debugLineNum = 399;BA.debugLine="Sub btnBack_Click";
 //BA.debugLineNum = 400;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 401;BA.debugLine="StartActivity(Main)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._main.getObject()));
 //BA.debugLineNum = 402;BA.debugLine="function.SetAnimation(\"animtoright\", \"animfromlef";
mostCurrent._function._setanimation(mostCurrent.activityBA,"animtoright","animfromleft");
 //BA.debugLineNum = 403;BA.debugLine="End Sub";
return "";
}
public static String  _btnclearhs_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 619;BA.debugLine="Sub btnClearHS_Click";
 //BA.debugLineNum = 620;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 621;BA.debugLine="res = Msgbox2(\"Do you really want to clear the hi";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want to clear the highscore?","Reset","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 622;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 623;BA.debugLine="db.ExecNonQuery(\"DELETE FROM hscore\")";
_db.ExecNonQuery("DELETE FROM hscore");
 //BA.debugLineNum = 624;BA.debugLine="StateManager.SetSetting(\"pnum\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"pnum","0");
 //BA.debugLineNum = 625;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 626;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 }else {
 //BA.debugLineNum = 628;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 630;BA.debugLine="End Sub";
return "";
}
public static String  _btnevent_click() throws Exception{
anywheresoftware.b4a.objects.StringUtils _su = null;
float _fheight = 0f;
anywheresoftware.b4a.objects.ButtonWrapper _btnsender = null;
String _name = "";
anywheresoftware.b4a.objects.drawable.ColorDrawable _border = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _enabled3 = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _pressed3 = null;
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld = null;
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed2 = null;
 //BA.debugLineNum = 201;BA.debugLine="Sub btnEvent_Click";
 //BA.debugLineNum = 202;BA.debugLine="Dim su As StringUtils";
_su = new anywheresoftware.b4a.objects.StringUtils();
 //BA.debugLineNum = 203;BA.debugLine="Dim fheight As Float";
_fheight = 0f;
 //BA.debugLineNum = 204;BA.debugLine="Private btnSender As Button";
_btnsender = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 205;BA.debugLine="Dim name As String";
_name = "";
 //BA.debugLineNum = 206;BA.debugLine="Dim border, enabled3, pressed3 As ColorDrawable";
_border = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
_enabled3 = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
_pressed3 = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 207;BA.debugLine="Dim sld, sld2 As StateListDrawable";
_sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
_sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 208;BA.debugLine="Dim enabled, pressed, enabled2, pressed2 As Bitma";
_enabled = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_enabled2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 211;BA.debugLine="border.Initialize2(0xFF28507B, 0, 4dip, Colors.Ye";
_border.Initialize2((int) (0xff28507b),(int) (0),anywheresoftware.b4a.keywords.Common.DipToCurrent((int) (4)),anywheresoftware.b4a.keywords.Common.Colors.Yellow);
 //BA.debugLineNum = 212;BA.debugLine="Try";
try { //BA.debugLineNum = 214;BA.debugLine="btnSender = Sender";
_btnsender.setObject((android.widget.Button)(anywheresoftware.b4a.keywords.Common.Sender(mostCurrent.activityBA)));
 //BA.debugLineNum = 215;BA.debugLine="Select btnSender.Tag";
switch (BA.switchObjectToInt(_btnsender.getTag(),(Object)(mostCurrent._ans))) {
case 0:
 //BA.debugLineNum = 220;BA.debugLine="If play.IsPlaying Then";
if (_play.IsPlaying()) { 
 //BA.debugLineNum = 221;BA.debugLine="play.Stop";
_play.Stop();
 };
 //BA.debugLineNum = 224;BA.debugLine="play.Load(File.DirAssets, \"correct-sound-effect.";
_play.Load(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"correct-sound-effect.mp3");
 //BA.debugLineNum = 225;BA.debugLine="play.Play";
_play.Play();
 //BA.debugLineNum = 229;BA.debugLine="enabled.Initialize(LoadBitmapSample(File.DirAsse";
_enabled.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"nextlevel_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 230;BA.debugLine="pressed.Initialize(LoadBitmapSample(File.DirAsse";
_pressed.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"nextlevel_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 231;BA.debugLine="sld.Initialize";
_sld.Initialize();
 //BA.debugLineNum = 232;BA.debugLine="sld.AddState(sld.State_Pressed, pressed)";
_sld.AddState(_sld.State_Pressed,(android.graphics.drawable.Drawable)(_pressed.getObject()));
 //BA.debugLineNum = 233;BA.debugLine="sld.AddState(sld.State_Enabled, enabled)";
_sld.AddState(_sld.State_Enabled,(android.graphics.drawable.Drawable)(_enabled.getObject()));
 //BA.debugLineNum = 237;BA.debugLine="Activity.AddView(pnlCorPop, 0, 9%x, 100%x, 100%y";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlcorpop.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 238;BA.debugLine="pnlCorPop.Color = 0x64000000";
mostCurrent._pnlcorpop.setColor((int) (0x64000000));
 //BA.debugLineNum = 240;BA.debugLine="pnlCorPop.AddView(pnlCorTrivia, 2%x, 25%x, 96%x,";
mostCurrent._pnlcorpop.AddView((android.view.View)(mostCurrent._pnlcortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (25),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA));
 //BA.debugLineNum = 241;BA.debugLine="pnlCorTrivia.Background = border";
mostCurrent._pnlcortrivia.setBackground((android.graphics.drawable.Drawable)(_border.getObject()));
 //BA.debugLineNum = 243;BA.debugLine="pnlCorTrivia.AddView(lblCorStat, 60%x, 2%x, 35%x";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._lblcorstat.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (60),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (35),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (4),mostCurrent.activityBA));
 //BA.debugLineNum = 244;BA.debugLine="lblCorStat.Gravity = Gravity.RIGHT";
mostCurrent._lblcorstat.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.RIGHT);
 //BA.debugLineNum = 245;BA.debugLine="lblCorStat.TextSize = (10%x/100)*20";
mostCurrent._lblcorstat.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*20));
 //BA.debugLineNum = 246;BA.debugLine="lblCorStat.TextColor = Colors.White";
mostCurrent._lblcorstat.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 247;BA.debugLine="lblCorStat.Text = checkLib(thislevel)";
mostCurrent._lblcorstat.setText((Object)(_checklib(mostCurrent._thislevel)));
 //BA.debugLineNum = 249;BA.debugLine="pnlCorTrivia.AddView(lblCorName, 28%x, 6%x, 40%x";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._lblcorname.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (28),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (6),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (7),mostCurrent.activityBA));
 //BA.debugLineNum = 250;BA.debugLine="lblCorName.Text = animalName";
mostCurrent._lblcorname.setText((Object)(mostCurrent._animalname));
 //BA.debugLineNum = 251;BA.debugLine="lblCorName.TextColor = Colors.White";
mostCurrent._lblcorname.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 252;BA.debugLine="lblCorName.Gravity = Gravity.CENTER_HORIZONTAL";
mostCurrent._lblcorname.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 253;BA.debugLine="lblCorName.TextSize = (10%x/100)*35";
mostCurrent._lblcorname.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*35));
 //BA.debugLineNum = 255;BA.debugLine="pnlCorTrivia.AddView(scroCorTrivia, 2%x, 13%x, 9";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._scrocortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (70),mostCurrent.activityBA));
 //BA.debugLineNum = 256;BA.debugLine="scroCorTrivia.Panel.AddView(imgCorAnimal, 22%x,";
mostCurrent._scrocortrivia.getPanel().AddView((android.view.View)(mostCurrent._imgcoranimal.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (22),mostCurrent.activityBA),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA));
 //BA.debugLineNum = 257;BA.debugLine="imgCorAnimal.Bitmap = LoadBitmapSample(File.DirA";
mostCurrent._imgcoranimal.setBitmap((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._anpic),mostCurrent._imgcoranimal.getWidth(),mostCurrent._imgcoranimal.getHeight()).getObject()));
 //BA.debugLineNum = 258;BA.debugLine="imgCorAnimal.Gravity = Gravity.FILL";
mostCurrent._imgcoranimal.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.FILL);
 //BA.debugLineNum = 259;BA.debugLine="scroCorTrivia.Panel.AddView(lblCorTrivia, 2%x, 5";
mostCurrent._scrocortrivia.getPanel().AddView((android.view.View)(mostCurrent._lblcortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (90),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (90),mostCurrent.activityBA));
 //BA.debugLineNum = 260;BA.debugLine="lblCorTrivia.Text = trivia";
mostCurrent._lblcortrivia.setText((Object)(mostCurrent._trivia));
 //BA.debugLineNum = 261;BA.debugLine="lblCorTrivia.Gravity = Gravity.CENTER_HORIZONTAL";
mostCurrent._lblcortrivia.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER_HORIZONTAL);
 //BA.debugLineNum = 262;BA.debugLine="lblCorTrivia.TextSize = (9%x/100)*40";
mostCurrent._lblcortrivia.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)/(double)100)*40));
 //BA.debugLineNum = 263;BA.debugLine="scroCorTrivia.Panel.Height = 150%x";
mostCurrent._scrocortrivia.getPanel().setHeight(anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (150),mostCurrent.activityBA));
 //BA.debugLineNum = 264;BA.debugLine="scroCorTrivia.ScrollPosition = 0";
mostCurrent._scrocortrivia.setScrollPosition((int) (0));
 //BA.debugLineNum = 266;BA.debugLine="pnlCorTrivia.AddView(btnNext,23%x, 84%x, 50%x, 1";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._btnnext.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (23),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (84),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (50),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 267;BA.debugLine="btnNext.Background = sld";
mostCurrent._btnnext.setBackground((android.graphics.drawable.Drawable)(_sld.getObject()));
 //BA.debugLineNum = 273;BA.debugLine="fheight = su.MeasureMultilineTextHeight(lblCorTr";
_fheight = (float) (_su.MeasureMultilineTextHeight((android.widget.TextView)(mostCurrent._lblcortrivia.getObject()),mostCurrent._lblcortrivia.getText()));
 //BA.debugLineNum = 274;BA.debugLine="lblCorTrivia.Height = fheight";
mostCurrent._lblcortrivia.setHeight((int) (_fheight));
 //BA.debugLineNum = 275;BA.debugLine="scroCorTrivia.Panel.Height = imgCorAnimal.Height";
mostCurrent._scrocortrivia.getPanel().setHeight((int) (mostCurrent._imgcoranimal.getHeight()+_fheight));
 //BA.debugLineNum = 278;BA.debugLine="db.ExecNonQuery(\"UPDATE game SET stat='1', lib='";
_db.ExecNonQuery("UPDATE game SET stat='1', lib='1' WHERE id='"+mostCurrent._thislevel+"'");
 //BA.debugLineNum = 280;BA.debugLine="lblLevel.Text = count(1)";
mostCurrent._lbllevel.setText((Object)(_count((int) (1))));
 //BA.debugLineNum = 282;BA.debugLine="animate.Stop";
mostCurrent._animate.Stop();
 //BA.debugLineNum = 283;BA.debugLine="btnPlay.SetBackgroundImage(LoadBitmapSample(File";
mostCurrent._btnplay.SetBackgroundImage((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()));
 //BA.debugLineNum = 286;BA.debugLine="If count(0) == 0 Then";
if (_count((int) (0))==0) { 
 //BA.debugLineNum = 288;BA.debugLine="enabled3.Initialize(0xFF1367B2, 0)";
_enabled3.Initialize((int) (0xff1367b2),(int) (0));
 //BA.debugLineNum = 289;BA.debugLine="pressed3.Initialize(0xFF0A1015, 0)";
_pressed3.Initialize((int) (0xff0a1015),(int) (0));
 //BA.debugLineNum = 290;BA.debugLine="sld2.Initialize";
_sld2.Initialize();
 //BA.debugLineNum = 291;BA.debugLine="sld2.AddState(sld2.State_Pressed, pressed3)";
_sld2.AddState(_sld2.State_Pressed,(android.graphics.drawable.Drawable)(_pressed3.getObject()));
 //BA.debugLineNum = 292;BA.debugLine="sld2.AddState(sld2.State_Enabled, enabled3)";
_sld2.AddState(_sld2.State_Enabled,(android.graphics.drawable.Drawable)(_enabled3.getObject()));
 //BA.debugLineNum = 293;BA.debugLine="btnNext.RemoveView";
mostCurrent._btnnext.RemoveView();
 //BA.debugLineNum = 295;BA.debugLine="pnlCorTrivia.AddView(lblCongrat, 2%x, 2%x, 60%x";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._lblcongrat.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (60),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (4),mostCurrent.activityBA));
 //BA.debugLineNum = 296;BA.debugLine="lblCongrat.Text = \"Congratulations! You complet";
mostCurrent._lblcongrat.setText((Object)("Congratulations! You completed all level!"));
 //BA.debugLineNum = 297;BA.debugLine="lblCongrat.TextSize = (10%x/100)*17";
mostCurrent._lblcongrat.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*17));
 //BA.debugLineNum = 298;BA.debugLine="lblCongrat.TextColor = Colors.White";
mostCurrent._lblcongrat.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 300;BA.debugLine="pnlCorTrivia.AddView(edtName, 2%x, 84%x, 60%x,";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._edtname.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (84),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (60),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 301;BA.debugLine="edtName.Color = 0xFF233451";
mostCurrent._edtname.setColor((int) (0xff233451));
 //BA.debugLineNum = 302;BA.debugLine="edtName.Text = \"Player\" & playerNum";
mostCurrent._edtname.setText((Object)("Player"+BA.NumberToString(_playernum())));
 //BA.debugLineNum = 303;BA.debugLine="edtName.TextSize = (10%x/100)*30";
mostCurrent._edtname.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*30));
 //BA.debugLineNum = 304;BA.debugLine="edtName.SingleLine = True";
mostCurrent._edtname.setSingleLine(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 305;BA.debugLine="edtName.RequestFocus";
mostCurrent._edtname.RequestFocus();
 //BA.debugLineNum = 307;BA.debugLine="pnlCorTrivia.AddView(btnSubmit, 62%x, 84%x, 32%";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._btnsubmit.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (62),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (84),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (32),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 308;BA.debugLine="btnSubmit.Background = sld2";
mostCurrent._btnsubmit.setBackground((android.graphics.drawable.Drawable)(_sld2.getObject()));
 //BA.debugLineNum = 309;BA.debugLine="btnSubmit.Text = \"SUBMIT\"";
mostCurrent._btnsubmit.setText((Object)("SUBMIT"));
 //BA.debugLineNum = 310;BA.debugLine="btnSubmit.Typeface = Typeface.DEFAULT_BOLD";
mostCurrent._btnsubmit.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.DEFAULT_BOLD);
 //BA.debugLineNum = 311;BA.debugLine="btnSubmit.TextSize = (10%x/100)*30";
mostCurrent._btnsubmit.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*30));
 //BA.debugLineNum = 312;BA.debugLine="btnSubmit.TextColor = Colors.White";
mostCurrent._btnsubmit.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 313;BA.debugLine="btnSubmit.Gravity = Gravity.CENTER";
mostCurrent._btnsubmit.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 315;BA.debugLine="finalscore = count(1)";
_finalscore = _count((int) (1));
 //BA.debugLineNum = 316;BA.debugLine="reset";
_reset();
 };
 //BA.debugLineNum = 319;BA.debugLine="anim2.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anim2.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 320;BA.debugLine="anim2.StartAnim(pnlCorTrivia)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlcortrivia.getObject()));
 break;
default:
 //BA.debugLineNum = 325;BA.debugLine="If play.IsPlaying Then";
if (_play.IsPlaying()) { 
 //BA.debugLineNum = 326;BA.debugLine="play.Stop";
_play.Stop();
 };
 //BA.debugLineNum = 329;BA.debugLine="play.Load(File.DirAssets, \"fail-sound-effect.mp3";
_play.Load(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"fail-sound-effect.mp3");
 //BA.debugLineNum = 330;BA.debugLine="play.Play";
_play.Play();
 //BA.debugLineNum = 332;BA.debugLine="enabled3.Initialize(0xFF1367B2, 0)";
_enabled3.Initialize((int) (0xff1367b2),(int) (0));
 //BA.debugLineNum = 333;BA.debugLine="pressed3.Initialize(0xFF0A1015, 0)";
_pressed3.Initialize((int) (0xff0a1015),(int) (0));
 //BA.debugLineNum = 334;BA.debugLine="sld2.Initialize";
_sld2.Initialize();
 //BA.debugLineNum = 335;BA.debugLine="sld2.AddState(sld2.State_Pressed, pressed3)";
_sld2.AddState(_sld2.State_Pressed,(android.graphics.drawable.Drawable)(_pressed3.getObject()));
 //BA.debugLineNum = 336;BA.debugLine="sld2.AddState(sld2.State_Enabled, enabled3)";
_sld2.AddState(_sld2.State_Enabled,(android.graphics.drawable.Drawable)(_enabled3.getObject()));
 //BA.debugLineNum = 338;BA.debugLine="Activity.AddView(pnlCorPop, 0, 9%x, 100%x, 100%y";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlcorpop.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 339;BA.debugLine="pnlCorPop.Color = 0x64000000";
mostCurrent._pnlcorpop.setColor((int) (0x64000000));
 //BA.debugLineNum = 341;BA.debugLine="pnlCorPop.AddView(pnlCorTrivia, 2%x, 40%x, 96%x,";
mostCurrent._pnlcorpop.AddView((android.view.View)(mostCurrent._pnlcortrivia.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (96),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (23),mostCurrent.activityBA));
 //BA.debugLineNum = 342;BA.debugLine="pnlCorTrivia.Background = border";
mostCurrent._pnlcortrivia.setBackground((android.graphics.drawable.Drawable)(_border.getObject()));
 //BA.debugLineNum = 344;BA.debugLine="pnlCorTrivia.AddView(lblCongrat,2%x, 2%x, 92%x,";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._lblcongrat.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (92),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (7),mostCurrent.activityBA));
 //BA.debugLineNum = 345;BA.debugLine="lblCongrat.Text = \"Sorry, You Lose.\"";
mostCurrent._lblcongrat.setText((Object)("Sorry, You Lose."));
 //BA.debugLineNum = 346;BA.debugLine="lblCongrat.TextColor = Colors.White";
mostCurrent._lblcongrat.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 347;BA.debugLine="lblCongrat.TextSize = (lblCongrat.Height/100)*50";
mostCurrent._lblcongrat.setTextSize((float) ((mostCurrent._lblcongrat.getHeight()/(double)100)*50));
 //BA.debugLineNum = 348;BA.debugLine="lblCongrat.Gravity = Gravity.CENTER";
mostCurrent._lblcongrat.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 350;BA.debugLine="pnlCorTrivia.AddView(edtName, 2%x, 11%x, 60%x, 1";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._edtname.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (2),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (11),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (60),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 351;BA.debugLine="edtName.Color = 0xFF233451";
mostCurrent._edtname.setColor((int) (0xff233451));
 //BA.debugLineNum = 352;BA.debugLine="edtName.Text = \"Player\" & playerNum";
mostCurrent._edtname.setText((Object)("Player"+BA.NumberToString(_playernum())));
 //BA.debugLineNum = 353;BA.debugLine="edtName.TextSize = (10%x/100)*30";
mostCurrent._edtname.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*30));
 //BA.debugLineNum = 354;BA.debugLine="edtName.SingleLine = True";
mostCurrent._edtname.setSingleLine(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 355;BA.debugLine="edtName.RequestFocus";
mostCurrent._edtname.RequestFocus();
 //BA.debugLineNum = 357;BA.debugLine="pnlCorTrivia.AddView(btnSubmit, 62%x, 11%x, 32%x";
mostCurrent._pnlcortrivia.AddView((android.view.View)(mostCurrent._btnsubmit.getObject()),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (62),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (11),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (32),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA));
 //BA.debugLineNum = 358;BA.debugLine="btnSubmit.Background = sld2";
mostCurrent._btnsubmit.setBackground((android.graphics.drawable.Drawable)(_sld2.getObject()));
 //BA.debugLineNum = 359;BA.debugLine="btnSubmit.Text = \"SUBMIT\"";
mostCurrent._btnsubmit.setText((Object)("SUBMIT"));
 //BA.debugLineNum = 360;BA.debugLine="btnSubmit.Typeface = Typeface.DEFAULT_BOLD";
mostCurrent._btnsubmit.setTypeface(anywheresoftware.b4a.keywords.Common.Typeface.DEFAULT_BOLD);
 //BA.debugLineNum = 361;BA.debugLine="btnSubmit.TextSize = (10%x/100)*30";
mostCurrent._btnsubmit.setTextSize((float) ((anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (10),mostCurrent.activityBA)/(double)100)*30));
 //BA.debugLineNum = 362;BA.debugLine="btnSubmit.TextColor = Colors.White";
mostCurrent._btnsubmit.setTextColor(anywheresoftware.b4a.keywords.Common.Colors.White);
 //BA.debugLineNum = 363;BA.debugLine="btnSubmit.Gravity = Gravity.CENTER";
mostCurrent._btnsubmit.setGravity(anywheresoftware.b4a.keywords.Common.Gravity.CENTER);
 //BA.debugLineNum = 365;BA.debugLine="anim2.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anim2.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 366;BA.debugLine="anim2.StartAnim(pnlCorTrivia)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlcortrivia.getObject()));
 //BA.debugLineNum = 368;BA.debugLine="animate.Stop";
mostCurrent._animate.Stop();
 //BA.debugLineNum = 369;BA.debugLine="btnPlay.SetBackgroundImage(LoadBitmapSample(File";
mostCurrent._btnplay.SetBackgroundImage((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()));
 //BA.debugLineNum = 371;BA.debugLine="finalscore = count(1)";
_finalscore = _count((int) (1));
 //BA.debugLineNum = 372;BA.debugLine="reset";
_reset();
 break;
}
;
 } 
       catch (Exception e285) {
			processBA.setLastException(e285); };
 //BA.debugLineNum = 378;BA.debugLine="End Sub";
return "";
}
public static String  _btnnext_click() throws Exception{
 //BA.debugLineNum = 477;BA.debugLine="Sub btnNext_Click";
 //BA.debugLineNum = 478;BA.debugLine="anim2.SlideToTop(\"anim2\", slideval, durval)";
mostCurrent._anim2.SlideToTop(mostCurrent.activityBA,"anim2",_slideval,(long) (_durval));
 //BA.debugLineNum = 479;BA.debugLine="anim2.StartAnim(pnlCorTrivia)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlcortrivia.getObject()));
 //BA.debugLineNum = 480;BA.debugLine="End Sub";
return "";
}
public static String  _btnplay_click() throws Exception{
float _vol = 0f;
 //BA.debugLineNum = 380;BA.debugLine="Sub btnPlay_Click";
 //BA.debugLineNum = 381;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (float)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 383;BA.debugLine="animate.AddFrame(LoadBitmapSample(File.DirAssets,";
mostCurrent._animate.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_0.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 384;BA.debugLine="animate.AddFrame(LoadBitmapSample(File.DirAssets,";
mostCurrent._animate.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_1.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 385;BA.debugLine="animate.AddFrame(LoadBitmapSample(File.DirAssets,";
mostCurrent._animate.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_2.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 386;BA.debugLine="animate.AddFrame(LoadBitmapSample(File.DirAssets,";
mostCurrent._animate.AddFrame((Object)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()),(int) (150));
 //BA.debugLineNum = 387;BA.debugLine="animate.OneShot = False";
mostCurrent._animate.setOneShot(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 388;BA.debugLine="btnPlay.Background = animate";
mostCurrent._btnplay.setBackground((android.graphics.drawable.Drawable)(mostCurrent._animate.getObject()));
 //BA.debugLineNum = 390;BA.debugLine="play.SetVolume(vol/10, vol/10)";
_play.SetVolume((float) (_vol/(double)10),(float) (_vol/(double)10));
 //BA.debugLineNum = 392;BA.debugLine="play.Play";
_play.Play();
 //BA.debugLineNum = 394;BA.debugLine="animate.Start";
mostCurrent._animate.Start();
 //BA.debugLineNum = 397;BA.debugLine="End Sub";
return "";
}
public static String  _btnreset_click() throws Exception{
int _res = 0;
 //BA.debugLineNum = 600;BA.debugLine="Sub btnReset_Click";
 //BA.debugLineNum = 601;BA.debugLine="Dim res As Int";
_res = 0;
 //BA.debugLineNum = 602;BA.debugLine="res = Msgbox2(\"Do you really want reset the game?";
_res = anywheresoftware.b4a.keywords.Common.Msgbox2("Do you really want reset the game?"+anywheresoftware.b4a.keywords.Common.CRLF+"*note that data will deleted including game progress and library.","Reset game","Yes","","No",(android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.Null),mostCurrent.activityBA);
 //BA.debugLineNum = 603;BA.debugLine="If res == DialogResponse.POSITIVE Then";
if (_res==anywheresoftware.b4a.keywords.Common.DialogResponse.POSITIVE) { 
 //BA.debugLineNum = 604;BA.debugLine="function.resetall(db)";
mostCurrent._function._resetall(mostCurrent.activityBA,_db);
 //BA.debugLineNum = 605;BA.debugLine="ToastMessageShow(\"Success!\", False)";
anywheresoftware.b4a.keywords.Common.ToastMessageShow("Success!",anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 606;BA.debugLine="new";
_new();
 //BA.debugLineNum = 607;BA.debugLine="lblLevel.Text = \"0\"";
mostCurrent._lbllevel.setText((Object)("0"));
 }else {
 //BA.debugLineNum = 609;BA.debugLine="Return True";
if (true) return BA.ObjectToString(anywheresoftware.b4a.keywords.Common.True);
 };
 //BA.debugLineNum = 611;BA.debugLine="End Sub";
return "";
}
public static String  _btnset_click() throws Exception{
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _checked = null;
anywheresoftware.b4a.objects.drawable.ColorDrawable _unchecked = null;
anywheresoftware.b4a.objects.drawable.StateListDrawable _sld2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _enabled2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _pressed2 = null;
int _fstat2 = 0;
boolean _s = false;
float _vol = 0f;
 //BA.debugLineNum = 516;BA.debugLine="Sub btnSet_Click";
 //BA.debugLineNum = 517;BA.debugLine="Dim sld As StateListDrawable";
_sld = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 518;BA.debugLine="Dim checked As BitmapDrawable";
_checked = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 519;BA.debugLine="Dim unchecked As ColorDrawable";
_unchecked = new anywheresoftware.b4a.objects.drawable.ColorDrawable();
 //BA.debugLineNum = 520;BA.debugLine="Dim sld2 As StateListDrawable";
_sld2 = new anywheresoftware.b4a.objects.drawable.StateListDrawable();
 //BA.debugLineNum = 521;BA.debugLine="Dim enabled2, pressed2 As BitmapDrawable";
_enabled2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_pressed2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 523;BA.debugLine="enabled2.Initialize(LoadBitmapSample(File.DirAsse";
_enabled2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 524;BA.debugLine="pressed2.Initialize(LoadBitmapSample(File.DirAsse";
_pressed2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"resetgame_button1.png",anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (40),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (13),mostCurrent.activityBA)).getObject()));
 //BA.debugLineNum = 525;BA.debugLine="sld2.Initialize";
_sld2.Initialize();
 //BA.debugLineNum = 526;BA.debugLine="sld2.AddState(sld.State_Pressed, pressed2)";
_sld2.AddState(_sld.State_Pressed,(android.graphics.drawable.Drawable)(_pressed2.getObject()));
 //BA.debugLineNum = 527;BA.debugLine="sld2.AddState(sld.State_Enabled, enabled2)";
_sld2.AddState(_sld.State_Enabled,(android.graphics.drawable.Drawable)(_enabled2.getObject()));
 //BA.debugLineNum = 529;BA.debugLine="checked.Initialize(LoadBitmap(File.DirAssets, \"tg";
_checked.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmap(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"tglc.png").getObject()));
 //BA.debugLineNum = 530;BA.debugLine="unchecked.Initialize(Colors.White, 0)";
_unchecked.Initialize(anywheresoftware.b4a.keywords.Common.Colors.White,(int) (0));
 //BA.debugLineNum = 531;BA.debugLine="sld.Initialize";
_sld.Initialize();
 //BA.debugLineNum = 532;BA.debugLine="sld.AddState(sld.State_Unchecked, unchecked)";
_sld.AddState(_sld.State_Unchecked,(android.graphics.drawable.Drawable)(_unchecked.getObject()));
 //BA.debugLineNum = 533;BA.debugLine="sld.AddState(sld.State_Checked, checked)";
_sld.AddState(_sld.State_Checked,(android.graphics.drawable.Drawable)(_checked.getObject()));
 //BA.debugLineNum = 534;BA.debugLine="Dim fstat2 = StateManager.GetSetting2(\"fullscreen";
_fstat2 = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"fullscreen","0")));
 //BA.debugLineNum = 535;BA.debugLine="Dim s As Boolean";
_s = false;
 //BA.debugLineNum = 536;BA.debugLine="Dim vol = StateManager.GetSetting2(\"volume\", \"5\")";
_vol = (float)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"volume","5")));
 //BA.debugLineNum = 537;BA.debugLine="If fstat2 == 1 Then";
if (_fstat2==1) { 
 //BA.debugLineNum = 538;BA.debugLine="s = True";
_s = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 540;BA.debugLine="s = False";
_s = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 542;BA.debugLine="If setclick == False Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.False) { 
 //BA.debugLineNum = 543;BA.debugLine="Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9";
mostCurrent._activity.AddView((android.view.View)(mostCurrent._pnlbg.getObject()),(int) (0),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA),anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (100),mostCurrent.activityBA),(int) (anywheresoftware.b4a.keywords.Common.PerYToCurrent((float) (100),mostCurrent.activityBA)-anywheresoftware.b4a.keywords.Common.PerXToCurrent((float) (9),mostCurrent.activityBA)));
 //BA.debugLineNum = 544;BA.debugLine="pnlBg.LoadLayout(\"5\")";
mostCurrent._pnlbg.LoadLayout("5",mostCurrent.activityBA);
 //BA.debugLineNum = 545;BA.debugLine="pnlCur.Width = (vol/10)*pnlVol.Width";
mostCurrent._pnlcur.setWidth((int) ((_vol/(double)10)*mostCurrent._pnlvol.getWidth()));
 //BA.debugLineNum = 546;BA.debugLine="pnlBg.Color = 0x64000000";
mostCurrent._pnlbg.setColor((int) (0x64000000));
 //BA.debugLineNum = 547;BA.debugLine="tglFull.Background = sld";
mostCurrent._tglfull.setBackground((android.graphics.drawable.Drawable)(_sld.getObject()));
 //BA.debugLineNum = 548;BA.debugLine="tglFull.Checked = s";
mostCurrent._tglfull.setChecked(_s);
 //BA.debugLineNum = 549;BA.debugLine="btnReset.Background = sld2";
mostCurrent._btnreset.setBackground((android.graphics.drawable.Drawable)(_sld2.getObject()));
 //BA.debugLineNum = 550;BA.debugLine="anim2.SlideFromTop(\"\", slideval, durval)";
mostCurrent._anim2.SlideFromTop(mostCurrent.activityBA,"",_slideval,(long) (_durval));
 //BA.debugLineNum = 551;BA.debugLine="anim2.StartAnim(pnlSet)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 552;BA.debugLine="setclick = True";
_setclick = anywheresoftware.b4a.keywords.Common.True;
 }else {
 //BA.debugLineNum = 554;BA.debugLine="anim2.SlideToTop(\"anim4\",slideval, durval)";
mostCurrent._anim2.SlideToTop(mostCurrent.activityBA,"anim4",_slideval,(long) (_durval));
 //BA.debugLineNum = 555;BA.debugLine="anim2.StartAnim(pnlSet)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 556;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 557;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 559;BA.debugLine="End Sub";
return "";
}
public static String  _btnsubmit_click() throws Exception{
String _playername = "";
 //BA.debugLineNum = 499;BA.debugLine="Sub btnSubmit_Click";
 //BA.debugLineNum = 500;BA.debugLine="Dim playername As String";
_playername = "";
 //BA.debugLineNum = 501;BA.debugLine="playername = edtName.Text";
_playername = mostCurrent._edtname.getText();
 //BA.debugLineNum = 502;BA.debugLine="savescore(playername)";
_savescore(_playername);
 //BA.debugLineNum = 503;BA.debugLine="Activity.Finish";
mostCurrent._activity.Finish();
 //BA.debugLineNum = 504;BA.debugLine="StartActivity(Score)";
anywheresoftware.b4a.keywords.Common.StartActivity(mostCurrent.activityBA,(Object)(mostCurrent._score.getObject()));
 //BA.debugLineNum = 505;BA.debugLine="End Sub";
return "";
}
public static String  _checklib(String _val) throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor = null;
int _lib = 0;
String _stat = "";
 //BA.debugLineNum = 462;BA.debugLine="Sub checkLib(val As String) As String";
 //BA.debugLineNum = 463;BA.debugLine="Dim cursor As Cursor";
_cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 464;BA.debugLine="Dim lib As Int";
_lib = 0;
 //BA.debugLineNum = 465;BA.debugLine="Dim stat As String";
_stat = "";
 //BA.debugLineNum = 466;BA.debugLine="cursor = db.ExecQuery(\"SELECT * FROM game WHERE i";
_cursor.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM game WHERE id='"+mostCurrent._thislevel+"'")));
 //BA.debugLineNum = 467;BA.debugLine="cursor.Position = 0";
_cursor.setPosition((int) (0));
 //BA.debugLineNum = 468;BA.debugLine="lib = cursor.GetInt(\"lib\")";
_lib = _cursor.GetInt("lib");
 //BA.debugLineNum = 469;BA.debugLine="If lib == 1 Then";
if (_lib==1) { 
 //BA.debugLineNum = 470;BA.debugLine="stat = \"(Already in Library!)\"";
_stat = "(Already in Library!)";
 }else {
 //BA.debugLineNum = 472;BA.debugLine="stat = \"(Added to Library!)\"";
_stat = "(Added to Library!)";
 };
 //BA.debugLineNum = 474;BA.debugLine="Return stat";
if (true) return _stat;
 //BA.debugLineNum = 475;BA.debugLine="End Sub";
return "";
}
public static int  _count(int _value) throws Exception{
anywheresoftware.b4a.sql.SQL.CursorWrapper _row = null;
int _all = 0;
 //BA.debugLineNum = 411;BA.debugLine="Sub	count(value As Int) As Int";
 //BA.debugLineNum = 412;BA.debugLine="Dim row As Cursor";
_row = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 413;BA.debugLine="Dim all As Int";
_all = 0;
 //BA.debugLineNum = 414;BA.debugLine="row = db.ExecQuery(\"SELECT * FROM game WHERE stat";
_row.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM game WHERE stat='"+BA.NumberToString(_value)+"'")));
 //BA.debugLineNum = 415;BA.debugLine="all = row.RowCount";
_all = _row.getRowCount();
 //BA.debugLineNum = 416;BA.debugLine="Return all";
if (true) return _all;
 //BA.debugLineNum = 417;BA.debugLine="End Sub";
return 0;
}
public static String  _fade_animationend() throws Exception{
 //BA.debugLineNum = 495;BA.debugLine="Sub fade_animationend";
 //BA.debugLineNum = 496;BA.debugLine="new";
_new();
 //BA.debugLineNum = 497;BA.debugLine="End Sub";
return "";
}
public static String  _globals() throws Exception{
 //BA.debugLineNum = 14;BA.debugLine="Sub Globals";
 //BA.debugLineNum = 17;BA.debugLine="Private btnBack As Button";
mostCurrent._btnback = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 18;BA.debugLine="Private btnPlay As Button";
mostCurrent._btnplay = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 19;BA.debugLine="Private img1 As ImageView";
mostCurrent._img1 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 20;BA.debugLine="Private img2 As ImageView";
mostCurrent._img2 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 21;BA.debugLine="Private img3 As ImageView";
mostCurrent._img3 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private img4 As ImageView";
mostCurrent._img4 = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 23;BA.debugLine="Dim pic1, pic2, pic3, pic4, sound, ans, trivia, a";
mostCurrent._pic1 = "";
mostCurrent._pic2 = "";
mostCurrent._pic3 = "";
mostCurrent._pic4 = "";
mostCurrent._sound = "";
mostCurrent._ans = "";
mostCurrent._trivia = "";
mostCurrent._anpic = "";
mostCurrent._animalname = "";
 //BA.debugLineNum = 24;BA.debugLine="Private lblLevel As Label";
mostCurrent._lbllevel = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 25;BA.debugLine="Dim animate As AnimationDrawable";
mostCurrent._animate = new flm.b4a.animationplus.AnimationDrawable();
 //BA.debugLineNum = 26;BA.debugLine="Dim thislevel As String";
mostCurrent._thislevel = "";
 //BA.debugLineNum = 27;BA.debugLine="Dim screen As PhoneWakeState";
mostCurrent._screen = new anywheresoftware.b4a.phone.Phone.PhoneWakeState();
 //BA.debugLineNum = 28;BA.debugLine="Private pnlPics As Panel";
mostCurrent._pnlpics = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 29;BA.debugLine="Private lblTrivia As Label";
mostCurrent._lbltrivia = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 30;BA.debugLine="Dim anim2 As ICOSSlideAnimation";
mostCurrent._anim2 = new giuseppe.salvi.icos.library.ICOSSlideAnimation();
 //BA.debugLineNum = 31;BA.debugLine="Dim aniFade As ICOSFadeAnimation";
mostCurrent._anifade = new giuseppe.salvi.icos.library.ICOSFadeAnimation();
 //BA.debugLineNum = 32;BA.debugLine="Dim phone As Phone";
mostCurrent._phone = new anywheresoftware.b4a.phone.Phone();
 //BA.debugLineNum = 33;BA.debugLine="Dim slideval = 700, durval = 1500 , fadedur = 300";
_slideval = (float) (700);
_durval = (float) (1500);
_fadedur = (float) (300);
 //BA.debugLineNum = 35;BA.debugLine="Private pnlCorPop, pnlCorTrivia As Panel";
mostCurrent._pnlcorpop = new anywheresoftware.b4a.objects.PanelWrapper();
mostCurrent._pnlcortrivia = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 36;BA.debugLine="Private lblCorTrivia, lblCorName, lblCorStat, lbl";
mostCurrent._lblcortrivia = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcorname = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcorstat = new anywheresoftware.b4a.objects.LabelWrapper();
mostCurrent._lblcongrat = new anywheresoftware.b4a.objects.LabelWrapper();
 //BA.debugLineNum = 37;BA.debugLine="Private imgCorAnimal As ImageView";
mostCurrent._imgcoranimal = new anywheresoftware.b4a.objects.ImageViewWrapper();
 //BA.debugLineNum = 38;BA.debugLine="Private scroCorTrivia As ScrollView";
mostCurrent._scrocortrivia = new anywheresoftware.b4a.objects.ScrollViewWrapper();
 //BA.debugLineNum = 39;BA.debugLine="Private btnNext, btnSubmit As Button";
mostCurrent._btnnext = new anywheresoftware.b4a.objects.ButtonWrapper();
mostCurrent._btnsubmit = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 40;BA.debugLine="Private edtName As EditText";
mostCurrent._edtname = new anywheresoftware.b4a.objects.EditTextWrapper();
 //BA.debugLineNum = 42;BA.debugLine="Private pnlSet As Panel";
mostCurrent._pnlset = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 43;BA.debugLine="Private pnlVol As Panel";
mostCurrent._pnlvol = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 44;BA.debugLine="Private pnlCur As Panel";
mostCurrent._pnlcur = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 45;BA.debugLine="Private tglFull As ToggleButton";
mostCurrent._tglfull = new anywheresoftware.b4a.objects.CompoundButtonWrapper.ToggleButtonWrapper();
 //BA.debugLineNum = 46;BA.debugLine="Private btnReset As Button";
mostCurrent._btnreset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 47;BA.debugLine="Private btnSet As Button";
mostCurrent._btnset = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 48;BA.debugLine="Private pnlBg As Panel";
mostCurrent._pnlbg = new anywheresoftware.b4a.objects.PanelWrapper();
 //BA.debugLineNum = 49;BA.debugLine="Dim setclick As Boolean";
_setclick = false;
 //BA.debugLineNum = 50;BA.debugLine="Dim finalscore As Int";
_finalscore = 0;
 //BA.debugLineNum = 51;BA.debugLine="Private btnClearHS As Button";
mostCurrent._btnclearhs = new anywheresoftware.b4a.objects.ButtonWrapper();
 //BA.debugLineNum = 52;BA.debugLine="End Sub";
return "";
}
public static String  _new() throws Exception{
int _totalrows = 0;
int _randlevel = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd1 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd2 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd3 = null;
anywheresoftware.b4a.objects.drawable.BitmapDrawable _bd4 = null;
 //BA.debugLineNum = 148;BA.debugLine="Sub new";
 //BA.debugLineNum = 149;BA.debugLine="Dim totalrows , randlevel As Int";
_totalrows = 0;
_randlevel = 0;
 //BA.debugLineNum = 150;BA.debugLine="Dim cursor As Cursor";
_cursor = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 151;BA.debugLine="Dim bd1, bd2, bd3, bd4 As BitmapDrawable";
_bd1 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd2 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd3 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
_bd4 = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
 //BA.debugLineNum = 154;BA.debugLine="cursor = db.ExecQuery(\"SELECT * FROM game WHERE S";
_cursor.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM game WHERE STAT='0'")));
 //BA.debugLineNum = 155;BA.debugLine="totalrows = cursor.RowCount";
_totalrows = _cursor.getRowCount();
 //BA.debugLineNum = 158;BA.debugLine="If totalrows == 1 Then";
if (_totalrows==1) { 
 //BA.debugLineNum = 159;BA.debugLine="cursor.Position = 0";
_cursor.setPosition((int) (0));
 }else {
 //BA.debugLineNum = 161;BA.debugLine="randlevel = Rnd(0,totalrows)";
_randlevel = anywheresoftware.b4a.keywords.Common.Rnd((int) (0),_totalrows);
 //BA.debugLineNum = 162;BA.debugLine="cursor.Position = randlevel";
_cursor.setPosition(_randlevel);
 };
 //BA.debugLineNum = 165;BA.debugLine="pnlPics.Invalidate";
mostCurrent._pnlpics.Invalidate();
 //BA.debugLineNum = 167;BA.debugLine="thislevel = cursor.GetString(\"id\")";
mostCurrent._thislevel = _cursor.GetString("id");
 //BA.debugLineNum = 168;BA.debugLine="pic1 = cursor.GetString(\"1\")";
mostCurrent._pic1 = _cursor.GetString("1");
 //BA.debugLineNum = 169;BA.debugLine="pic2 = cursor.GetString(\"2\")";
mostCurrent._pic2 = _cursor.GetString("2");
 //BA.debugLineNum = 170;BA.debugLine="pic3 = cursor.GetString(\"3\")";
mostCurrent._pic3 = _cursor.GetString("3");
 //BA.debugLineNum = 171;BA.debugLine="pic4 = cursor.GetString(\"4\")";
mostCurrent._pic4 = _cursor.GetString("4");
 //BA.debugLineNum = 172;BA.debugLine="sound = cursor.GetString(\"sound\")";
mostCurrent._sound = _cursor.GetString("sound");
 //BA.debugLineNum = 173;BA.debugLine="ans = cursor.GetString(\"ans\")";
mostCurrent._ans = _cursor.GetString("ans");
 //BA.debugLineNum = 174;BA.debugLine="anPic = cursor.GetString(ans)";
mostCurrent._anpic = _cursor.GetString(mostCurrent._ans);
 //BA.debugLineNum = 175;BA.debugLine="trivia = cursor.GetString(\"trivia\")";
mostCurrent._trivia = _cursor.GetString("trivia");
 //BA.debugLineNum = 176;BA.debugLine="animalName = cursor.GetString(\"name\")";
mostCurrent._animalname = _cursor.GetString("name");
 //BA.debugLineNum = 178;BA.debugLine="cursor.Close";
_cursor.Close();
 //BA.debugLineNum = 180;BA.debugLine="Log(\"id: \" & thislevel & CRLF & \"answer: \" & ans)";
anywheresoftware.b4a.keywords.Common.Log("id: "+mostCurrent._thislevel+anywheresoftware.b4a.keywords.Common.CRLF+"answer: "+mostCurrent._ans);
 //BA.debugLineNum = 182;BA.debugLine="play.Load(File.DirAssets, function.getSndExt(soun";
_play.Load(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getsndext(mostCurrent.activityBA,mostCurrent._sound));
 //BA.debugLineNum = 184;BA.debugLine="bd1.Initialize(LoadBitmapSample(File.DirAssets, f";
_bd1.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic1),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 185;BA.debugLine="bd2.Initialize(LoadBitmapSample(File.DirAssets, f";
_bd2.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic2),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 186;BA.debugLine="bd3.Initialize(LoadBitmapSample(File.DirAssets, f";
_bd3.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic3),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 187;BA.debugLine="bd4.Initialize(LoadBitmapSample(File.DirAssets, f";
_bd4.Initialize((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),mostCurrent._function._getimgext(mostCurrent.activityBA,mostCurrent._pic4),mostCurrent._img1.getWidth(),mostCurrent._img1.getHeight()).getObject()));
 //BA.debugLineNum = 189;BA.debugLine="img1.Background = bd1";
mostCurrent._img1.setBackground((android.graphics.drawable.Drawable)(_bd1.getObject()));
 //BA.debugLineNum = 190;BA.debugLine="img2.Background = bd2";
mostCurrent._img2.setBackground((android.graphics.drawable.Drawable)(_bd2.getObject()));
 //BA.debugLineNum = 191;BA.debugLine="img3.Background = bd3";
mostCurrent._img3.setBackground((android.graphics.drawable.Drawable)(_bd3.getObject()));
 //BA.debugLineNum = 192;BA.debugLine="img4.Background = bd4";
mostCurrent._img4.setBackground((android.graphics.drawable.Drawable)(_bd4.getObject()));
 //BA.debugLineNum = 194;BA.debugLine="aniFade.FadeIn(\"\", fadedur)";
mostCurrent._anifade.FadeIn(mostCurrent.activityBA,"",(long) (_fadedur));
 //BA.debugLineNum = 195;BA.debugLine="aniFade.StartAnim(pnlPics)";
mostCurrent._anifade.StartAnim((android.view.View)(mostCurrent._pnlpics.getObject()));
 //BA.debugLineNum = 197;BA.debugLine="StateManager.SetSetting(\"currentid\", thislevel)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"currentid",mostCurrent._thislevel);
 //BA.debugLineNum = 198;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 199;BA.debugLine="End Sub";
return "";
}
public static String  _play_complete() throws Exception{
 //BA.debugLineNum = 425;BA.debugLine="Sub play_Complete";
 //BA.debugLineNum = 426;BA.debugLine="animate.Stop";
mostCurrent._animate.Stop();
 //BA.debugLineNum = 427;BA.debugLine="btnPlay.SetBackgroundImage(LoadBitmapSample(File.";
mostCurrent._btnplay.SetBackgroundImage((android.graphics.Bitmap)(anywheresoftware.b4a.keywords.Common.LoadBitmapSample(anywheresoftware.b4a.keywords.Common.File.getDirAssets(),"volume_3.png",mostCurrent._btnplay.getWidth(),mostCurrent._btnplay.getHeight()).getObject()));
 //BA.debugLineNum = 428;BA.debugLine="End Sub";
return "";
}
public static int  _playernum() throws Exception{
int _num = 0;
int _num2 = 0;
 //BA.debugLineNum = 419;BA.debugLine="Sub playerNum As Int";
 //BA.debugLineNum = 420;BA.debugLine="Dim num = StateManager.GetSetting2(\"pnum\", \"0\"),";
_num = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"pnum","0")));
_num2 = 0;
 //BA.debugLineNum = 421;BA.debugLine="num2 = num + 1";
_num2 = (int) (_num+1);
 //BA.debugLineNum = 422;BA.debugLine="Return num2";
if (true) return _num2;
 //BA.debugLineNum = 423;BA.debugLine="End Sub";
return 0;
}
public static String  _pnlbg_click() throws Exception{
 //BA.debugLineNum = 561;BA.debugLine="Sub pnlBg_Click";
 //BA.debugLineNum = 562;BA.debugLine="If setclick == True Then";
if (_setclick==anywheresoftware.b4a.keywords.Common.True) { 
 //BA.debugLineNum = 563;BA.debugLine="anim2.SlideToTop(\"anim4\",slideval, durval)";
mostCurrent._anim2.SlideToTop(mostCurrent.activityBA,"anim4",_slideval,(long) (_durval));
 //BA.debugLineNum = 564;BA.debugLine="anim2.StartAnim(pnlSet)";
mostCurrent._anim2.StartAnim((android.view.View)(mostCurrent._pnlset.getObject()));
 //BA.debugLineNum = 565;BA.debugLine="btnSet.Enabled = False";
mostCurrent._btnset.setEnabled(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 566;BA.debugLine="setclick = False";
_setclick = anywheresoftware.b4a.keywords.Common.False;
 };
 //BA.debugLineNum = 568;BA.debugLine="End Sub";
return "";
}
public static String  _pnlcorpop_click() throws Exception{
 //BA.debugLineNum = 458;BA.debugLine="Sub pnlCorPop_Click";
 //BA.debugLineNum = 460;BA.debugLine="End Sub";
return "";
}
public static String  _pnlset_click() throws Exception{
 //BA.debugLineNum = 570;BA.debugLine="Sub pnlSet_Click";
 //BA.debugLineNum = 572;BA.debugLine="End Sub";
return "";
}
public static String  _pnlvol_touch(int _action,float _x,float _y) throws Exception{
float _a = 0f;
 //BA.debugLineNum = 575;BA.debugLine="Sub pnlVol_Touch (Action As Int, X As Float, Y As";
 //BA.debugLineNum = 576;BA.debugLine="Dim a As Float";
_a = 0f;
 //BA.debugLineNum = 577;BA.debugLine="If X < 0 Then";
if (_x<0) { 
 //BA.debugLineNum = 578;BA.debugLine="X = 0";
_x = (float) (0);
 }else if(_x>mostCurrent._pnlvol.getWidth()) { 
 //BA.debugLineNum = 580;BA.debugLine="X = pnlVol.Width";
_x = (float) (mostCurrent._pnlvol.getWidth());
 };
 //BA.debugLineNum = 582;BA.debugLine="a = (X/pnlVol.Width)*10";
_a = (float) ((_x/(double)mostCurrent._pnlvol.getWidth())*10);
 //BA.debugLineNum = 583;BA.debugLine="StateManager.SetSetting(\"volume\", a)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"volume",BA.NumberToString(_a));
 //BA.debugLineNum = 584;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 585;BA.debugLine="pnlCur.Width = X";
mostCurrent._pnlcur.setWidth((int) (_x));
 //BA.debugLineNum = 586;BA.debugLine="play.SetVolume(a/10, a/10)";
_play.SetVolume((float) (_a/(double)10),(float) (_a/(double)10));
 //BA.debugLineNum = 587;BA.debugLine="End Sub";
return "";
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 6;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 9;BA.debugLine="Dim db As SQL";
_db = new anywheresoftware.b4a.sql.SQL();
 //BA.debugLineNum = 10;BA.debugLine="Private play As MediaPlayer";
_play = new anywheresoftware.b4a.objects.MediaPlayerWrapper();
 //BA.debugLineNum = 12;BA.debugLine="End Sub";
return "";
}
public static String  _reset() throws Exception{
 //BA.debugLineNum = 405;BA.debugLine="Sub reset";
 //BA.debugLineNum = 406;BA.debugLine="db.ExecNonQuery(\"UPDATE game SET stat='0'\")";
_db.ExecNonQuery("UPDATE game SET stat='0'");
 //BA.debugLineNum = 407;BA.debugLine="StateManager.SetSetting(\"currentid\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"currentid","0");
 //BA.debugLineNum = 408;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 409;BA.debugLine="End Sub";
return "";
}
public static String  _savescore(String _pname) throws Exception{
String _oldnam = "";
String _oldsco = "";
int _t = 0;
int _playn = 0;
anywheresoftware.b4a.sql.SQL.CursorWrapper _cursor2 = null;
 //BA.debugLineNum = 430;BA.debugLine="Sub savescore(pname As String)";
 //BA.debugLineNum = 431;BA.debugLine="Dim oldnam, oldsco As String";
_oldnam = "";
_oldsco = "";
 //BA.debugLineNum = 432;BA.debugLine="Dim t = finalscore, playN = StateManager.GetSetti";
_t = _finalscore;
_playn = (int)(Double.parseDouble(mostCurrent._statemanager._getsetting2(mostCurrent.activityBA,"pnum","0")));
 //BA.debugLineNum = 433;BA.debugLine="Dim cursor2 As Cursor";
_cursor2 = new anywheresoftware.b4a.sql.SQL.CursorWrapper();
 //BA.debugLineNum = 435;BA.debugLine="If pname == \"\" Then";
if ((_pname).equals("")) { 
 //BA.debugLineNum = 436;BA.debugLine="pname = \"Player\" & playerNum";
_pname = "Player"+BA.NumberToString(_playernum());
 };
 //BA.debugLineNum = 439;BA.debugLine="If pname.StartsWith(\"Player\") Then";
if (_pname.startsWith("Player")) { 
 //BA.debugLineNum = 440;BA.debugLine="StateManager.SetSetting(\"pnum\", playN + 1)";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"pnum",BA.NumberToString(_playn+1));
 //BA.debugLineNum = 441;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 };
 //BA.debugLineNum = 446;BA.debugLine="cursor2 = db.ExecQuery(\"SELECT * FROM hscore WHER";
_cursor2.setObject((android.database.Cursor)(_db.ExecQuery("SELECT * FROM hscore WHERE rscore='"+BA.NumberToString(_t)+"'")));
 //BA.debugLineNum = 447;BA.debugLine="If cursor2.RowCount == 1 Then";
if (_cursor2.getRowCount()==1) { 
 //BA.debugLineNum = 448;BA.debugLine="cursor2.Position = 0";
_cursor2.setPosition((int) (0));
 //BA.debugLineNum = 449;BA.debugLine="oldnam = cursor2.GetString(\"name\")";
_oldnam = _cursor2.GetString("name");
 //BA.debugLineNum = 450;BA.debugLine="oldsco = cursor2.GetString(\"score\")";
_oldsco = _cursor2.GetString("score");
 //BA.debugLineNum = 451;BA.debugLine="cursor2.Close";
_cursor2.Close();
 //BA.debugLineNum = 452;BA.debugLine="db.ExecNonQuery(\"UPDATE hscore SET	name='\" & old";
_db.ExecNonQuery("UPDATE hscore SET	name='"+_oldnam+anywheresoftware.b4a.keywords.Common.CRLF+_pname+"', score='"+_oldsco+anywheresoftware.b4a.keywords.Common.CRLF+BA.NumberToString(_t)+"' WHERE rscore='"+BA.NumberToString(_t)+"'");
 }else {
 //BA.debugLineNum = 454;BA.debugLine="db.ExecNonQuery2(\"INSERT INTO hscore VALUES (?,?";
_db.ExecNonQuery2("INSERT INTO hscore VALUES (?,?,?)",anywheresoftware.b4a.keywords.Common.ArrayToList(new Object[]{(Object)(_pname),(Object)(_t),(Object)(_t)}));
 };
 //BA.debugLineNum = 456;BA.debugLine="End Sub";
return "";
}
public static String  _tglfull_checkedchange(boolean _checked) throws Exception{
 //BA.debugLineNum = 589;BA.debugLine="Sub tglFull_CheckedChange(Checked As Boolean)";
 //BA.debugLineNum = 590;BA.debugLine="If Checked Then";
if (_checked) { 
 //BA.debugLineNum = 591;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"1\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","1");
 }else {
 //BA.debugLineNum = 593;BA.debugLine="StateManager.SetSetting(\"fullscreen\", \"0\")";
mostCurrent._statemanager._setsetting(mostCurrent.activityBA,"fullscreen","0");
 };
 //BA.debugLineNum = 595;BA.debugLine="StateManager.SaveSettings";
mostCurrent._statemanager._savesettings(mostCurrent.activityBA);
 //BA.debugLineNum = 596;BA.debugLine="function.FullScreen(Checked, \"Play\")";
mostCurrent._function._fullscreen(mostCurrent.activityBA,_checked,"Play");
 //BA.debugLineNum = 597;BA.debugLine="Activity.Invalidate";
mostCurrent._activity.Invalidate();
 //BA.debugLineNum = 598;BA.debugLine="End Sub";
return "";
}
}
