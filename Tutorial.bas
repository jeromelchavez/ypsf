﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim MP As MediaPlayer
	Dim db As SQL

End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.
	Private btnPic1 As Button
	Private btnPic2 As Button
	Private btnPic4 As Button
	Private btnPic3 As Button
	Private btnBack As Button
	Private btnSet As Button
	Private lblLevel As Label
	Private img1 As ImageView
	Private img2 As ImageView
	Private img4 As ImageView
	Private img3 As ImageView
	Private btnPlay As Button
	Private pnlPics As Panel
	Private pnlTutBG As Panel
	Dim screen As PhoneWakeState
	Private pnlBG, pnlTut As Panel
	Private lblIns As Label
	Private btnNext, btnDone, btnYes, btnNo As Button
	Dim aniSlide As ICOSSlideAnimation
	Dim aniFade As ICOSFadeAnimation
	Dim playAnim As AnimationDrawable
	Dim slideval = 700, durval = 1500 , fadedur = 300 As Float
	Private btnClearHS As Button
	
	Private lblCorName, lblCortrivia As Label
	Private scroCorTrivia, scroTut As ScrollView
	Private imgCoranimal As ImageView
	Dim trivia As String
	trivia =	"Dogs are thought to have been first domesticated in East Asia thousands of years ago. People primarily used dogs for guarding the hunters and areas of land." & _ 
				"Todays domestic dog Is actually a subspecies of the gray wolf, a Type of dog that Is feared by most humans. Many people today, in all countries around the world, keep dogs As household pets And many even regard their dog As a family member."
	
	Dim msg1, msg2, msg3 As String
	msg1 =	"To pass the level, first, click the play button to hear the sound question."
	msg2 =	"The four pictures below are the choices," & CRLF & _
	 		"if you click the picture that matches the sound, a trivia will popup." & CRLF & _
			"in this example, the first picture(dog) is the right answer, click it."
	msg3 =	"You completed the tutorial." & CRLF & _
			"Do you want to start the game now?"
			
	Private pnlSet As Panel
	Private pnlVol As Panel
	Private pnlCur As Panel
	Private tglFull As ToggleButton
	Private btnReset As Button
	Dim setclick As Boolean
	
	Dim sld, sld2 As StateListDrawable
	Dim border As ColorDrawable
	Dim enabled, pressed, ed1, pd1, ed2, pd2 As BitmapDrawable
	
	Dim sname As String
	
End Sub

Sub Activity_Create(FirstTime As Boolean)
	If FirstTime Then
		MP.Initialize2("MP")
	End If
	
	Activity.LoadLayout("2")
	lblLevel.Text = "0"
	pnlBG.Initialize("pnlBg")
	pnlTut.Initialize("")
	lblIns.Initialize("")
	btnNext.Initialize("btnNext")
	btnYes.Initialize("btnYes")
	btnNo.Initialize("btnNo")
	lblCorName.Initialize("")
	lblCortrivia.Initialize("")
	scroCorTrivia.Initialize(1000dip)
	imgCoranimal.Initialize("")
	pnlTutBG.Initialize("")
	scroTut.Initialize(500)
	btnDone.Initialize("btnDone")
	
	
	level("dog1.jpg", "cat1.jpg", "tiger1.jpg", "elephant1.jpg")
	pop(msg1, "1")
	
	screen.KeepAlive(True)
	screen.PartialLock
End Sub

Sub Activity_Resume
	Dim fstat = StateManager.GetSetting2("fullscreen", "0") As Int
	Dim vol = StateManager.GetSetting2("volume", "5") As Int
	If fstat == 1 Then
		function.FullScreen(True, "Tutorial")
	Else
		function.FullScreen(False, "Tutorial")
	End If
	Activity.Invalidate
	playAnim.Initialize
	MP.SetVolume(vol/10, vol/10)
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	screen.ReleaseKeepAlive
	screen.ReleasePartialLock
End Sub

Sub level(pic1 As String, pic2 As String, pic3 As String, pic4 As String)
 	Dim i1, i2, i3, i4 As BitmapDrawable
	i1.Initialize(LoadBitmapSample(File.DirAssets, pic1, img1.Width, img1.Height))
	i2.Initialize(LoadBitmapSample(File.DirAssets, pic2, img1.Width, img1.Height))
	i3.Initialize(LoadBitmapSample(File.DirAssets, pic3, img1.Width, img1.Height))
	i4.Initialize(LoadBitmapSample(File.DirAssets, pic4, img1.Width, img1.Height))
	img1.Background = i1
	img2.Background = i2
	img3.Background = i3
	img4.Background = i4
	btnPic1.Enabled = False
	btnPic2.Enabled = False
	btnPic3.Enabled = False
	btnPic4.Enabled = False
	btnPlay.Enabled = False
End Sub

Sub pop(msg As String, seq As String)
	Dim stringu As StringUtils
	border.Initialize2(0xFF28507B, 0, 4dip, Colors.Yellow)

	Activity.AddView(pnlTutBG, 0, 9%x, 100%x, 100%y - 9%x)
	pnlTutBG.Color = 0x64000000
	pnlTutBG.AddView(pnlTut, 2%x, 30%x, 96%x, 46%x)
	pnlTut.Background = border
	pnlTut.AddView(scroTut, 2%x, 2%x,92%x, 30%x)
	scroTut.Panel.AddView(lblIns, 0, 0, 92%x,200)
	lblIns.TextSize = (9%x/100)*35
	lblIns.Gravity = Gravity.CENTER
	lblIns.TextColor = Colors.White
	lblIns.Text = msg
	lblIns.Height = stringu.MeasureMultilineTextHeight(lblIns, lblIns.Text)
	scroTut.Panel.Height = stringu.MeasureMultilineTextHeight(lblIns, lblIns.Text)
	If seq == "last" Then
	
		ed1.Initialize(LoadBitmapSample(File.DirAssets, "yes_button.png", 50%x, 10%x))
		pd1.Initialize(LoadBitmapSample(File.DirAssets, "yes_button1.png", 50%x, 10%x))
		
		ed2.Initialize(LoadBitmapSample(File.DirAssets, "no_button.png", 50%x, 10%x))
		pd2.Initialize(LoadBitmapSample(File.DirAssets, "no_button1.png", 50%x, 10%x))
		sld.Initialize
		sld.AddState(sld.State_Pressed, pd1)
		sld.AddState(sld.State_Enabled, ed1)
	
		sld2.Initialize
		sld2.AddState(sld2.State_Pressed, pd2)
		sld2.AddState(sld2.State_Enabled, ed2)
		
		pnlTut.AddView(btnYes,2%x, 34%x, 45%x, 10%x)
		btnYes.Background = sld
'		btnYes.Text = "Yes"
'		btnYes.TextSize = (10%x/100)*30
'		btnYes.Gravity = Gravity.CENTER_HORIZONTAL
		pnlTut.AddView(btnNo,49%x, 34%x, 45%x, 10%x)
'		btnNo.TextSize = (10%x/100)*30
'		btnNo.Text = "No"
'		btnNo.Gravity = Gravity.CENTER_HORIZONTAL
		btnNo.Background = sld2
	Else
		
		enabled.Initialize(LoadBitmapSample(File.DirAssets, "next_button.png", 50%x, 10%x))
		pressed.Initialize(LoadBitmapSample(File.DirAssets, "next_button1.png", 50%x, 10%x))
		sld.Initialize
		sld.AddState(sld.State_Pressed, pressed)
		sld.AddState(sld.State_Enabled, enabled)
		
		pnlTut.AddView(btnNext,23%x, 34%x, 50%x, 10%x)
		btnNext.Background = sld
'		btnNext.Text = "NEXT"
'		btnNext.Gravity = Gravity.CENTER_HORIZONTAL
'		btnNext.TextSize = (10%x/100)*30
	End If
	sname = seq
End Sub

Sub btnNext_Click
	btnNext.RemoveView
	lblIns.RemoveView
	scroTut.RemoveView
	pnlTut.RemoveView
	pnlTutBG.RemoveView
	If sname == "1" Then
		btnPlay.Enabled = True
	else if sname == "2" Then
		btnPic1.Enabled = True
		btnPlay.Enabled = False
	End If
End Sub

Sub btnEvent_Click	
	border.Initialize2(0xFF28507B, 0, 4dip, Colors.Yellow)
	enabled.Initialize(LoadBitmapSample(File.DirAssets, "nextlevel_button.png", 50%x, 10%x))
	pressed.Initialize(LoadBitmapSample(File.DirAssets, "nextlevel_button1.png", 50%x, 10%x))
	sld.Initialize
	sld.AddState(sld.State_Pressed, pressed)
	sld.AddState(sld.State_Enabled, enabled)
		
		Activity.AddView(pnlTutBG, 0, 9%x, 100%x, 100%y - 9%x)
		pnlTutBG.Color = 0x64000000
		'trivia
		pnlTutBG.AddView(pnlTut, 2%x, 25%x, 96%x, 96%x)
		pnlTut.Background = border
		'animals name
		pnlTut.AddView(lblCorName, 28%x, 6%x, 40%x, 7%x) 
		lblCorName.Text = "Dog"
		lblCorName.TextColor = Colors.White
		lblCorName.Gravity = Gravity.CENTER_HORIZONTAL
		lblCorName.TextSize = (10%x/100)*35
		'scrollview info
		pnlTut.AddView(scroCorTrivia, 2%x, 13%x, 92%x, 70%x)
		
		scroCorTrivia.Panel.AddView(imgCoranimal, 22%x, 0, 50%x, 50%x)
		
		imgCoranimal.Bitmap = LoadBitmapSample(File.DirAssets, "dog1.jpg", imgCoranimal.Width, imgCoranimal.Height)
		imgCoranimal.Gravity = Gravity.FILL
		
		scroCorTrivia.Panel.AddView(lblCortrivia, 2%x, 50%x, 90%x, 90%x)
		lblCortrivia.Text = trivia
		lblCortrivia.Gravity = Gravity.CENTER_HORIZONTAL
		lblCortrivia.TextSize = (10%x/100)*30
		scroCorTrivia.Panel.Height = 150%x
		scroCorTrivia.ScrollPosition = 0
		'done
		pnlTut.AddView(btnDone,23%x, 84%x, 50%x, 10%x)
		btnDone.Background = sld
'		btnDone.Text = "NEXT LEVEL"
'		btnDone.Typeface = Typeface.DEFAULT_BOLD
'		btnDone.TextSize = (10%x/100)*30
'		btnDone.TextColor = Colors.White
'		btnDone.Gravity = Gravity.CENTER_HORIZONTAL
		aniSlide.SlideFromTop("", slideval, durval)
		aniSlide.StartAnim(pnlTut)
		
'		MP.Load(File.DirAssets, "correct-sound-effect.mp3")
'		MP.Play
End Sub

Sub btnDone_Click
	pnlTut.RemoveAllViews
	pnlTutBG.RemoveAllViews
	pnlTutBG.RemoveView
	lblLevel.Text = "0"
	aniFade.FadeOut("newlevel", durval)
	aniFade.StartAnim(pnlPics)
End Sub

Sub newlevel_animationend
	level("beetle1.jpg", "bee1.jpg", "cricket2.jpg", "deer1.jpg")
	aniFade.FadeIn("endtutor", fadedur)
	aniFade.StartAnim(pnlPics)
End Sub

Sub endtutor_animationend
	pop(msg3, "last")
End Sub

Sub btnPlay_Click
	MP.Load(File.DirAssets, "dog.mp3")
	MP.Play
	playAnim.AddFrame(LoadBitmapSample(File.DirAssets, "volume_0.png", btnPlay.Width, btnPlay.Height), 150)
	playAnim.AddFrame(LoadBitmapSample(File.DirAssets, "volume_1.png", btnPlay.Width, btnPlay.Height), 150)
	playAnim.AddFrame(LoadBitmapSample(File.DirAssets, "volume_2.png", btnPlay.Width, btnPlay.Height), 150)
	playAnim.AddFrame(LoadBitmapSample(File.DirAssets, "volume_3.png", btnPlay.Width, btnPlay.Height), 150)
	playAnim.OneShot = False
	btnPlay.Background = playAnim
	playAnim.Start
End Sub

Sub MP_Complete
	playAnim.Stop
	btnPlay.SetBackgroundImage(LoadBitmapSample(File.DirAssets, "volume_3.png", btnPlay.Width, btnPlay.Height))
	pop(msg2, "2")
End Sub

Sub btnYes_Click
	Activity.Finish
	StartActivity(Play)
	function.SetAnimation("animtoright", "animfromleft")
End Sub

Sub btnNo_Click
	Activity.Finish
	StartActivity(Main)
	function.SetAnimation("animtoright", "animfromleft")
End Sub

Sub btnBack_Click
	Activity.Finish
	StartActivity(Main)
	function.SetAnimation("animtoright", "animfromleft")
End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean
    If KeyCode = KeyCodes.KEYCODE_BACK Then
		Return True
	Else If KeyCode = KeyCodes.KEYCODE_MENU And btnSet.Enabled == True Then
		btnSet_Click
	End If
End Sub

Sub btnSet_Click
	Dim sld As StateListDrawable
	Dim checked As BitmapDrawable
	Dim unchecked As ColorDrawable
	Dim sld2 As StateListDrawable
	Dim enabled2, pressed2 As BitmapDrawable
	
	enabled2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button.png", 40%x, 13%x))
	pressed2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button1.png", 40%x, 13%x))
	sld2.Initialize
	sld2.AddState(sld2.State_Pressed, pressed2)
	sld2.AddState(sld2.State_Enabled, enabled2)
	
	checked.Initialize(LoadBitmap(File.DirAssets, "tglc.png"))
	unchecked.Initialize(Colors.White, 0)
	sld.Initialize
	sld.AddState(sld.State_Unchecked, unchecked)
	sld.AddState(sld.State_Checked, checked)
	Dim fstat2 = StateManager.GetSetting2("fullscreen", "0") As Int
	Dim s As Boolean
	Dim vol = StateManager.GetSetting2("volume", "5") As Float
	If fstat2 == 1 Then
		s = True
	Else
		s = False
	End If
	If setclick == False Then
		Activity.AddView(pnlBG, 0, 9%x, 100%x, 100%y - 9%x)
		pnlBG.LoadLayout("5")
		pnlCur.Width = (vol/10)*pnlVol.Width
		pnlBG.Color = 0x64000000
		tglFull.Background = sld
		tglFull.Checked = s
		btnReset.Background = sld2
		aniSlide.SlideFromTop("", slideval, durval)
		aniSlide.StartAnim(pnlSet)
		setclick = True
	Else
		aniSlide.SlideToTop("anim",slideval, durval)
		aniSlide.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlBg_Click
	If setclick == True Then
		aniSlide.SlideToTop("anim",slideval, durval)
		aniSlide.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlSet_Click

End Sub

Sub pnlVol_Touch (Action As Int, X As Float, Y As Float)
	Dim a As Float
	If X < 0 Then
		X = 0
	Else If X > pnlVol.Width Then
		X = pnlVol.Width
	End If
	a = (X/pnlVol.Width)*10
	StateManager.SetSetting("volume", a)
	StateManager.SaveSettings
	pnlCur.Width = X
	MP.SetVolume(a/10, a/10)
End Sub

Sub tglFull_CheckedChange(Checked As Boolean)
	If Checked Then
		StateManager.SetSetting("fullscreen", "1")
	Else
		StateManager.SetSetting("fullscreen", "0")
	End If
	StateManager.SaveSettings
	function.FullScreen(Checked, "Tutorial")
	Activity.Invalidate
End Sub

Sub btnReset_Click
	Dim res As Int
	res = Msgbox2("Do you really want reset the game?"&CRLF&"*note that data will deleted including game progress and library.", "Reset game", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		function.resetall(db)
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub

Sub anim_animationend
	pnlBG.RemoveAllViews
	pnlBG.RemoveView
	btnSet.Enabled = True
End Sub

Sub btnClearHS_Click
	Dim res As Int
	res = Msgbox2("Do you really want to clear the highscore?", "Reset", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		db.ExecNonQuery("DELETE FROM hscore")
		StateManager.SetSetting("pnum", "0")
		StateManager.SaveSettings
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub