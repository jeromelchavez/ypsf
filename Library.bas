﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: False
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Dim sql As SQL
	Dim timer As Timer
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private btnBack As Button
	Private btnSet As Button
	Private imgInfo As ImageView
	Private pnlPop ,pnlInfo As Panel
	Dim name, imgAni, aniTrivia As String
	Dim id As Int
	Private listTrivia As ListView
	Private lblInfo ,lblName ,lblTrivia As Label
	Private scroInfo As ScrollView
	Dim anim As ICOSSlideAnimation
	Dim label1, label2 As Label
	Dim pnlLoad, pnlLoadBG As Panel
	Dim progLoad As ProgressBar
	Dim screen As PhoneWakeState
	Dim lblProg, lblLoading, lblProcessed As Label
	Dim n = 0 , total As Int
	Dim cursor As Cursor
	Private pnlSet As Panel
	Private pnlVol As Panel
	Private pnlCur As Panel
	Private tglFull As ToggleButton
	Private btnReset As Button
	Dim setclick As Boolean
	Private pnlBg As Panel
	Dim slideval = 700, durval = 1500 As Float
	Private btnClearHS As Button
End Sub

Sub Activity_Create(FirstTime As Boolean)
	Dim border As ColorDrawable
	Dim pbcd, pbcdbg As ColorDrawable
	border.Initialize2(0xFF28507B, 0, 4dip, Colors.Yellow)
	pbcd.Initialize(Colors.Red, 0)
	pbcdbg.Initialize(Colors.White, 0)
	
	If File.Exists(File.DirInternal,"db") == False Then
		File.Copy(File.DirAssets, "db.db", File.DirInternal, "db")
	End If
	If FirstTime Then
		sql.Initialize(File.DirInternal, "db",False)
	End If
	timer.Initialize("timer", 50)
	timer.Enabled = True
	pnlLoadBG.Initialize("pnlLoadBG")
	pnlLoad.Initialize("")
	progLoad.Initialize("")
	lblProg.Initialize("")
	lblLoading.Initialize("")
	lblProcessed.Initialize("")
	pnlBg.Initialize("pnlBg")
	
	Activity.AddView(pnlLoadBG, 0, 9%x, 100%x, 100%y - 9%x)
	pnlLoadBG.Color = 0x64000000
	pnlLoadBG.AddView(pnlLoad, 2%x, 30%x, 96%x, 33%x)
	pnlLoad.Background = border
	pnlLoad.AddView(lblLoading, 2%x, 2%x, 30%x, 10%x)
	lblLoading.Text = "Loading..."
	lblLoading.TextColor = Colors.White
	lblLoading.TextSize = (10%x/100)*40
	pnlLoad.AddView(progLoad, 2%x, 12%x, 92%x, 5%x)
	pnlLoad.AddView(lblProg, 2%x, 21%x, 20%x, 10%x)
	lblProg.TextColor = Colors.White
	pnlLoad.AddView(lblProcessed, 74%x, 21%x, 20%x, 10%x)
	lblProcessed.TextColor = Colors.White
	lblProcessed.Gravity = Gravity.RIGHT
	function.SetProgressDrawable(progLoad, pbcd)
	progLoad.Background = pbcdbg
	Activity.LoadLayout("4")
	
	pnlLoadBG.BringToFront
	pnlPop.Initialize("pnlPop")
	pnlInfo.Initialize("")
	imgInfo.Initialize("")
	lblName.Initialize("")
	lblTrivia.Initialize("")
	scroInfo.Initialize(1000dip)
	
	cursor = sql.ExecQuery("SELECT * FROM game WHERE lib='1'")
	total = cursor.RowCount
	
	label1 = listTrivia.TwoLinesAndBitmap.Label
	label1.TextSize = (10%x/100)*40
	label2 = listTrivia.TwoLinesAndBitmap.SecondLabel
	label2.TextSize = (10%x/100)*25
	
	screen.KeepAlive(True)
	screen.PartialLock
End Sub

Sub Activity_Resume
	Dim fstat = StateManager.GetSetting2("fullscreen", "0") As Int
	If fstat == 1 Then
		function.FullScreen(True, "Library")
	Else
		function.FullScreen(False, "Library")
	End If
	Activity.Invalidate
End Sub

Sub Activity_Pause (UserClosed As Boolean)
	screen.ReleaseKeepAlive
	screen.ReleasePartialLock
End Sub

Sub btnBack_Click
	If timer.Enabled == True Then
		ToastMessageShow("You cant go back while loading the data", False)
	Else
		Activity.Finish
		StartActivity(Main)
		function.SetAnimation("animtoright", "animfromleft")
	End If
End Sub

Sub timer_Tick
	n = n + 1 
	If n > 0 And n < total+1 Then 
		progLoad.Progress = (n/total)*100
		cursor.Position = n-1
		name = cursor.GetString("name")
		id = cursor.GetInt("id")
		aniTrivia = cursor.GetString("trivia").SubString2(0, 50) & "..."
		imgAni = cursor.GetString(cursor.GetString("ans"))
		lblProg.Text = progLoad.Progress & "%"
		lblProcessed.Text = n & "/" & total
		listTrivia.AddTwoLinesAndBitmap2(name, aniTrivia, LoadBitmapSample(File.DirAssets,function.getImgExt(imgAni), 15%x, 15%x),  id)
	Else If n == total+1 Then
		lblLoading.Text = "Done"
		lblProg.Text = "100%"
		lblProcessed.Text = total & "/" & total
	Else If n == total + 20 Then
		pnlLoad.RemoveAllViews
		pnlLoadBG.RemoveAllViews
		pnlLoadBG.RemoveView
		timer.Enabled = False
		cursor.Close
	End If
End Sub

Sub listTrivia_ItemClick (Position As Int, Value As Object)
	Dim val = Value As Int
	Dim fheight As Float
	Dim animal, pic, trivia ,ans As String
	Dim cursor As Cursor
	Dim border As ColorDrawable
	Dim su As StringUtils
	
	border.Initialize2(0xFF28507B, 0, 4dip, Colors.Yellow)
	
	cursor = sql.ExecQuery("SELECT * FROM game WHERE id='" & val &"'")
	cursor.Position = 0
	ans = cursor.GetString("ans")
	animal = cursor.GetString("name")
	pic = function.getImgExt(cursor.GetString(ans))
	trivia = cursor.GetString("trivia")
	cursor.Close
	'popup 
	Activity.AddView(pnlPop, 0, 9%x, 100%x, 100%y - 9%x)
	pnlPop.Color = 0x64000000
	'Infos
	pnlPop.AddView(pnlInfo, 2%x, 20%x, 96%x, 102%x)
	pnlInfo.Background = border
	'animalname
	pnlInfo.AddView(lblName, 28%x, 2%x, 40%x, 9%x)
	lblName.TextColor = 0xFFFFFFFF
	lblName.TextSize = (9%x/100)*45
	lblName.Text = animal
	lblName.Gravity = Gravity.CENTER
	'image
	pnlInfo.AddView(imgInfo, 23%x, 11%x, 50%x, 50%x)
	imgInfo.Gravity = Gravity.FILL
	imgInfo.Bitmap = LoadBitmapSample(File.DirAssets, pic, imgInfo.Width, imgInfo.Height)
	'scrollview trivia
	pnlInfo.AddView(scroInfo, 2%x, 61%x, 94%x, 39%x)
	scroInfo.ScrollPosition = 0
	scroInfo.Panel.Height = 200%x
	scroInfo.Panel.AddView(lblTrivia, 2%x, 0, 90%x, 200%x)
	lblTrivia.TextColor = 0xFFFFFFFF
	lblTrivia.TextSize = (9%x/100)*40
	lblTrivia.Text = trivia
	lblTrivia.Gravity = Gravity.CENTER_HORIZONTAL
	
	anim.SlideFromTop("", 650, 1500)
	anim.StartAnim(pnlInfo)
	
	fheight = su.MeasureMultilineTextHeight(lblTrivia, lblTrivia.Text)
	lblTrivia.Height = fheight
	scroInfo.Panel.Height = fheight
	
	
	
End Sub

Sub pnlLoadBG_Click

End Sub

Sub Activity_KeyPress (KeyCode As Int) As Boolean
    If KeyCode = KeyCodes.KEYCODE_BACK Then
		Return True
	Else If KeyCode = KeyCodes.KEYCODE_MENU And btnSet.Enabled == True Then
		btnSet_Click
	End If
End Sub

Sub pnlPop_Click
	anim.SlideToTop("anim",650, 1000)
	anim.StartAnim(pnlInfo)
End Sub

Sub anim_animationend
	pnlPop.RemoveView
	pnlInfo.RemoveView
	imgInfo.RemoveView
	lblName.RemoveView
	scroInfo.RemoveView
	lblTrivia.RemoveView
End Sub

Sub btnSet_Click
	Dim sld As StateListDrawable
	Dim checked As BitmapDrawable
	Dim unchecked As ColorDrawable
	Dim sld2 As StateListDrawable
	Dim enabled2, pressed2 As BitmapDrawable
	
	
	enabled2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button.png", 40%x, 13%x))
	pressed2.Initialize(LoadBitmapSample(File.DirAssets, "resetgame_button1.png", 40%x, 13%x))
	sld2.Initialize
	sld2.AddState(sld2.State_Pressed, pressed2)
	sld2.AddState(sld2.State_Enabled, enabled2)
	
	checked.Initialize(LoadBitmap(File.DirAssets, "tglc.png"))
	unchecked.Initialize(Colors.White, 0)
	sld.Initialize
	sld.AddState(sld.State_Unchecked, unchecked)
	sld.AddState(sld.State_Checked, checked)
	Dim fstat2 = StateManager.GetSetting2("fullscreen", "0") As Int
	Dim s As Boolean
	Dim vol = StateManager.GetSetting2("volume", "5") As Float
	If fstat2 == 1 Then
		s = True
	Else
		s = False
	End If
	If setclick == False Then
		Activity.AddView(pnlBg, 0, 9%x, 100%x, 100%y - 9%x)
		pnlBg.LoadLayout("5")
		pnlCur.Width = (vol/10)*pnlVol.Width
		pnlBg.Color = 0x64000000
		tglFull.Background = sld
		tglFull.Checked = s
		btnReset.Background = sld2
		anim.SlideFromTop("", slideval, durval)
		anim.StartAnim(pnlSet)
		setclick = True
	Else
		anim.SlideToTop("anim1",slideval, durval)
		anim.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlBg_Click
	If setclick == True Then
		anim.SlideToTop("anim1",slideval, durval)
		anim.StartAnim(pnlSet)
		btnSet.Enabled = False
		setclick = False
	End If
End Sub

Sub pnlSet_Click

End Sub

Sub pnlVol_Touch (Action As Int, X As Float, Y As Float)
	Dim a As Float
	If X < 0 Then
		X = 0
	Else If X > pnlVol.Width Then
		X = pnlVol.Width
	End If
	a = (X/pnlVol.Width)*10
	StateManager.SetSetting("volume", a)
	StateManager.SaveSettings
	pnlCur.Width = X
End Sub

Sub tglFull_CheckedChange(Checked As Boolean)
	If Checked Then
		StateManager.SetSetting("fullscreen", "1")
	Else
		StateManager.SetSetting("fullscreen", "0")
	End If
	StateManager.SaveSettings
	function.FullScreen(Checked, "Library")
	Activity.Invalidate
End Sub

Sub btnReset_Click
	Dim res As Int
	res = Msgbox2("Do you really want reset the game?"&CRLF&"*note that data will deleted including game progress and library.", "Reset game", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		function.resetall(sql)
		ToastMessageShow("Success!", False)
		listTrivia.Clear
	Else
		Return True
	End If
End Sub

Sub anim1_animationend
	pnlBg.RemoveAllViews
	pnlBg.RemoveView
	btnSet.Enabled = True
End Sub

Sub btnClearHS_Click
	Dim res As Int
	res = Msgbox2("Do you really want to clear the highscore?", "Reset", "Yes", "", "No", Null)
	If res == DialogResponse.POSITIVE Then
		sql.ExecNonQuery("DELETE FROM hscore")
		StateManager.SetSetting("pnum", "0")
		StateManager.SaveSettings
		ToastMessageShow("Success!", False)
	Else
		Return True
	End If
End Sub